function rename-js-to-mjs -d "Rename .js file to .mjs."
  set -l filename (string trim --left --chars "./" $argv[1])
  set -l split (string split '.' $filename)
  set -l name $split[1]
  set -l extension $split[2]
  if test $extension = js
    mv $filename $name.mjs
  end
end

function rename-js-to-mjs-recursive -d "Recursively rename .js to .mjs."
  set -l directory $argv[1]
  for item in (ls $directory)
    if test -d $directory/$item
      rename-js-to-mjs-recursive $directory/$item
    else if test -f $directory/$item
      rename-js-to-mjs $directory/$item
    end
  end
end

set -l directory $argv[1]

if test -z $directory
  echo "Please provide a directory as the first argument to this script."
  exit 1
end

rename-js-to-mjs-recursive $directory