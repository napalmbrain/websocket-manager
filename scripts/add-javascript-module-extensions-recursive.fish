function add-javascript-module-extensions -d "Add .mjs extensions to imports and exports."
  set -l file $argv[1]
  sed -r -i '' 's#from "(\./|\.\./)(.*)";#from "\1\2.mjs";#g' $file
end

function add-javascript-module-extensions-recursive -d "Recursively add .mjs extensions to import and exports."
  set -l directory $argv[1]
  for item in (ls $directory)
    if test -d $directory/$item
      add-javascript-module-extensions-recursive $directory/$item
    else if test -f $directory/$item
      add-javascript-module-extensions $directory/$item
    end
  end
end

set -l directory $argv[1]

if test -z $directory
  echo "Please provide a directory as the first argument to this script."
  exit 1
end

add-javascript-module-extensions-recursive $directory
