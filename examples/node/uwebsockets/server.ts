import { App } from "uWebSockets.js";

import { NodeMicroWebSocketServer } from "../../../dist/src/node/uwebsockets/server";
import { STATE, sleep } from "../../../dist/src/index";

const server = new NodeMicroWebSocketServer({ app: App() });

let stop = false;

server.handle("long-running", async (connection, message) => {
  console.log("LONG RUNNING STARTED");
  let state = await connection.state!.promise();
  let count = 0;
  while (!stop && state.status === STATE.connected) {
    state = await connection.state!.promise();
    server.send(connection, {
      id: message.id,
      type: "long-running-response",
      data: { count: count++ },
    });
    await sleep(1000);
  }
});

server.handle("long-running-stop", (connection, message) => {
  stop = true;
  server.send(connection, {
    id: message.id,
    type: "long-running-complete",
  });
});

server.listen();
