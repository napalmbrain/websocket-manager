import { NodeWebSocketClient } from "../../../dist/src/index";

const client = new NodeWebSocketClient();

let id: string;

client.handle("long-running-response", (connection, message) => {
  id = message.id as string;
  connection.actions!.get(message.id as string)?.timeout?.restart();
  connection.actions!.get(message.id as string)?.observable.next(message);
});

client.handle("long-running-complete", (connection, message) => {
  connection.actions!.get(message.id as string)?.observable.complete();
});

const connection = client.connect("ws://localhost:8000");

client.message(
  connection,
  { type: "long-running" },
  [
    {
      next: (message) => {
        console.log(message);
      },
      complete: () => {
        console.log("LONG RUNNING COMPLETE");
      },
    },
  ],
  false
);
