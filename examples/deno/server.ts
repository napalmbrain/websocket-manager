import { DenoWebSocketServer, STATE, sleep } from "./../../mod.ts";

const server = new DenoWebSocketServer();

let stop = false;

server.handle("long-running", async (connection, message) => {
  console.log("LONG RUNNING STARTED");
  let state = await connection.state!.promise();
  let count = 0;
  while (!stop && state.status === STATE.connected) {
    state = await connection.state!.promise();
    server.send(connection, {
      id: message.id,
      type: "long-running-response",
      data: { count: count++ },
    });
    await sleep(1000);
  }
});

server.handle("long-running-stop", (connection, message) => {
  stop = true;
  server.send(connection, {
    id: message.id,
    type: "long-running-complete",
  });
});

server.options.callbacks!.onConnected = () => {
  console.log("CONNECTED");
};

server.options.callbacks!.onDisconnected = () => {
  console.log("DISCONNECTED");
};

server.options.callbacks!.onClose = () => {
  console.log("CLOSE");
};

server.options.callbacks!.onInit = () => {
  console.log("INIT");
};

server.options.callbacks!.onInitAck = () => {
  console.log("INIT ACK");
};

server.listen();
