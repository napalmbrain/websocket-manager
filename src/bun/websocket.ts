export interface ServerWebSocket {
  readonly data: any;
  readonly readyState: number;
  readonly remoteAddress: string;
  getBufferedAmount(): number;
  send(message: string | ArrayBuffer | Uint8Array, compress?: boolean): number;
  close(code?: number, reason?: string): void;
  subscribe(topic: string): void;
  unsubscribe(topic: string): void;
  publish(topic: string, message: string | ArrayBuffer | Uint8Array): void;
  isSubscribed(topic: string): boolean;
  cork(cb: (ws: ServerWebSocket) => void): void;
}

export class WebSocket {
  private ws: ServerWebSocket;

  public onopen?: (event: Event) => void;
  public onmessage?: (event: MessageEvent) => void;
  public onerror?: (event: Event | ErrorEvent) => void;
  public onclose?: (event: CloseEvent) => void;

  constructor(ws: ServerWebSocket) {
    this.ws = ws;
  }

  public get bufferedAmount(): number {
    return this.ws.getBufferedAmount();
  }

  public send(data: string) {
    this.ws.send(data);
  }

  public close(code?: number, reason?: string) {
    this.ws.close(code, reason);
  }
}
