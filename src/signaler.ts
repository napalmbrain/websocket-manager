import { Deferred } from "./common.ts";

export class Signaler<T> {
  public value?: T;
  private deferreds: Deferred<T>[];
  private callbacks: ((value: T) => void)[];

  constructor() {
    this.reset();
    this.deferreds = [];
    this.callbacks = [];
  }

  public get signaled(): boolean {
    return Object.hasOwn(this, "value");
  }

  public reset() {
    delete this.value;
  }

  public promise(): Promise<T> {
    const deferred = {} as Deferred<T>;
    deferred.promise = new Promise<T>((resolve) => {
      deferred.resolve = resolve;
    });
    if (this.signaled) {
      // @ts-ignore: In this branch, `value` is defined on the class instance.
      deferred.resolve(this.value);
    } else {
      this.deferreds.push(deferred);
    }
    return deferred.promise;
  }

  public unpromise(promise: Promise<T>) {
    const deferred = this.deferreds.find(({ promise: p }) => promise === p);
    if (deferred) {
      this.deferreds = this.deferreds.filter((d) => deferred !== d);
    }
  }

  public next(): Promise<T> {
    const deferred = {} as Deferred<T>;
    deferred.promise = new Promise<T>((resolve) => {
      deferred.resolve = resolve;
    });
    this.deferreds.push(deferred);
    return deferred.promise;
  }

  public signal(value: T) {
    this.value = value;
    for (const deferred of this.deferreds) {
      deferred.resolve(this.value);
    }
    this.deferreds = [];
    for (const callback of this.callbacks) {
      callback(this.value);
    }
  }

  public on(callback: (value: T) => void) {
    this.callbacks.push(callback);
  }

  public off(callback: (value: T) => void) {
    this.callbacks = this.callbacks.filter((c) => callback !== c);
  }
}
