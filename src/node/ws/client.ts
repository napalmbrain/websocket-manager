import { WebSocket as WSWebSocket, ClientOptions as WSClientOptions } from "ws";

import { WebSocket } from "./websocket.ts";
import { WebSocketClient } from "../../client.ts";
import { Connection, Generic, MetaFunction } from "../../common.ts";

export class NodeWebSocketClient extends WebSocketClient {
  // @ts-ignore: Ignore type conflict with parent class.
  public connect(
    address: string | URL,
    meta?: MetaFunction,
    protocols?: string[],
    options?: WSClientOptions
  ) {
    const ws = new WSWebSocket(address, protocols, options);
    const socket = new WebSocket(ws);
    const connection: Connection = {
      metaFunction: meta,
      // @ts-ignore: WebSocket is not fully implemented for nodejs.
      socket,
      params: {
        address,
        protocols,
        options,
      },
    };
    this.attach(connection);
    return connection;
  }

  public async reconnect(connection: Connection) {
    await this.close(connection);
    this.detach(connection);
    const ws = new WSWebSocket(
      connection.params!.address as string | URL,
      connection.params!.protocols as string[],
      connection.params!.options as Generic
    );
    // @ts-ignore: Ignore type conflict.
    connection.socket = new WebSocket(ws);
    this.attach(connection);
  }
}
