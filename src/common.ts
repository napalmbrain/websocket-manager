import { Observable } from "./observable.ts";
import { Signaler } from "./signaler.ts";
import { Timeout } from "./timeout.ts";

export const TIMEOUT = 10_000;
export const KEEPALIVE = TIMEOUT / 2;

export type Generic = Record<string | number, unknown>;

export type Message = Generic;

export type Handler = (
  connection: Connection,
  message: Message
) => Promise<void> | void;

export type HandlerCallback = Handler;

export enum PROTOCOL {
  init = "init",
  init_ack = "init_ack",
  ping = "ping",
  pong = "pong",
  meta = "meta",
  meta_ack = "meta_ack",
}

export interface Action<T> {
  message: T;
  observable: Observable<T>;
  timeout?: Timeout;
  close: Signaler<void>;
  status: "active" | "inactive";
}

export type MetaFunction =
  | ((meta: Generic) => Generic)
  | ((meta: Generic) => Promise<Generic>);

export interface Remote {
  manager?: string;
  connection?: string;
}

export interface Connection {
  socket: WebSocket;
  id?: string;
  remote?: Remote;
  actions?: Map<string, Action<Message>>;
  state?: Signaler<State>;
  keepalive?: Timeout;
  timeout?: Timeout;
  meta?: Generic;
  metaFunction?: MetaFunction;
  params?: Generic;
}

export enum STATE {
  connected,
  disconnected,
}

export interface State {
  status: STATE;
}

export interface Deferred<T> {
  promise: Promise<T>;
  resolve: (value: T | PromiseLike<T>) => void;
}
