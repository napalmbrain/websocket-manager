import { WebSocketServer, WebSocketServerOptions } from "../server.ts";
import { Connection } from "../common.ts";

export interface ServeOptions {
  hostname?: string;
  port?: number;
  onListen?: (params: { hostname: string; port: number }) => void;
}

export class DenoWebSocketServer extends WebSocketServer {
  public abortController: AbortController;

  constructor(options: WebSocketServerOptions = {}) {
    super(options);
    this.abortController = new AbortController();
  }

  public upgrade(request: Request): Response {
    const { socket, response } = Deno.upgradeWebSocket(request);
    const connection: Connection = {
      socket,
    };
    this.attach(connection);
    return response;
  }

  public listen(
    options: ServeOptions = {
      hostname: "localhost",
      port: 8000,
    }
  ) {
    // @ts-ignore: use of `Deno` will fail in Bun.
    Deno.serve(
      { signal: this.abortController.signal, onListen: () => {}, ...options },
      (request: Request): Response => {
        const upgrade = request.headers.get("upgrade") || "";
        if (upgrade.toLocaleLowerCase() === "websocket") {
          return this.upgrade(request);
        }
        return new Response("Not Found", { status: 404 });
      }
    );
  }

  public async stop() {
    this.abortController.abort();
    await super.stop();
  }
}
