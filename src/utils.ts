export function isDeno() {
  if (window && "Deno" in window) {
    return true;
  }
  return false;
}

/**
 * Determines if the environment is the browser.
 * @returns true if the environment is the browser, else false.
 */
export function isBrowser() {
  if (window && !isDeno()) {
    return true;
  }
  return false;
}

export function isFunction(object: object) {
  if (typeof object === "function") {
    return true;
  }
  return false;
}

export function sleep(time: number, options: { signal?: AbortSignal } = {}) {
  return new Promise<void>((resolve, reject) => {
    let timeout: unknown;
    const onAbort = () => {
      options.signal?.removeEventListener("abort", onAbort);
      // @ts-ignore: Clear timeout on type `unknown` for compatibility with Deno, Bun and Node.
      clearTimeout(timeout);
      reject();
    };
    options.signal?.addEventListener("abort", onAbort);
    timeout = setTimeout(() => {
      options.signal?.removeEventListener("abort", onAbort);
      resolve();
    }, time);
  });
}

export function keepalive(keepalive: number) {
  return (
    keepalive - keepalive / 2 + Math.floor((keepalive / 2) * Math.random())
  );
}

export function random(min: number, max: number) {
  return Math.round(Math.random() * (max - min) + min);
}

export function limitedAssign(a: object, b: object, limit = 4096): object {
  const alpha = JSON.stringify(a || {});
  const beta = JSON.stringify(b || {});
  const length = alpha.length + beta.length;
  if (length > limit) {
    throw new Error("Object size limit.");
  }
  Object.assign(a, b);
  return a;
}

export function uuidv4(): string {
  return "10000000-1000-4000-8000-100000000000".replace(/[018]/g, (c) =>
    (
      (c as unknown as number) ^
      (crypto.getRandomValues(new Uint8Array(1))[0] &
        (15 >> ((c as unknown as number) / 4)))
    ).toString(16)
  );
}
