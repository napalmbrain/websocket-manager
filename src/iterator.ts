import { WebSocketManager } from "./manager.ts";
import { Connection } from "./common.ts";

export class ConnectionAsyncIterator
  implements AsyncIterableIterator<Connection>
{
  private manager: WebSocketManager;
  private pullQueue: ((data: IteratorResult<Connection, unknown>) => void)[];
  private pushQueue: Connection[];
  private onConnected?: (connection: Connection) => Promise<void> | void;

  constructor(manager: WebSocketManager) {
    this.manager = manager;
    this.pullQueue = [];
    this.pushQueue = [];
    this.onConnected = manager.options.callbacks?.onConnected;
    this.manager.options.callbacks!.onConnected = (connection) => {
      this.pushValue(connection);
      this.onConnected?.(connection);
    };
  }

  [Symbol.asyncIterator]() {
    return this;
  }

  public next(): Promise<IteratorResult<Connection, unknown>> {
    return this.pullValue();
  }

  public return(): Promise<IteratorResult<Connection, unknown>> {
    this.emptyQueue();
    return Promise.resolve({ value: undefined, done: true });
  }

  private pullValue(): Promise<IteratorResult<Connection, unknown>> {
    return new Promise<IteratorResult<Connection, unknown>>((resolve) => {
      if (this.pushQueue.length > 0) {
        resolve({ value: this.pushQueue.shift()!, done: false });
      } else {
        this.pullQueue.push(resolve);
      }
    });
  }

  private pushValue(connection: Connection) {
    if (this.pullQueue.length > 0) {
      this.pullQueue.shift()!({ value: connection, done: false });
    } else {
      this.pushQueue.push(connection);
    }
  }

  private emptyQueue() {
    for (const resolve of this.pullQueue.values()) {
      resolve({ value: undefined, done: true });
    }
    this.pullQueue = [];
    this.pushQueue = [];
  }
}
