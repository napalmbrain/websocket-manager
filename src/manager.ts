import {
  Action,
  Connection,
  Generic,
  Handler,
  HandlerCallback,
  KEEPALIVE,
  Message,
  PROTOCOL,
  Remote,
  STATE,
  State,
  TIMEOUT,
} from "./common.ts";
import { Signaler } from "./signaler.ts";
import { Timeout } from "./timeout.ts";
import { Observable, Observer } from "./observable.ts";
import { random, limitedAssign, uuidv4 } from "./utils.ts";

export interface Callbacks {
  onOpen?: (connection: Connection) => Promise<void> | void;
  onConnected?: (connection: Connection) => Promise<void> | void;
  onDisconnected?: (connection: Connection) => Promise<void> | void;
  onMessage?: (
    connection: Connection,
    message: Message
  ) => Promise<void> | void;
  onError?: (
    connection: Connection,
    event: ErrorEvent | Event
  ) => Promise<void> | void;
  onTimeout?: (connection: Connection) => Promise<void> | void;
  onClose?: (connection: Connection) => Promise<void> | void;
  onInit?: HandlerCallback;
  onInitAck?: HandlerCallback;
  onPing?: HandlerCallback;
  onPong?: HandlerCallback;
  onMeta?: HandlerCallback;
  onMetaAck?: HandlerCallback;
}

export interface WebSocketManagerOptions {
  callbacks?: Callbacks;
  enableKeepalive?: boolean;
  keepalive?: number;
  timeout?: number;
}

export class WebSocketManager {
  public options: WebSocketManagerOptions;

  public connections: Map<string, Connection>;
  protected handlers: Map<string, Handler>;

  public id: string;

  constructor(options?: WebSocketManagerOptions) {
    this.options = Object.assign(
      {
        keepalive: KEEPALIVE,
        timeout: TIMEOUT,
        callbacks: {},
      },
      options
    );
    this.id = uuidv4();
    this.connections = new Map<string, Connection>();
    this.handlers = new Map<string, Handler>();
    this.handle(PROTOCOL.init, (connection, message) => {
      try {
        connection.meta = limitedAssign(
          connection.meta!,
          message.meta as object
        ) as Generic;
      } catch (error) {
        console.log("[WebSocketManager handle(PROTOCOL.init)]: ", error);
        delete connection.meta;
        delete message.meta;
        this.close(connection);
        return;
      }
      this.send(connection, {
        id: message.id,
        type: PROTOCOL.init_ack,
        remote: {
          manager: this.id,
          connection: connection.id,
        },
        meta: connection.meta!,
      });
      this.options.callbacks?.onInit?.(connection, message);
    });
    this.handle(PROTOCOL.init_ack, (connection, message) => {
      try {
        connection.meta = limitedAssign(
          connection.meta!,
          message.meta as object
        ) as Generic;
      } catch (error) {
        console.log("[WebSocketManager handle(PROTOCOL.init_ack)]: ", error);
        delete connection.meta;
        delete message.meta;
        this.close(connection);
        return;
      }
      this.options.callbacks?.onInitAck?.(connection, message);
      const remote = message.remote as Remote;
      connection.remote = { ...remote };
      connection.actions?.get(message.id as string)?.observable.next(message);
      connection.actions?.get(message.id as string)?.observable.complete();
    });
    this.handle(PROTOCOL.ping, (connection, message) => {
      connection.timeout?.restart();
      this.send(connection, {
        id: message.id,
        type: PROTOCOL.pong,
        data: message.data,
      });
      this.options.callbacks?.onPing?.(connection, message);
    });
    this.handle(PROTOCOL.pong, (connection, message) => {
      connection.timeout?.restart();
      connection.actions?.get(message.id as string)?.observable.next(message);
      connection.actions?.get(message.id as string)?.observable.complete();
      this.options.callbacks?.onPong?.(connection, message);
    });
    this.handle(PROTOCOL.meta, (connection, message) => {
      try {
        connection.meta = limitedAssign(
          connection.meta!,
          message.meta as object
        ) as Generic;
      } catch (error) {
        console.log("[WebSocketManager handle(PROTOCOL.meta)]: ", error);
        delete connection.meta;
        delete message.meta;
        this.close(connection);
        return;
      }
      this.send(connection, {
        id: message.id,
        type: PROTOCOL.meta_ack,
        meta: connection.meta!,
      });
      this.options.callbacks?.onMeta?.(connection, message);
    });
    this.handle(PROTOCOL.meta_ack, (connection, message) => {
      try {
        connection.meta = limitedAssign(
          connection.meta!,
          message.meta as object
        ) as Generic;
      } catch (error) {
        console.log("[WebSocketManager handle(PROTOCOL.meta_ack)]: ", error);
        delete connection.meta;
        delete message.meta;
        this.close(connection);
        return;
      }
      connection.actions?.get(message.id as string)?.observable.next(message);
      connection.actions?.get(message.id as string)?.observable.complete();
      this.options.callbacks?.onMetaAck?.(connection, message);
    });
  }

  public init(connection: Connection) {
    if (!connection.actions) {
      connection.actions = new Map<string, Action<Message>>();
    }
    if (!connection.state) {
      connection.state = new Signaler<State>();
      connection.state.on(({ status }) => {
        if (status === STATE.connected) {
          for (const action of connection.actions!.values()) {
            if (
              ![PROTOCOL.init, PROTOCOL.init_ack].includes(
                action.message.type as PROTOCOL
              )
            ) {
              this.dispatch(connection, action);
            }
          }
          this.options.callbacks?.onConnected?.(connection);
        } else if (status === STATE.disconnected) {
          for (const action of connection.actions!.values()) {
            action.timeout?.stop();
            action.status = "inactive";
          }
          this.options.callbacks?.onDisconnected?.(connection);
        }
      });
    } else {
      connection.state.reset();
    }
    if (this.options.timeout && this.options.timeout > 0) {
      if (!connection.timeout) {
        connection.timeout = new Timeout(() => {
          this.options.callbacks?.onTimeout?.(connection);
          // XXX: Simulate a close event.
          // @ts-ignore: Do not provide an `event` parameter to `onclose`.
          connection.socket.onclose?.();
        }, this.options.timeout!);
      } else {
        connection.timeout.restart();
      }
    }
    if (this.options.keepalive && this.options.keepalive > 0) {
      if (!connection.keepalive) {
        connection.keepalive = new Timeout(() => {
          this.message(connection, { type: PROTOCOL.ping });
          connection.keepalive!.timeout = random(0, this.options.keepalive!);
          connection.keepalive!.restart();
        }, random(0, this.options.keepalive!));
      } else {
        connection.keepalive.restart();
      }
    }
    if (!connection.meta) {
      connection.meta = {};
    }
    if (!connection.params) {
      connection.params = {};
    }
  }

  public deinit(connection: Connection) {
    connection.keepalive?.stop();
    connection.timeout?.stop();
    this.close(connection);
    if (connection.state!.value?.status !== STATE.disconnected) {
      connection.state!.signal({ status: STATE.disconnected });
    }
  }

  public bind(connection: Connection) {
    connection.socket.onmessage = this.onmessage.bind(this, connection);
    connection.socket.onerror = this.onerror.bind(this, connection);
    connection.socket.onclose = this.onclose.bind(this, connection);
    connection.socket.onopen = this.onopen.bind(this, connection);
  }

  public unbind(connection: Connection) {
    connection.socket.onmessage = null;
    connection.socket.onerror = null;
    connection.socket.onclose = null;
    connection.socket.onopen = null;
  }

  public attach(connection: Connection) {
    if (!connection.id || this.connections.has(connection.id)) {
      let id: string;
      do {
        id = uuidv4();
      } while (this.connections.has(id));
      connection.id = id;
    }
    this.init(connection);
    this.bind(connection);
    this.connections.set(connection.id, connection);
  }

  public detach(connection: Connection) {
    this.unbind(connection);
    this.deinit(connection);
    this.connections.delete(connection.id!);
  }

  public handle(type: string, handler: Handler) {
    this.handlers.set(type, handler);
  }

  public send(connection: Connection, message: Message) {
    try {
      connection.socket.send(JSON.stringify(message));
    } catch (error) {
      console.log("[WebSocketManager send]: failed.", { error, message });
    }
  }

  public sendOrThrow(connection: Connection, message: Message) {
    connection.socket.send(JSON.stringify(message));
  }

  public async close(
    connection: Connection,
    code?: number,
    reason?: string
  ): Promise<State> {
    let promise = connection.state!.promise();
    const { status } = await promise;
    if (status === STATE.connected) {
      promise = connection.state!.next();
      connection.socket.close(code, reason);
    }
    return promise;
  }

  public action(
    connection: Connection,
    message: Message,
    observers: Observer<Message>[] = [],
    timeout = TIMEOUT
  ): Action<Message> {
    do {
      message.id = uuidv4();
    } while (connection.actions?.has(message.id as string));
    const action: Action<Message> = {
      message,
      observable: new Observable<Message>(observers),
      close: new Signaler<void>(),
      status: "inactive",
    };
    if (timeout && timeout > 0) {
      action.timeout = new Timeout(() => {
        action.observable.error({
          errors: ["Action timed out."],
          action,
        });
        action.observable.complete();
      }, timeout);
    }
    action.timeout?.stop();
    action.observable.push({
      error: (data) => {
        console.log("[WebSocketManager action]: error.", data);
      },
      complete: () => {
        this.remove(connection, action);
      },
    });
    this.add(connection, action);
    return action;
  }

  public add(connection: Connection, action: Action<Message>) {
    connection.actions?.set(action.message.id as string, action);
  }

  public remove(connection: Connection, action: Action<Message>) {
    action.timeout?.stop();
    action.close.signal();
    connection.actions?.delete(action.message.id as string);
  }

  public dispatch(connection: Connection, action: Action<Message>) {
    if (action.status === "inactive") {
      action.status = "active";
      action.timeout?.restart();
      try {
        this.sendOrThrow(connection, action.message);
      } catch (error) {
        action.status = "inactive";
        action.timeout?.stop();
        console.log("[WebSocketManager dispatch]: failed.", { error, action });
      }
    }
  }

  public message(
    connection: Connection,
    message: Message,
    observers: Observer<Message>[] = [],
    timeout = TIMEOUT
  ): Action<Message> {
    const action = this.action(connection, message, observers, timeout);
    connection.state!.promise().then(({ status }) => {
      if (status === STATE.connected) {
        this.dispatch(connection, action);
      }
    });
    return action;
  }

  public ping(
    connection: Connection,
    payload: Generic,
    observers: Observer<Generic>[] = []
  ): Promise<Generic> {
    return new Promise<Generic>((resolve) => {
      let result: Generic;
      observers.push({
        next: (message) => {
          result = message.data as Generic;
        },
        complete: () => {
          resolve(result);
        },
      });
      this.message(
        connection,
        { type: PROTOCOL.ping, data: payload },
        observers
      );
    });
  }

  public meta(
    connection: Connection,
    payload: Generic = {},
    observers: Observer<Generic>[] = []
  ): Promise<Generic> {
    return new Promise<Generic>((resolve) => {
      let result: Generic;
      observers.push({
        next: (message) => {
          result = message.meta as Generic;
        },
        complete: () => {
          resolve(result);
        },
      });
      this.message(
        connection,
        { type: PROTOCOL.meta, meta: payload },
        observers
      );
    });
  }

  public async stop() {
    const promises: Promise<State>[] = [];
    for (const connection of this.connections.values()) {
      const promise = this.close(connection).then((state) => {
        this.detach(connection);
        return state;
      });
      promises.push(promise);
    }
    await Promise.all(promises);
  }

  protected async onopen(connection: Connection, _event: Event) {
    let error = false;
    const action = this.action(
      connection,
      {
        type: PROTOCOL.init,
        meta: await connection.metaFunction?.(connection.meta!),
      },
      [
        {
          error: () => {
            error = true;
          },
          complete: () => {
            if (error) {
              // Simulate `onclose`.
              // @ts-ignore: don't pass a CloseEvent to `onclose`.
              connection.socket.onclose?.();
            } else {
              connection.state!.signal({ status: STATE.connected });
            }
          },
        },
      ]
    );
    this.dispatch(connection, action);
    this.options.callbacks?.onOpen?.(connection);
  }

  protected onmessage(connection: Connection, event: MessageEvent) {
    const message = JSON.parse(event.data);
    this.handlers.get(message.type)?.(connection, message);
    this.options.callbacks?.onMessage?.(connection, message);
  }

  protected onerror(connection: Connection, event: Event | ErrorEvent) {
    this.options.callbacks?.onError?.(connection, event);
  }

  protected onclose(connection: Connection, _event: CloseEvent) {
    this.options.callbacks?.onClose?.(connection);
    this.detach(connection);
  }
}
