import {
  beforeEach,
  afterEach,
  describe,
  it,
  expect,
  FakeTime,
  mock,
} from "../../test_deps.ts";

import { Signaler } from "../../src/signaler.ts";
import { STATE, State } from "../../src/common.ts";

describe("signaler", () => {
  let time: FakeTime;

  beforeEach(() => {
    time = new FakeTime();
  });

  afterEach(() => {
    time.restore();
  });

  it("can signal", () => {
    const signaler = new Signaler<State>();
    setTimeout(() => {
      signaler.signal({ status: STATE.connected });
    }, 50);
    signaler.promise().then(({ status }) => {
      expect(status).toBe(STATE.connected);
    });
    time.next();
  });

  it("can update signal", async () => {
    const signaler = new Signaler<State>();
    signaler.signal({ status: STATE.disconnected });
    signaler.signal({ status: STATE.connected });
    const { status } = await signaler.promise();
    expect(status).toBe(STATE.connected);
  });

  it("can reset", () => {
    const signaler = new Signaler<State>();
    signaler.signal({ status: STATE.disconnected });
    signaler.reset();
    expect(signaler.value).toBeUndefined();
  });

  it("can wait for the next signal", () => {
    const signaler = new Signaler<State>();
    signaler.signal({ status: STATE.connected });
    setTimeout(() => {
      signaler.signal({ status: STATE.disconnected });
    }, 50);
    signaler.next().then(({ status }) => {
      expect(status).toEqual(STATE.disconnected);
    });
    time.next();
  });

  it("can signal a falsy value", async () => {
    const fn = mock.fn();
    const signaler = new Signaler<void>();
    signaler.signal();
    await signaler.promise().then(fn);
    expect(fn).toHaveBeenCalled();
  });

  it("can use the signaled getter", () => {
    const signaler = new Signaler<void>();
    signaler.signal();
    expect(signaler.signaled).toBeTruthy();
  });
});
