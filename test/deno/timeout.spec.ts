import {
  beforeEach,
  afterEach,
  describe,
  it,
  expect,
  mock,
  FakeTime,
} from "../../test_deps.ts";

import { Timeout } from "../../src/timeout.ts";

describe("timeout", () => {
  let time: FakeTime;

  beforeEach(() => {
    time = new FakeTime();
  });

  afterEach(() => {
    time.restore();
  });

  it("can start automatically", () => {
    const fn = mock.fn();
    new Timeout(fn, 50);
    time.next();
    expect(fn).toHaveBeenCalled();
  });

  it("can be stopped", () => {
    const fn = mock.fn();
    const timeout = new Timeout(fn, 50);
    timeout.stop();
    time.next();
    expect(fn).not.toHaveBeenCalled();
  });

  it("can be started", () => {
    const fn = mock.fn();
    const timeout = new Timeout(fn, 50);
    timeout.stop();
    timeout.start();
    time.next();
    expect(fn).toHaveBeenCalled();
  });

  it("can be restarted", () => {
    const fn = mock.fn();
    const timeout = new Timeout(fn, 50);
    timeout.restart();
    time.next();
    expect(fn).toHaveBeenCalled();
  });

  it("can attach callbacks with `on`", () => {
    const fn = mock.fn();
    const timeout = new Timeout(() => {}, 50);
    timeout.on(fn);
    time.next();
    expect(fn).toHaveBeenCalled();
  });
});
