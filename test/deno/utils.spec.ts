import {
  beforeEach,
  afterEach,
  describe,
  it,
  expect,
  mock,
  FakeTime,
} from "../../test_deps.ts";
import { sleep, limitedAssign } from "../../src/utils.ts";

describe("sleep", () => {
  let time: FakeTime;

  beforeEach(() => {
    time = new FakeTime();
  });

  afterEach(() => {
    time.restore();
  });

  it("can wait for a specified time", async () => {
    const fn = mock.fn();
    const promise = sleep(1000).then(fn);
    time.next();
    await promise;
    expect(fn).toHaveBeenCalled();
  });

  it("can be aborted", async () => {
    const fn = mock.fn();
    const controller = new AbortController();
    const promise = sleep(1000, { signal: controller.signal })
      .then(fn)
      .catch(() => {
        /* ignore promise rejection */
      });
    controller.abort();
    time.next();
    await promise;
    expect(fn).not.toHaveBeenCalled();
  });
});

describe("limitedAssign", () => {
  it("can assign properties within a size limit", () => {
    const a = { alpha: "a" };
    const b = { alpha: "alpha", beta: "beta" };
    limitedAssign(a, b);
    expect(a).toEqual({ alpha: "alpha", beta: "beta" });
  });

  it("can throw when an object size exceeds a size limit", () => {
    const array = Array.from({ length: 4096 }, () => 0);
    const a = { alpha: "a" };
    const b = { alpha: "alpha", beta: "beta", array };
    expect(() => limitedAssign(a, b)).toThrow("Object size limit.");
  });
});
