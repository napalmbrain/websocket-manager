import { describe, it, expect, mock } from "../../test_deps.ts";

import { Observable, Observer } from "../../src/observable.ts";

describe("observable", () => {
  it("can be initialized without observers", () => {
    const observable = new Observable<void>();
    expect(observable).toBeDefined();
  });

  it("can be initialized with observers", () => {
    const fn = mock.fn();
    const observable = new Observable<void>([
      {
        complete: fn,
      },
    ]);
    observable.complete();
    expect(fn).toHaveBeenCalled();
  });

  it("can call next", () => {
    const fn = mock.fn();
    const observable = new Observable<void>([
      {
        next: fn,
      },
    ]);
    observable.next();
    expect(fn).toHaveBeenCalled();
    expect(observable.observers.length).toEqual(1);
  });

  it("can call error", () => {
    const fn = mock.fn();
    const observable = new Observable<void>([
      {
        error: fn,
      },
    ]);
    observable.error();
    expect(fn).toHaveBeenCalled();
    expect(observable.observers.length).toEqual(1);
  });

  it("can call complete", () => {
    const fn = mock.fn();
    const observable = new Observable<void>([
      {
        complete: fn,
      },
    ]);
    observable.complete();
    expect(fn).toHaveBeenCalled();
    expect(observable.observers.length).toEqual(0);
  });

  it("can push observers", () => {
    const alpha = mock.fn();
    const beta = mock.fn();
    const observerOne = {
      complete: alpha,
    };
    const observerTwo = {
      complete: beta,
    };
    const observable = new Observable<void>([observerOne]);
    observable.push(observerTwo);
    expect(observable.observers[0]).toBe(observerOne);
    expect(observable.observers[1]).toBe(observerTwo);
    observable.complete();
    expect(alpha).toHaveBeenCalled();
    expect(beta).toHaveBeenCalled();
  });

  it("can unshift observers", () => {
    const alpha = mock.fn();
    const beta = mock.fn();
    const observerOne = {
      complete: alpha,
    };
    const observerTwo = {
      complete: beta,
    };
    const observable = new Observable<void>([observerOne]);
    observable.unshift(observerTwo);
    expect(observable.observers[0]).toBe(observerTwo);
    expect(observable.observers[1]).toBe(observerOne);
    observable.complete();
    expect(alpha).toHaveBeenCalled();
    expect(beta).toHaveBeenCalled();
  });

  it("can remove observers", () => {
    const alpha = mock.fn();
    const beta = mock.fn();
    const observable = new Observable<void>([
      {
        complete: alpha,
      },
    ]);
    const observer: Observer<void> = {
      complete: beta,
    };
    observable.push(observer);
    observable.unobserve(observer);
    observable.complete();
    expect(alpha).toHaveBeenCalled();
    expect(beta).not.toHaveBeenCalled();
  });
});
