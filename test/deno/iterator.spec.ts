import {
  describe,
  it,
  beforeEach,
  afterEach,
  expect,
} from "../../test_deps.ts";

import { ConnectionAsyncIterator } from "../../src/iterator.ts";
import { DenoWebSocketServer } from "../../src/deno/server.ts";
import { WebSocketClient } from "../../src/client.ts";

describe("iterator", () => {
  const hostname = "localhost";
  const port = 8001;
  const url = new URL(`ws://${hostname}:${port}`);
  let server: DenoWebSocketServer;
  let client: WebSocketClient;

  beforeEach(() => {
    server = new DenoWebSocketServer();
    server.listen({
      hostname,
      port,
    });
    client = new WebSocketClient({ reconnect: false });
  });

  afterEach(async () => {
    await Promise.all([client.stop(), server.stop()]);
  });

  it("can iterate over new connections", async () => {
    const serverIterator = new ConnectionAsyncIterator(server);
    const clientIterator = new ConnectionAsyncIterator(client);
    client.connect(url);
    const [serverConnection, clientConnection] = (
      await Promise.all([serverIterator.next(), clientIterator.next()])
    ).map(({ value }) => value);
    expect(serverConnection).toBeDefined();
    expect(clientConnection).toBeDefined();
    await clientIterator.return();
    await serverIterator.return();
  });
});
