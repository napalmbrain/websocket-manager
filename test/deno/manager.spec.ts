import {
  describe,
  it,
  beforeEach,
  afterEach,
  expect,
  mock,
} from "../../test_deps.ts";

import { DenoWebSocketServer } from "../../src/deno/server.ts";
import { WebSocketClient } from "../../src/client.ts";
import { Connection, Message } from "../../src/common.ts";
import { ConnectionAsyncIterator, STATE } from "../../mod.ts";

describe("manager", () => {
  const hostname = "localhost";
  const port = 8001;
  const url = new URL(`ws://${hostname}:${port}`);
  let server: DenoWebSocketServer;
  let client: WebSocketClient;
  let serverIterator: ConnectionAsyncIterator;
  let clientIterator: ConnectionAsyncIterator;
  const connected = async (): Promise<Connection[]> => {
    return (
      await Promise.all([serverIterator.next(), clientIterator.next()])
    ).map(({ value }) => value) as Connection[];
  };

  beforeEach(() => {
    server = new DenoWebSocketServer();
    server.listen({
      hostname,
      port,
    });
    client = new WebSocketClient({ reconnect: false });
    serverIterator = new ConnectionAsyncIterator(server);
    clientIterator = new ConnectionAsyncIterator(client);
  });

  afterEach(async () => {
    await Promise.all([
      serverIterator.return(),
      clientIterator.return(),
      client.stop(),
      server.stop(),
    ]);
  });

  it("can attach and detach a connection without a keepalive", async () => {
    const connection: Connection = {
      socket: new WebSocket(url),
    };
    client.attach(connection);
    await connection.state!.promise();
    expect(connection.state).toBeDefined();
    expect(connection.timeout).toBeDefined();
    expect(connection.timeout!.state).toEqual("started");
    expect(connection.keepalive).not.toBeDefined();
    expect(connection.meta).toBeDefined();
    expect(connection.params).toBeDefined();
    client.detach(connection);
    expect(connection.timeout!.state).toEqual("stopped");
  });

  it("can attach and detach a connection with a keepalive", async () => {
    const connection: Connection = {
      socket: new WebSocket(url),
    };
    server.attach(connection);
    await connection.state!.promise();
    expect(connection.state).toBeDefined();
    expect(connection.timeout).toBeDefined();
    expect(connection.timeout!.state).toEqual("started");
    expect(connection.keepalive).toBeDefined();
    expect(connection.keepalive!.state).toEqual("started");
    expect(connection.meta).toBeDefined();
    expect(connection.params).toBeDefined();
    server.detach(connection);
    expect(connection.timeout!.state).toEqual("stopped");
    expect(connection.keepalive!.state).toEqual("stopped");
  });

  it("can set a connection's id on `attach`", async () => {
    const connection: Connection = {
      socket: new WebSocket(url),
    };
    client.attach(connection);
    await connection.state!.promise();
    expect(connection.id).toBeDefined();
    client.detach(connection);
  });

  it("can set a connection's remote id on `init_ack`", async () => {
    const connection: Connection = {
      socket: new WebSocket(url),
    };
    client.attach(connection);
    await connection.state!.promise();
    expect(connection.remote?.manager).toBeDefined();
    expect(connection.remote?.connection).toBeDefined();
    client.detach(connection);
  });

  it("can add handlers", async () => {
    const test = mock.fn((connection: Connection, message: Message) => {
      connection.socket.send(
        JSON.stringify({ id: message.id as string, type: "test_ack" })
      );
    });
    const test_ack = mock.fn((connection: Connection, message: Message) => {
      connection.actions!.get(message.id as string)?.observable.complete();
    });
    server.handle("test", test);
    client.handle("test_ack", test_ack);
    const connection = client.connect(url);
    await connected();
    await new Promise<void>((resolve) => {
      client.message(connection, { type: "test" }, [
        {
          complete: resolve,
        },
      ]);
    });
    expect(test).toHaveBeenCalled();
    expect(test_ack).toHaveBeenCalled();
  });

  it("can create an action", async () => {
    const connection = client.connect(url);
    await connected();
    const action = client.action(connection, { type: "test" });
    expect(action.message).toBeDefined();
    expect(action.message.id).toBeDefined();
    expect(action.message.type).toEqual("test");
    expect(action.observable).toBeDefined();
    expect(action.timeout).toBeDefined();
    expect(action.timeout!.state).toEqual("stopped");
    expect(action.close).toBeDefined();
    expect(connection.actions?.has(action.message.id as string)).toBeTruthy();
  });

  it("can stop an action's timeout and signal closed when complete", async () => {
    const fn = mock.fn();
    const connection = client.connect(url);
    const action = client.action(connection, { type: "test" });
    action.observable.complete();
    await action.close.promise().then(fn);
    expect(action.timeout!.state).toEqual("stopped");
    expect(fn).toHaveBeenCalled();
  });

  it("can be initialized with meta data", async () => {
    const connection = client.connect(url, (meta) => ({
      ...meta,
      test: "ing",
    }));
    await connection.state!.promise();
    expect(connection.meta).toEqual({ test: "ing" });
  });

  it("can update it's meta data", async () => {
    const connection = client.connect(url, (meta) => ({
      ...meta,
      test: "ing",
    }));
    await connection.state!.promise();
    await client.meta(connection, { test: "123" });
    expect(connection.meta).toEqual({ test: "123" });
  });

  it("can get it's meta data", async () => {
    const connection = client.connect(url, (meta) => ({
      ...meta,
      test: "ing",
    }));
    await connection.state!.promise();
    const meta = await client.meta(connection);
    expect(meta).toEqual({ test: "ing" });
    expect(connection.meta).toEqual({ test: "ing" });
  });

  it("can close a connection", async () => {
    client.connect(url);
    const [serverConnection, clientConnection] = await connected();
    const statuses = [
      serverConnection.state!.next(),
      clientConnection.state!.next(),
    ];
    client.close(clientConnection);
    server.stop(); // XXX: this shouldn't be necessary.
    const [{ status: serverStatus }, { status: clientStatus }] =
      await Promise.all(statuses);
    expect(serverStatus).toEqual(STATE.disconnected);
    expect(clientStatus).toEqual(STATE.disconnected);
  });

  it("can spam close on a connection", async () => {
    client.connect(url);
    const [serverConnection, clientConnection] = await connected();
    const statuses = [
      serverConnection.state!.next(),
      clientConnection.state!.next(),
    ];
    client.close(clientConnection);
    client.close(clientConnection);
    client.close(clientConnection);
    server.stop(); // XXX: this shouldn't be necessary.
    const [{ status: serverStatus }, { status: clientStatus }] =
      await Promise.all(statuses);
    expect(serverStatus).toEqual(STATE.disconnected);
    expect(clientStatus).toEqual(STATE.disconnected);
  });

  it("can send `init` and `init_ack`", async () => {
    const serverInit = mock.fn();
    const serverInitAck = mock.fn();
    const clientInit = mock.fn();
    const clientInitAck = mock.fn();
    server.options.callbacks!.onInit = serverInit;
    server.options.callbacks!.onInitAck = serverInitAck;
    client.options.callbacks!.onInit = clientInit;
    client.options.callbacks!.onInitAck = clientInitAck;
    client.connect(url);
    await connected();
    expect(serverInit).toHaveBeenCalledTimes(1);
    expect(serverInitAck).toHaveBeenCalledTimes(1);
    expect(clientInit).toHaveBeenCalledTimes(1);
    expect(clientInitAck).toHaveBeenCalledTimes(1);
  });

  it("can send `ping` and `pong`", async () => {
    const clientPing = mock.fn();
    const serverPong = mock.fn();
    client.options.callbacks!.onPing = clientPing;
    server.options.callbacks!.onPong = serverPong;
    client.connect(url);
    const [serverConnection, _] = await connected();
    const response = await server.ping(serverConnection, { test: "ing" });
    expect(response.test).toEqual("ing");
    expect(clientPing).toHaveBeenCalledTimes(1);
    expect(serverPong).toHaveBeenCalledTimes(1);
  });

  it("can send `meta` and `meta_ack`", async () => {
    const serverMeta = mock.fn();
    const clientMetaAck = mock.fn();
    server.options.callbacks!.onMeta = serverMeta;
    client.options.callbacks!.onMetaAck = clientMetaAck;
    client.connect(url);
    const [serverConnection, clientConnection] = await connected();
    const result = await client.meta(clientConnection, { test: "value" });
    expect(result.test).toEqual("value");
    expect(serverConnection.meta!.test).toEqual("value");
    expect(clientConnection.meta!.test).toEqual("value");
    expect(serverMeta).toHaveBeenCalledTimes(1);
    expect(clientMetaAck).toHaveBeenCalledTimes(1);
  });
});
