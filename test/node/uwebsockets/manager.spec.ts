import { getPort } from "get-port-please";
import {
  App,
  TemplatedApp,
  us_listen_socket,
  us_listen_socket_close,
} from "uWebSockets.js";

import { NodeMicroWebSocketServer } from "../../../dist/src/node/uwebsockets/server";
import { NodeWebSocketClient } from "../../../dist/src/node/ws/client";
import { Connection } from "../../../dist/src/common";
import { ConnectionAsyncIterator } from "../../../dist/src/iterator";

describe("server", () => {
  const hostname = "localhost";
  let port: number;
  let url: string;
  let app: TemplatedApp;
  let listenSocket: us_listen_socket | false;
  let server: NodeMicroWebSocketServer;
  let client: NodeWebSocketClient;
  let serverIterator: ConnectionAsyncIterator;
  let clientIterator: ConnectionAsyncIterator;
  const connected = async (): Promise<Connection[]> => {
    return (
      await Promise.all([serverIterator.next(), clientIterator.next()])
    ).map(({ value }) => value) as Connection[];
  };

  beforeEach(async () => {
    port = await getPort();
    url = `ws://${hostname}:${port}`;
    app = App();
    server = new NodeMicroWebSocketServer();
    client = new NodeWebSocketClient({ reconnect: false });
    server.listen({ app });
    listenSocket = await new Promise<us_listen_socket | false>((resolve) => {
      app.listen("127.0.0.1", port, (listenSocket) => {
        resolve(listenSocket);
      });
    });
    serverIterator = new ConnectionAsyncIterator(server);
    clientIterator = new ConnectionAsyncIterator(client);
  });

  afterEach(async () => {
    await serverIterator.return();
    await clientIterator.return();
    await client.stop();
    await server.stop();
    us_listen_socket_close(listenSocket);
  });

  it("can send `init` and `init_ack`", async () => {
    const serverInit = jest.fn();
    const serverInitAck = jest.fn();
    const clientInit = jest.fn();
    const clientInitAck = jest.fn();
    server.options.callbacks!.onInit = serverInit;
    server.options.callbacks!.onInitAck = serverInitAck;
    client.options.callbacks!.onInit = clientInit;
    client.options.callbacks!.onInitAck = clientInitAck;
    client.connect(url);
    await connected();
    expect(serverInit).toHaveBeenCalledTimes(1);
    expect(serverInitAck).toHaveBeenCalledTimes(1);
    expect(clientInit).toHaveBeenCalledTimes(1);
    expect(clientInitAck).toHaveBeenCalledTimes(1);
  });

  it("can send `ping` and `pong`", async () => {
    const clientPing = jest.fn();
    const serverPong = jest.fn();
    client.options.callbacks!.onPing = clientPing;
    server.options.callbacks!.onPong = serverPong;
    client.connect(url);
    const [serverConnection, _] = await connected();
    const response = await server.ping(serverConnection, { test: "ing" });
    expect(clientPing).toHaveBeenCalledTimes(1);
    expect(serverPong).toHaveBeenCalledTimes(1);
    expect(response.test).toEqual("ing");
  });

  it("can send `meta` and `meta_ack`", async () => {
    const serverMeta = jest.fn();
    const clientMetaAck = jest.fn();
    server.options.callbacks!.onMeta = serverMeta;
    client.options.callbacks!.onMetaAck = clientMetaAck;
    client.connect(url);
    const [_, clientConnection] = await connected();
    const result = await client.meta(clientConnection, { test: "value" });
    expect(result.test).toEqual("value");
    expect(serverMeta).toHaveBeenCalledTimes(1);
    expect(clientMetaAck).toHaveBeenCalledTimes(1);
  });
});
