import { getPort } from "get-port-please";

import { NodeWebSocketServer } from "../../../dist/src/node/ws/server";
import { NodeWebSocketClient } from "../../../dist/src/node/ws/client";
import { Connection, Message, STATE } from "../../../dist/src/common";
import { ConnectionAsyncIterator } from "../../../dist/src/iterator";
import { sleep } from "../../../dist/src/utils";

describe("client", () => {
  const hostname = "localhost";
  let port: number;
  let url: string;
  let server: NodeWebSocketServer;
  let client: NodeWebSocketClient;
  let serverIterator: ConnectionAsyncIterator;
  let clientIterator: ConnectionAsyncIterator;
  const connected = async (): Promise<Connection[]> => {
    return (
      await Promise.all([serverIterator.next(), clientIterator.next()])
    ).map(({ value }) => value) as Connection[];
  };

  beforeEach(async () => {
    port = await getPort();
    url = `ws://${hostname}:${port}`;
    server = new NodeWebSocketServer();
    server.listen({ port });
    client = new NodeWebSocketClient({ reconnect: false });
    serverIterator = new ConnectionAsyncIterator(server);
    clientIterator = new ConnectionAsyncIterator(client);
  });

  afterEach(async () => {
    await serverIterator.return();
    await clientIterator.return();
    await client.stop();
    await server.stop();
  });

  it("can reconnect", async () => {
    client.connect(url);
    const [_, clientConnection] = await connected();
    let result = await client.close(clientConnection);
    expect(result.status).toEqual(STATE.disconnected);
    await client.reconnect(clientConnection);
    result = await clientConnection.state!.promise();
    expect(result.status).toEqual(STATE.connected);
  });

  it("can spam reconnect", async () => {
    client.connect(url);
    const [_, clientConnection] = await connected();
    let result = await client.close(clientConnection);
    expect(result.status).toEqual(STATE.disconnected);
    await client.reconnect(clientConnection);
    await client.reconnect(clientConnection);
    await client.reconnect(clientConnection);
    result = await clientConnection.state!.promise();
    expect(result.status).toEqual(STATE.connected);
  });

  it("can resume actions on reconnect", async () => {
    const next = jest.fn((message: Message) => {
      //console.log(message);
    });
    let stop = false;
    let id: string;
    client.connect(url);
    const [serverConnection1, clientConnection1] = await connected();
    server.handle("long-running", async (connection, message) => {
      stop = false;
      let count = 0;
      const controller = new AbortController();
      while (!stop && connection.state?.value?.status === STATE.connected) {
        server.send(connection, {
          id: message.id,
          type: "long-running-response",
          data: { count: count++ },
        });
        await Promise.race([
          sleep(1000, { signal: controller.signal }),
          connection.state!.next(),
        ]);
      }
      controller.abort();
    });
    server.handle("long-running-stop", (connection, message) => {
      stop = true;
      server.send(connection, {
        id: message.id,
        type: "long-running-complete",
      });
    });
    client.handle("long-running-response", (connection, message) => {
      id = message.id as string;
      connection.actions!.get(message.id as string)?.timeout!.restart();
      connection.actions!.get(message.id as string)?.observable.next(message);
    });
    client.handle("long-running-complete", (connection, message) => {
      connection.actions!.get(message.id as string)?.observable.complete();
    });
    const promise = new Promise<void>((resolve) => {
      let count = 0;
      const action = client.action(
        clientConnection1,
        { type: "long-running" },
        []
      );
      action.observable.push({
        //next,
        next: (message) => {
          next(message);
          count++;
          if (count === 2) {
            client.send(clientConnection2, { id, type: "long-running-stop" });
          }
        },
        complete: resolve,
      });
      client.dispatch(clientConnection1, action);
    });
    //await sleep(900);
    await client.reconnect(clientConnection1);
    const [serverConnection2, clientConnection2] = await connected();
    //await sleep(10000);
    // @ts-ignore: `id` is set.
    await promise;
    expect(serverConnection1).not.toEqual(serverConnection2);
    expect(clientConnection1).toEqual(clientConnection2);
    expect(next).toHaveBeenCalledTimes(2);
  });
});
