import { describe, it, expect, mock, beforeEach, afterEach } from "bun:test";

import { BunWebSocketServer } from "../../src/bun/server.ts";
import { WebSocketClient } from "../../src/client.ts";
import { Connection, Message, STATE } from "../../src/common.ts";
import { ConnectionAsyncIterator } from "../../src/iterator.ts";
import { sleep } from "../../src/utils.ts";

describe("client", () => {
  const hostname = "localhost";
  const port = 8001;
  const url = new URL(`ws://${hostname}:${port}`);
  let server: BunWebSocketServer;
  let client: WebSocketClient;
  let serverIterator: ConnectionAsyncIterator;
  let clientIterator: ConnectionAsyncIterator;
  const connected = async (): Promise<Connection[]> => {
    return (
      await Promise.all([serverIterator.next(), clientIterator.next()])
    ).map(({ value }) => value) as Connection[];
  };

  beforeEach(() => {
    server = new BunWebSocketServer();
    server.listen({
      hostname,
      port,
    });
    client = new WebSocketClient({ reconnect: false });
    serverIterator = new ConnectionAsyncIterator(server);
    clientIterator = new ConnectionAsyncIterator(client);
  });

  afterEach(async () => {
    await Promise.all([
      serverIterator.return(),
      clientIterator.return(),
      client.stop(),
      server.stop(),
    ]);
  });

  it("can reconnect", async () => {
    client.connect(url);
    const [_, clientConnection] = await connected();
    let result = await client.close(clientConnection);
    expect(result.status).toEqual(STATE.disconnected);
    await client.reconnect(clientConnection);
    result = await clientConnection.state!.promise();
    expect(result.status).toEqual(STATE.connected);
  });

  it("can spam reconnect", async () => {
    client.connect(url);
    const [_, clientConnection] = await connected();
    let result = await client.close(clientConnection);
    expect(result.status).toEqual(STATE.disconnected);
    await client.reconnect(clientConnection);
    await client.reconnect(clientConnection);
    await client.reconnect(clientConnection);
    result = await clientConnection.state!.promise();
    expect(result.status).toEqual(STATE.connected);
  });

  it.only("can resume actions on reconnect", async () => {
    const next = mock((message: Message) => {
      //console.log(message);
    });
    let stop: boolean;
    let id: string;
    client.connect(url);
    const [serverConnection1, clientConnection1] = await connected();
    server.handle("long-running", async (connection, message) => {
      stop = false;
      let count = 0;
      const controller = new AbortController();
      while (!stop && connection.state?.value?.status === STATE.connected) {
        server.send(connection, {
          id: message.id,
          type: "long-running-response",
          data: { count: count++ },
        });
        await Promise.race([
          sleep(1000, { signal: controller.signal }),
          connection.state!.next(),
        ]);
      }
      controller.abort();
    });
    server.handle("long-running-stop", (connection, message) => {
      stop = true;
      server.send(connection, {
        id: message.id,
        type: "long-running-complete",
      });
    });
    client.handle("long-running-response", (connection, message) => {
      id = message.id as string;
      connection.actions!.get(message.id as string)?.timeout!.restart();
      connection.actions!.get(message.id as string)?.observable.next(message);
    });
    client.handle("long-running-complete", (connection, message) => {
      connection.actions!.get(message.id as string)?.observable.complete();
    });
    const promise = new Promise<void>((resolve) => {
      let count = 0;
      const action = client.action(
        clientConnection1,
        { type: "long-running" },
        []
      );
      action.observable.push({
        //next,
        next: () => {
          next();
          count++;
          if (count === 2) {
            client.send(clientConnection2, { id, type: "long-running-stop" });
          }
        },
        complete: resolve,
      });
      client.dispatch(clientConnection1, action);
    });
    await client.reconnect(clientConnection1);
    const [serverConnection2, clientConnection2] = await connected();
    // @ts-ignore: `id` is set.
    //client.send(clientConnection2, { id, type: "long-running-stop" });
    await promise;
    expect(serverConnection1).not.toEqual(serverConnection2);
    expect(clientConnection1).toEqual(clientConnection2);
    expect(next).toHaveBeenCalledTimes(2);
  });
});
