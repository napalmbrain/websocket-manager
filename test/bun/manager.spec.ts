import { describe, it, expect, mock, beforeEach, afterEach } from "bun:test";

import { BunWebSocketServer } from "../../src/bun/server.ts";
import { WebSocketClient } from "../../src/client.ts";
import { Connection } from "../../src/common.ts";
import { ConnectionAsyncIterator } from "../../src/iterator.ts";

describe("server", () => {
  const hostname = "localhost";
  let port: number;
  let url: string;
  let server: BunWebSocketServer;
  let client: WebSocketClient;
  let serverIterator: ConnectionAsyncIterator;
  let clientIterator: ConnectionAsyncIterator;
  const connected = async (): Promise<Connection[]> => {
    return (
      await Promise.all([serverIterator.next(), clientIterator.next()])
    ).map(({ value }) => value) as Connection[];
  };

  beforeEach(() => {
    port = 8001;
    url = `ws://${hostname}:${port}`;
    server = new BunWebSocketServer();
    client = new WebSocketClient({ reconnect: false });
    server.listen({ port });
    serverIterator = new ConnectionAsyncIterator(server);
    clientIterator = new ConnectionAsyncIterator(client);
  });

  afterEach(async () => {
    await serverIterator.return();
    await clientIterator.return();
    await client.stop();
    await server.stop();
  });

  it("can send `init` and `init_ack`", async () => {
    const serverInit = mock();
    const serverInitAck = mock();
    const clientInit = mock();
    const clientInitAck = mock();
    server.options.callbacks!.onInit = serverInit;
    server.options.callbacks!.onInitAck = serverInitAck;
    client.options.callbacks!.onInit = clientInit;
    client.options.callbacks!.onInitAck = clientInitAck;
    client.connect(url);
    await connected();
    expect(serverInit).toHaveBeenCalledTimes(1);
    expect(serverInitAck).toHaveBeenCalledTimes(1);
    expect(clientInit).toHaveBeenCalledTimes(1);
    expect(clientInitAck).toHaveBeenCalledTimes(1);
  });

  it("can send `ping` and `pong`", async () => {
    const clientPing = mock();
    const serverPong = mock();
    client.options.callbacks!.onPing = clientPing;
    server.options.callbacks!.onPong = serverPong;
    client.connect(url);
    const [serverConnection, _] = await connected();
    const response = await server.ping(serverConnection, { test: "ing" });
    expect(clientPing).toHaveBeenCalledTimes(1);
    expect(serverPong).toHaveBeenCalledTimes(1);
    expect(response.test).toEqual("ing");
  });

  it("can send `meta` and `meta_ack`", async () => {
    const serverMeta = mock();
    const clientMetaAck = mock();
    server.options.callbacks!.onMeta = serverMeta;
    client.options.callbacks!.onMetaAck = clientMetaAck;
    client.connect(url);
    const [_, clientConnection] = await connected();
    const result = await client.meta(clientConnection, { test: "value" });
    expect(result.test).toEqual("value");
    expect(serverMeta).toHaveBeenCalledTimes(1);
    expect(clientMetaAck).toHaveBeenCalledTimes(1);
  });
});
