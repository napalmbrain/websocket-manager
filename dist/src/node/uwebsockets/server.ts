import { App, TemplatedApp, WebSocketBehavior } from "uWebSockets.js";

import { WebSocketServer, WebSocketServerOptions } from "../../server";
import { Connection } from "../../common";
import { MessageEvent, CloseEvent } from "../event";

import { WebSocket } from "./websocket";

interface UserData {
  connection: Connection;
}

export class NodeMicroWebSocketServer extends WebSocketServer {
  protected decoder: TextDecoder;

  constructor(options: WebSocketServerOptions = {}) {
    super(options);
    this.decoder = new TextDecoder();
  }

  public listen(options: {
    app: TemplatedApp;
    pattern?: string;
    behavior?: WebSocketBehavior<unknown>;
  }) {
    options = Object.assign({ pattern: "/*", behavior: {} }, options);
    options.app.ws<UserData>(options.pattern!, {
      ...options.behavior,
      upgrade: (response, request, context) => {
        const connection = {} as Connection;
        response.upgrade(
          { connection },
          request.getHeader("sec-websocket-key"),
          request.getHeader("sec-websocket-protocol"),
          request.getHeader("sec-websocket-extensions"),
          context
        );
      },
      open: (ws) => {
        const data = ws.getUserData();
        // @ts-ignore: Ignore missing attributes.
        data.connection.socket = new WebSocket(ws);
        this.attach(data.connection);
        const event = new Event("open");
        data.connection.socket.onopen?.(event);
      },
      message: (ws, message) => {
        const data = ws.getUserData();
        const event = new MessageEvent("message", {
          data: this.decoder.decode(message),
        });
        // @ts-ignore: Ignore missing attributes.
        data.connection.socket.onmessage?.(event);
      },
      close: (ws) => {
        const data = ws.getUserData();
        const event = new CloseEvent("close");
        data.connection.socket.onclose?.(event);
      },
    });
  }
}
