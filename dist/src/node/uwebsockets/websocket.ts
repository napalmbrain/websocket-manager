import { WebSocket as MicroWebSocket } from "uWebSockets.js";
import { MessageEvent, ErrorEvent, CloseEvent } from "../event";

export class WebSocket {
  private ws: MicroWebSocket<unknown>;

  public onopen?: (event: Event) => void;
  public onmessage?: (event: MessageEvent) => void;
  public onerror?: (event: Event | ErrorEvent) => void;
  public onclose?: (event: CloseEvent) => void;

  constructor(ws: MicroWebSocket<unknown>) {
    this.ws = ws;
  }

  public get bufferedAmount(): number {
    return this.ws.getBufferedAmount();
  }

  public send(data: string) {
    this.ws.send(data);
  }

  public close(code?: number, reason?: string) {
    try {
      this.ws.end(code, reason);
    } catch {
      /* do nothing */
    }
  }
}
