export class MessageEvent extends Event {
  readonly data?: string;

  constructor(type: string, eventInitDict?: Record<string, unknown>) {
    super(type, eventInitDict);
    this.data = eventInitDict?.data as string;
  }
}

export class ErrorEvent extends Event {
  readonly message?: string;

  constructor(type: string, eventInitDict?: Record<string, unknown>) {
    super(type, eventInitDict);
    this.message = eventInitDict?.message as string;
  }
}

export class CloseEvent extends Event {
  readonly code: number;
  readonly reason: string;
  readonly wasClean: boolean;

  constructor(type: string, eventInitDict?: Record<string, unknown>) {
    super(type, eventInitDict);
    this.code = eventInitDict?.code as number;
    this.reason = eventInitDict?.reason as string;
    this.wasClean = eventInitDict?.wasClean as boolean;
  }
}
