import { WebSocket as WSWebSocket, RawData as WSRawData } from "ws";
import { MessageEvent, ErrorEvent, CloseEvent } from "./../event";

// XXX: Define `Buffer` type for compatability.
// deno-lint-ignore no-explicit-any
type Buffer = any;
type Empty = null | undefined;

type OnOpen = (() => void) | Empty;
type OnMessage = ((data: WSRawData) => void) | Empty;
type OnError = ((error: Error) => void) | Empty;
type OnClose = ((code?: number, reason?: Buffer) => void) | Empty;

export class WebSocket {
  private ws: WSWebSocket;
  private _onopen: OnOpen;
  private _onmessage: OnMessage;
  private _onerror: OnError;
  private _onclose: OnClose;

  constructor(ws: WSWebSocket) {
    this.ws = ws;
  }

  public static get CONNECTING() {
    return WSWebSocket.CONNECTING;
  }

  public static get OPEN() {
    return WSWebSocket.OPEN;
  }

  public static get CLOSING() {
    return WSWebSocket.CLOSING;
  }

  public static get CLOSED() {
    return WSWebSocket.CLOSED;
  }

  public get readyState() {
    return this.ws.readyState;
  }

  public get bufferedAmount(): number {
    return this.ws.bufferedAmount;
  }

  public get onopen(): OnOpen {
    return this._onopen;
  }

  public set onopen(handler: ((event: Event) => void) | Empty) {
    if (this._onopen) {
      this.ws.off("open", this._onopen);
    }
    const transform = () => {
      const event = new Event("open");
      handler!(event);
    };
    this._onopen = handler ? transform : handler;
    if (this._onopen) {
      this.ws.on("open", this._onopen);
    }
  }

  public get onmessage(): OnMessage {
    return this._onmessage;
  }

  public set onmessage(handler: ((event: MessageEvent) => void) | Empty) {
    if (this._onmessage) {
      this.ws.off("message", this._onmessage);
    }
    const transform = (data: WSRawData) => {
      const event = new MessageEvent("message", { data: data.toString() });
      handler!(event);
    };
    this._onmessage = handler ? transform : handler;
    if (this._onmessage) {
      this.ws.on("message", this._onmessage);
    }
  }

  public get onerror(): OnError {
    return this._onerror;
  }

  public set onerror(handler: ((event: Event | ErrorEvent) => void) | Empty) {
    if (this._onerror) {
      this.ws.off("error", this._onerror);
    }
    const transform = (error: Error) => {
      const event = new ErrorEvent("error", { message: error.message });
      handler!(event);
    };
    this._onerror = handler ? transform : handler;
    if (this._onerror) {
      this.ws.on("error", this._onerror);
    }
  }

  public get onclose(): OnClose {
    return this._onclose;
  }

  public set onclose(handler: ((event: CloseEvent) => void) | Empty) {
    if (this._onclose) {
      this.ws.off("close", this._onclose);
    }
    const transform = (code?: number, reason?: Buffer) => {
      const event = new CloseEvent("close", {
        code,
        reason: reason?.toString(),
      });
      handler!(event);
    };
    this._onclose = handler ? transform : handler;
    if (this._onclose) {
      this.ws.on("close", this._onclose);
    }
  }

  public send(data: string) {
    this.ws.send(data);
  }

  public close(code?: number, reason?: string) {
    this.ws.close(code, reason);
  }
}
