import {
  WebSocketServer as WSWebSocketServer,
  WebSocket as WSWebSocket,
  ServerOptions as WSServerOptions,
} from "ws";

import { WebSocket } from "./websocket";
import { WebSocketServer, WebSocketServerOptions } from "../../server";
import { Connection } from "../../common";

export class NodeWebSocketServer extends WebSocketServer {
  protected server?: WSWebSocketServer;
  protected onconnection: (ws: WSWebSocket) => void;

  constructor(options: WebSocketServerOptions = {}) {
    super(options);
    this.onconnection = this.connection.bind(this);
  }

  private connection(ws: WSWebSocket) {
    const socket = new WebSocket(ws);
    const connection: Connection = {
      // @ts-ignore: WebSocket is not fully implemented for nodejs.
      socket,
    };
    this.attach(connection);
    // XXX: Simulate `onopen` for server websockets.
    const event = new Event("open");
    connection.socket.onopen?.(event);
  }

  public listen(options: WSServerOptions = { port: 8000 }) {
    this.server = new WSWebSocketServer({ ...options });
    this.server.on("connection", this.onconnection);
  }

  public async stop() {
    this.server?.off("connection", this.onconnection);
    await super.stop();
    await new Promise<void>((resolve, reject) => {
      this.server?.close((error?: Error) => {
        if (error) {
          console.log("[NodeWebSocketServer stop]: server stop error.", error);
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }
}
