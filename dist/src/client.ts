import { Connection, MetaFunction, KEEPALIVE, TIMEOUT } from "./common";
import { WebSocketManager, WebSocketManagerOptions } from "./manager";
import { random, sleep } from "./utils";

export interface WebSocketClientOptions extends WebSocketManagerOptions {
  reconnect?: boolean;
}

export class WebSocketClient extends WebSocketManager {
  public declare options: WebSocketClientOptions;

  constructor(options: WebSocketClientOptions = {}) {
    super(
      Object.assign(
        {
          keepalive: -1,
          timeout: TIMEOUT,
          reconnect: true,
          callbacks: {},
        },
        options
      )
    );
  }

  public connect(url: string | URL, meta?: MetaFunction): Connection {
    const socket = new WebSocket(url);
    const connection: Connection = {
      socket,
      metaFunction: meta,
      params: {
        url,
      },
    };
    this.attach(connection);
    return connection;
  }

  public async reconnect(connection: Connection) {
    await this.close(connection);
    this.detach(connection);
    connection.socket = new WebSocket(connection.params!.url as string | URL);
    this.attach(connection);
  }

  protected async onclose(connection: Connection, event: CloseEvent) {
    super.onclose(connection, event);
    if (this.options.reconnect) {
      await sleep(random(0, TIMEOUT));
      this.reconnect(connection);
    }
  }
}
