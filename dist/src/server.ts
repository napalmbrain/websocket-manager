import { Connection, KEEPALIVE, STATE } from "./common";
import { WebSocketManager, WebSocketManagerOptions } from "./manager";

export type WebSocketServerOptions = WebSocketManagerOptions;

export class WebSocketServer extends WebSocketManager {
  public declare options: WebSocketServerOptions;

  constructor(options: WebSocketServerOptions = {}) {
    super(Object.assign({ keepalive: KEEPALIVE }, options));
  }

  public deinit(connection: Connection) {
    if (connection.state?.value?.status === STATE.connected) {
      for (const action of connection.actions!.values()) {
        action.observable.complete();
      }
      this.close(connection);
      super.deinit(connection);
    }
  }

  public async stop() {
    await super.stop();
    for (const connection of this.connections.values()) {
      for (const action of connection.actions!.values()) {
        this.remove(connection, action);
      }
    }
  }
}
