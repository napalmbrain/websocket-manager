// @ts-ignore: bun import will fail in node.
import { serve } from "bun";
import { WebSocketServer } from "../server";
import { Connection } from "../common";
import { WebSocket, ServerWebSocket } from "./websocket";

interface HTTPServer {
  development: boolean;
  hostname: string;
  port: number;
  pendingRequests: number;
  stop(): void;
}

interface WSServer extends HTTPServer {
  pendingWebsockets: number;
  publish(
    topic: string,
    data: string | ArrayBufferView | ArrayBuffer,
    compress?: boolean
  ): number;
  upgrade(
    req: Request,
    options?: {
      headers?: HeadersInit;
      data?: any;
    }
  ): boolean;
}

interface WebSocketData {
  connection: Connection;
}

export class BunWebSocketServer extends WebSocketServer {
  protected server?: WSServer;

  public listen(
    options: { hostname?: string; port?: number } = { port: 8000 }
  ) {
    this.server = serve<WebSocketData>({
      ...options,
      fetch: (request: Request, server: WSServer) => {
        const connection = {} as Connection;
        server.upgrade(request, {
          data: { connection },
        });
      },
      websocket: {
        open: (ws: ServerWebSocket) => {
          ws.data.connection.socket = new WebSocket(ws);
          this.attach(ws.data.connection);
          const event = new Event("open");
          ws.data.connection.socket.onopen?.(event);
        },
        message: (ws: ServerWebSocket, message: string) => {
          const event = new MessageEvent("message", { data: message });
          ws.data.connection.socket.onmessage?.(event);
        },
        error: (ws: ServerWebSocket, error: Error) => {
          const event = new ErrorEvent("error", { message: error.message });
          ws.data.connection.socket.onerror?.(event);
        },
        close: (ws: ServerWebSocket) => {
          const event = new CloseEvent("close");
          ws.data.connection.socket.onclose?.(event);
        },
      },
    });
  }

  public async stop() {
    await super.stop();
    this.server?.stop();
  }
}
