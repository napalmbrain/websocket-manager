export class Timeout {
  private callback: () => void;
  //@ts-ignore: handle is set in the constructor via `start`..
  private handle: number;
  private callbacks: (() => void)[];

  public timeout: number;
  public state: "started" | "stopped";

  constructor(callback: () => void, timeout: number) {
    this.callback = callback;
    this.callbacks = [];
    this.timeout = timeout;
    this.state = "stopped";
    this.start();
  }

  public start() {
    // @ts-ignore: Ignore type for nodejs.
    this.handle = setTimeout(() => {
      this.callback();
      for (const callback of this.callbacks) {
        callback();
      }
    }, this.timeout);
    this.state = "started";
  }

  public stop() {
    clearTimeout(this.handle);
    this.state = "stopped";
  }

  public restart() {
    this.stop();
    this.start();
  }

  public on(callback: () => void) {
    this.callbacks.push(callback);
  }

  public off(callback: () => void) {
    this.callbacks = this.callbacks.filter((c) => callback !== c);
  }
}
