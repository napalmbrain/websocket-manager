"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectionAsyncIterator = void 0;
class ConnectionAsyncIterator {
    constructor(manager) {
        var _a;
        this.manager = manager;
        this.pullQueue = [];
        this.pushQueue = [];
        this.onConnected = (_a = manager.options.callbacks) === null || _a === void 0 ? void 0 : _a.onConnected;
        this.manager.options.callbacks.onConnected = (connection) => {
            var _a;
            this.pushValue(connection);
            (_a = this.onConnected) === null || _a === void 0 ? void 0 : _a.call(this, connection);
        };
    }
    [Symbol.asyncIterator]() {
        return this;
    }
    next() {
        return this.pullValue();
    }
    return() {
        this.emptyQueue();
        return Promise.resolve({ value: undefined, done: true });
    }
    pullValue() {
        return new Promise((resolve) => {
            if (this.pushQueue.length > 0) {
                resolve({ value: this.pushQueue.shift(), done: false });
            }
            else {
                this.pullQueue.push(resolve);
            }
        });
    }
    pushValue(connection) {
        if (this.pullQueue.length > 0) {
            this.pullQueue.shift()({ value: connection, done: false });
        }
        else {
            this.pushQueue.push(connection);
        }
    }
    emptyQueue() {
        for (const resolve of this.pullQueue.values()) {
            resolve({ value: undefined, done: true });
        }
        this.pullQueue = [];
        this.pushQueue = [];
    }
}
exports.ConnectionAsyncIterator = ConnectionAsyncIterator;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlcmF0b3IuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaXRlcmF0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBR0EsTUFBYSx1QkFBdUI7SUFRbEMsWUFBWSxPQUF5Qjs7UUFDbkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFBLE9BQU8sQ0FBQyxPQUFPLENBQUMsU0FBUywwQ0FBRSxXQUFXLENBQUM7UUFDMUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsU0FBVSxDQUFDLFdBQVcsR0FBRyxDQUFDLFVBQVUsRUFBRSxFQUFFOztZQUMzRCxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzNCLE1BQUEsSUFBSSxDQUFDLFdBQVcscURBQUcsVUFBVSxDQUFDLENBQUM7UUFDakMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVELENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQztRQUNwQixPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTSxJQUFJO1FBQ1QsT0FBTyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVNLE1BQU07UUFDWCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDbEIsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRU8sU0FBUztRQUNmLE9BQU8sSUFBSSxPQUFPLENBQXNDLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDbEUsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzdCLE9BQU8sQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO2FBQzFEO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzlCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sU0FBUyxDQUFDLFVBQXNCO1FBQ3RDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFHLENBQUMsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1NBQzdEO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNqQztJQUNILENBQUM7SUFFTyxVQUFVO1FBQ2hCLEtBQUssTUFBTSxPQUFPLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUM3QyxPQUFPLENBQUMsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1NBQzNDO1FBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztDQUNGO0FBekRELDBEQXlEQyJ9