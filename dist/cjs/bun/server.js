"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BunWebSocketServer = void 0;
// @ts-ignore: bun import will fail in node.
const bun_1 = require("bun");
const server_1 = require("../server");
const websocket_1 = require("./websocket");
class BunWebSocketServer extends server_1.WebSocketServer {
    listen(options = { port: 8000 }) {
        this.server = (0, bun_1.serve)(Object.assign(Object.assign({}, options), { fetch: (request, server) => {
                const connection = {};
                server.upgrade(request, {
                    data: { connection },
                });
            }, websocket: {
                open: (ws) => {
                    var _a, _b;
                    ws.data.connection.socket = new websocket_1.WebSocket(ws);
                    this.attach(ws.data.connection);
                    const event = new Event("open");
                    (_b = (_a = ws.data.connection.socket).onopen) === null || _b === void 0 ? void 0 : _b.call(_a, event);
                },
                message: (ws, message) => {
                    var _a, _b;
                    const event = new MessageEvent("message", { data: message });
                    (_b = (_a = ws.data.connection.socket).onmessage) === null || _b === void 0 ? void 0 : _b.call(_a, event);
                },
                error: (ws, error) => {
                    var _a, _b;
                    const event = new ErrorEvent("error", { message: error.message });
                    (_b = (_a = ws.data.connection.socket).onerror) === null || _b === void 0 ? void 0 : _b.call(_a, event);
                },
                close: (ws) => {
                    var _a, _b;
                    const event = new CloseEvent("close");
                    (_b = (_a = ws.data.connection.socket).onclose) === null || _b === void 0 ? void 0 : _b.call(_a, event);
                },
            } }));
    }
    stop() {
        const _super = Object.create(null, {
            stop: { get: () => super.stop }
        });
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            yield _super.stop.call(this);
            (_a = this.server) === null || _a === void 0 ? void 0 : _a.stop();
        });
    }
}
exports.BunWebSocketServer = BunWebSocketServer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2J1bi9zZXJ2ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsNENBQTRDO0FBQzVDLDZCQUE0QjtBQUM1QixzQ0FBNEM7QUFFNUMsMkNBQXlEO0FBOEJ6RCxNQUFhLGtCQUFtQixTQUFRLHdCQUFlO0lBRzlDLE1BQU0sQ0FDWCxVQUFnRCxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUU7UUFFOUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFBLFdBQUssa0NBQ2QsT0FBTyxLQUNWLEtBQUssRUFBRSxDQUFDLE9BQWdCLEVBQUUsTUFBZ0IsRUFBRSxFQUFFO2dCQUM1QyxNQUFNLFVBQVUsR0FBRyxFQUFnQixDQUFDO2dCQUNwQyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRTtvQkFDdEIsSUFBSSxFQUFFLEVBQUUsVUFBVSxFQUFFO2lCQUNyQixDQUFDLENBQUM7WUFDTCxDQUFDLEVBQ0QsU0FBUyxFQUFFO2dCQUNULElBQUksRUFBRSxDQUFDLEVBQW1CLEVBQUUsRUFBRTs7b0JBQzVCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLHFCQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQzlDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDaEMsTUFBTSxLQUFLLEdBQUcsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ2hDLE1BQUEsTUFBQSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUMsTUFBTSxtREFBRyxLQUFLLENBQUMsQ0FBQztnQkFDNUMsQ0FBQztnQkFDRCxPQUFPLEVBQUUsQ0FBQyxFQUFtQixFQUFFLE9BQWUsRUFBRSxFQUFFOztvQkFDaEQsTUFBTSxLQUFLLEdBQUcsSUFBSSxZQUFZLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7b0JBQzdELE1BQUEsTUFBQSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUMsU0FBUyxtREFBRyxLQUFLLENBQUMsQ0FBQztnQkFDL0MsQ0FBQztnQkFDRCxLQUFLLEVBQUUsQ0FBQyxFQUFtQixFQUFFLEtBQVksRUFBRSxFQUFFOztvQkFDM0MsTUFBTSxLQUFLLEdBQUcsSUFBSSxVQUFVLENBQUMsT0FBTyxFQUFFLEVBQUUsT0FBTyxFQUFFLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO29CQUNsRSxNQUFBLE1BQUEsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFDLE9BQU8sbURBQUcsS0FBSyxDQUFDLENBQUM7Z0JBQzdDLENBQUM7Z0JBQ0QsS0FBSyxFQUFFLENBQUMsRUFBbUIsRUFBRSxFQUFFOztvQkFDN0IsTUFBTSxLQUFLLEdBQUcsSUFBSSxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3RDLE1BQUEsTUFBQSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUMsT0FBTyxtREFBRyxLQUFLLENBQUMsQ0FBQztnQkFDN0MsQ0FBQzthQUNGLElBQ0QsQ0FBQztJQUNMLENBQUM7SUFFWSxJQUFJOzs7Ozs7WUFDZixNQUFNLE9BQU0sSUFBSSxXQUFFLENBQUM7WUFDbkIsTUFBQSxJQUFJLENBQUMsTUFBTSwwQ0FBRSxJQUFJLEVBQUUsQ0FBQzs7S0FDckI7Q0FDRjtBQXpDRCxnREF5Q0MifQ==