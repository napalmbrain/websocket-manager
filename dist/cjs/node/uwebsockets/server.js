"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodeMicroWebSocketServer = void 0;
const server_1 = require("../../server");
const event_1 = require("../event");
const websocket_1 = require("./websocket");
class NodeMicroWebSocketServer extends server_1.WebSocketServer {
    constructor(options = {}) {
        super(options);
        this.decoder = new TextDecoder();
    }
    listen(options) {
        options = Object.assign({ pattern: "/*", behavior: {} }, options);
        options.app.ws(options.pattern, Object.assign(Object.assign({}, options.behavior), { upgrade: (response, request, context) => {
                const connection = {};
                response.upgrade({ connection }, request.getHeader("sec-websocket-key"), request.getHeader("sec-websocket-protocol"), request.getHeader("sec-websocket-extensions"), context);
            }, open: (ws) => {
                var _a, _b;
                const data = ws.getUserData();
                // @ts-ignore: Ignore missing attributes.
                data.connection.socket = new websocket_1.WebSocket(ws);
                this.attach(data.connection);
                const event = new Event("open");
                (_b = (_a = data.connection.socket).onopen) === null || _b === void 0 ? void 0 : _b.call(_a, event);
            }, message: (ws, message) => {
                var _a, _b;
                const data = ws.getUserData();
                const event = new event_1.MessageEvent("message", {
                    data: this.decoder.decode(message),
                });
                // @ts-ignore: Ignore missing attributes.
                (_b = (_a = data.connection.socket).onmessage) === null || _b === void 0 ? void 0 : _b.call(_a, event);
            }, close: (ws) => {
                var _a, _b;
                const data = ws.getUserData();
                const event = new event_1.CloseEvent("close");
                (_b = (_a = data.connection.socket).onclose) === null || _b === void 0 ? void 0 : _b.call(_a, event);
            } }));
    }
}
exports.NodeMicroWebSocketServer = NodeMicroWebSocketServer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvdXdlYnNvY2tldHMvc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUVBLHlDQUF1RTtBQUV2RSxvQ0FBb0Q7QUFFcEQsMkNBQXdDO0FBTXhDLE1BQWEsd0JBQXlCLFNBQVEsd0JBQWU7SUFHM0QsWUFBWSxVQUFrQyxFQUFFO1FBQzlDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNmLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztJQUNuQyxDQUFDO0lBRU0sTUFBTSxDQUFDLE9BSWI7UUFDQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2xFLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFXLE9BQU8sQ0FBQyxPQUFRLGtDQUNwQyxPQUFPLENBQUMsUUFBUSxLQUNuQixPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxFQUFFO2dCQUN0QyxNQUFNLFVBQVUsR0FBRyxFQUFnQixDQUFDO2dCQUNwQyxRQUFRLENBQUMsT0FBTyxDQUNkLEVBQUUsVUFBVSxFQUFFLEVBQ2QsT0FBTyxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUN0QyxPQUFPLENBQUMsU0FBUyxDQUFDLHdCQUF3QixDQUFDLEVBQzNDLE9BQU8sQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsRUFDN0MsT0FBTyxDQUNSLENBQUM7WUFDSixDQUFDLEVBQ0QsSUFBSSxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUU7O2dCQUNYLE1BQU0sSUFBSSxHQUFHLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDOUIseUNBQXlDO2dCQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLHFCQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUM3QixNQUFNLEtBQUssR0FBRyxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDaEMsTUFBQSxNQUFBLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFDLE1BQU0sbURBQUcsS0FBSyxDQUFDLENBQUM7WUFDekMsQ0FBQyxFQUNELE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRSxPQUFPLEVBQUUsRUFBRTs7Z0JBQ3ZCLE1BQU0sSUFBSSxHQUFHLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDOUIsTUFBTSxLQUFLLEdBQUcsSUFBSSxvQkFBWSxDQUFDLFNBQVMsRUFBRTtvQkFDeEMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztpQkFDbkMsQ0FBQyxDQUFDO2dCQUNILHlDQUF5QztnQkFDekMsTUFBQSxNQUFBLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFDLFNBQVMsbURBQUcsS0FBSyxDQUFDLENBQUM7WUFDNUMsQ0FBQyxFQUNELEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFOztnQkFDWixNQUFNLElBQUksR0FBRyxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQzlCLE1BQU0sS0FBSyxHQUFHLElBQUksa0JBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdEMsTUFBQSxNQUFBLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFDLE9BQU8sbURBQUcsS0FBSyxDQUFDLENBQUM7WUFDMUMsQ0FBQyxJQUNELENBQUM7SUFDTCxDQUFDO0NBQ0Y7QUFqREQsNERBaURDIn0=