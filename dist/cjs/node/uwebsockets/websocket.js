"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebSocket = void 0;
class WebSocket {
    constructor(ws) {
        this.ws = ws;
    }
    get bufferedAmount() {
        return this.ws.getBufferedAmount();
    }
    send(data) {
        this.ws.send(data);
    }
    close(code, reason) {
        try {
            this.ws.end(code, reason);
        }
        catch (_a) {
            /* do nothing */
        }
    }
}
exports.WebSocket = WebSocket;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2Vic29ja2V0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvdXdlYnNvY2tldHMvd2Vic29ja2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUdBLE1BQWEsU0FBUztJQVFwQixZQUFZLEVBQTJCO1FBQ3JDLElBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDO0lBQ2YsQ0FBQztJQUVELElBQVcsY0FBYztRQUN2QixPQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRU0sSUFBSSxDQUFDLElBQVk7UUFDdEIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUVNLEtBQUssQ0FBQyxJQUFhLEVBQUUsTUFBZTtRQUN6QyxJQUFJO1lBQ0YsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQzNCO1FBQUMsV0FBTTtZQUNOLGdCQUFnQjtTQUNqQjtJQUNILENBQUM7Q0FDRjtBQTNCRCw4QkEyQkMifQ==