"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CloseEvent = exports.ErrorEvent = exports.MessageEvent = void 0;
class MessageEvent extends Event {
    constructor(type, eventInitDict) {
        super(type, eventInitDict);
        this.data = eventInitDict === null || eventInitDict === void 0 ? void 0 : eventInitDict.data;
    }
}
exports.MessageEvent = MessageEvent;
class ErrorEvent extends Event {
    constructor(type, eventInitDict) {
        super(type, eventInitDict);
        this.message = eventInitDict === null || eventInitDict === void 0 ? void 0 : eventInitDict.message;
    }
}
exports.ErrorEvent = ErrorEvent;
class CloseEvent extends Event {
    constructor(type, eventInitDict) {
        super(type, eventInitDict);
        this.code = eventInitDict === null || eventInitDict === void 0 ? void 0 : eventInitDict.code;
        this.reason = eventInitDict === null || eventInitDict === void 0 ? void 0 : eventInitDict.reason;
        this.wasClean = eventInitDict === null || eventInitDict === void 0 ? void 0 : eventInitDict.wasClean;
    }
}
exports.CloseEvent = CloseEvent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvbm9kZS9ldmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSxNQUFhLFlBQWEsU0FBUSxLQUFLO0lBR3JDLFlBQVksSUFBWSxFQUFFLGFBQXVDO1FBQy9ELEtBQUssQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLElBQUksR0FBRyxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsSUFBYyxDQUFDO0lBQzVDLENBQUM7Q0FDRjtBQVBELG9DQU9DO0FBRUQsTUFBYSxVQUFXLFNBQVEsS0FBSztJQUduQyxZQUFZLElBQVksRUFBRSxhQUF1QztRQUMvRCxLQUFLLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxPQUFPLEdBQUcsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE9BQWlCLENBQUM7SUFDbEQsQ0FBQztDQUNGO0FBUEQsZ0NBT0M7QUFFRCxNQUFhLFVBQVcsU0FBUSxLQUFLO0lBS25DLFlBQVksSUFBWSxFQUFFLGFBQXVDO1FBQy9ELEtBQUssQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLElBQUksR0FBRyxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsSUFBYyxDQUFDO1FBQzFDLElBQUksQ0FBQyxNQUFNLEdBQUcsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE1BQWdCLENBQUM7UUFDOUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsUUFBbUIsQ0FBQztJQUNyRCxDQUFDO0NBQ0Y7QUFYRCxnQ0FXQyJ9