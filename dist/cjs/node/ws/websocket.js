"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebSocket = void 0;
const ws_1 = require("ws");
const event_1 = require("./../event");
class WebSocket {
    constructor(ws) {
        this.ws = ws;
    }
    static get CONNECTING() {
        return ws_1.WebSocket.CONNECTING;
    }
    static get OPEN() {
        return ws_1.WebSocket.OPEN;
    }
    static get CLOSING() {
        return ws_1.WebSocket.CLOSING;
    }
    static get CLOSED() {
        return ws_1.WebSocket.CLOSED;
    }
    get readyState() {
        return this.ws.readyState;
    }
    get bufferedAmount() {
        return this.ws.bufferedAmount;
    }
    get onopen() {
        return this._onopen;
    }
    set onopen(handler) {
        if (this._onopen) {
            this.ws.off("open", this._onopen);
        }
        const transform = () => {
            const event = new Event("open");
            handler(event);
        };
        this._onopen = handler ? transform : handler;
        if (this._onopen) {
            this.ws.on("open", this._onopen);
        }
    }
    get onmessage() {
        return this._onmessage;
    }
    set onmessage(handler) {
        if (this._onmessage) {
            this.ws.off("message", this._onmessage);
        }
        const transform = (data) => {
            const event = new event_1.MessageEvent("message", { data: data.toString() });
            handler(event);
        };
        this._onmessage = handler ? transform : handler;
        if (this._onmessage) {
            this.ws.on("message", this._onmessage);
        }
    }
    get onerror() {
        return this._onerror;
    }
    set onerror(handler) {
        if (this._onerror) {
            this.ws.off("error", this._onerror);
        }
        const transform = (error) => {
            const event = new event_1.ErrorEvent("error", { message: error.message });
            handler(event);
        };
        this._onerror = handler ? transform : handler;
        if (this._onerror) {
            this.ws.on("error", this._onerror);
        }
    }
    get onclose() {
        return this._onclose;
    }
    set onclose(handler) {
        if (this._onclose) {
            this.ws.off("close", this._onclose);
        }
        const transform = (code, reason) => {
            const event = new event_1.CloseEvent("close", {
                code,
                reason: reason === null || reason === void 0 ? void 0 : reason.toString(),
            });
            handler(event);
        };
        this._onclose = handler ? transform : handler;
        if (this._onclose) {
            this.ws.on("close", this._onclose);
        }
    }
    send(data) {
        this.ws.send(data);
    }
    close(code, reason) {
        this.ws.close(code, reason);
    }
}
exports.WebSocket = WebSocket;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2Vic29ja2V0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvd3Mvd2Vic29ja2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDJCQUFvRTtBQUNwRSxzQ0FBa0U7QUFZbEUsTUFBYSxTQUFTO0lBT3BCLFlBQVksRUFBZTtRQUN6QixJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztJQUNmLENBQUM7SUFFTSxNQUFNLEtBQUssVUFBVTtRQUMxQixPQUFPLGNBQVcsQ0FBQyxVQUFVLENBQUM7SUFDaEMsQ0FBQztJQUVNLE1BQU0sS0FBSyxJQUFJO1FBQ3BCLE9BQU8sY0FBVyxDQUFDLElBQUksQ0FBQztJQUMxQixDQUFDO0lBRU0sTUFBTSxLQUFLLE9BQU87UUFDdkIsT0FBTyxjQUFXLENBQUMsT0FBTyxDQUFDO0lBQzdCLENBQUM7SUFFTSxNQUFNLEtBQUssTUFBTTtRQUN0QixPQUFPLGNBQVcsQ0FBQyxNQUFNLENBQUM7SUFDNUIsQ0FBQztJQUVELElBQVcsVUFBVTtRQUNuQixPQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDO0lBQzVCLENBQUM7SUFFRCxJQUFXLGNBQWM7UUFDdkIsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsSUFBVyxNQUFNO1FBQ2YsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3RCLENBQUM7SUFFRCxJQUFXLE1BQU0sQ0FBQyxPQUF5QztRQUN6RCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNuQztRQUNELE1BQU0sU0FBUyxHQUFHLEdBQUcsRUFBRTtZQUNyQixNQUFNLEtBQUssR0FBRyxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoQyxPQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEIsQ0FBQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQzdDLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ2xDO0lBQ0gsQ0FBQztJQUVELElBQVcsU0FBUztRQUNsQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDekIsQ0FBQztJQUVELElBQVcsU0FBUyxDQUFDLE9BQWdEO1FBQ25FLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ3pDO1FBQ0QsTUFBTSxTQUFTLEdBQUcsQ0FBQyxJQUFlLEVBQUUsRUFBRTtZQUNwQyxNQUFNLEtBQUssR0FBRyxJQUFJLG9CQUFZLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDckUsT0FBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xCLENBQUMsQ0FBQztRQUNGLElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztRQUNoRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUN4QztJQUNILENBQUM7SUFFRCxJQUFXLE9BQU87UUFDaEIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxJQUFXLE9BQU8sQ0FBQyxPQUFzRDtRQUN2RSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNyQztRQUNELE1BQU0sU0FBUyxHQUFHLENBQUMsS0FBWSxFQUFFLEVBQUU7WUFDakMsTUFBTSxLQUFLLEdBQUcsSUFBSSxrQkFBVSxDQUFDLE9BQU8sRUFBRSxFQUFFLE9BQU8sRUFBRSxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztZQUNsRSxPQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEIsQ0FBQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQzlDLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQztJQUVELElBQVcsT0FBTztRQUNoQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDdkIsQ0FBQztJQUVELElBQVcsT0FBTyxDQUFDLE9BQThDO1FBQy9ELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3JDO1FBQ0QsTUFBTSxTQUFTLEdBQUcsQ0FBQyxJQUFhLEVBQUUsTUFBZSxFQUFFLEVBQUU7WUFDbkQsTUFBTSxLQUFLLEdBQUcsSUFBSSxrQkFBVSxDQUFDLE9BQU8sRUFBRTtnQkFDcEMsSUFBSTtnQkFDSixNQUFNLEVBQUUsTUFBTSxhQUFOLE1BQU0sdUJBQU4sTUFBTSxDQUFFLFFBQVEsRUFBRTthQUMzQixDQUFDLENBQUM7WUFDSCxPQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEIsQ0FBQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQzlDLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQztJQUVNLElBQUksQ0FBQyxJQUFZO1FBQ3RCLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUFFTSxLQUFLLENBQUMsSUFBYSxFQUFFLE1BQWU7UUFDekMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzlCLENBQUM7Q0FDRjtBQXJIRCw4QkFxSEMifQ==