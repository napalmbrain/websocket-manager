"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodeWebSocketClient = void 0;
const ws_1 = require("ws");
const websocket_1 = require("./websocket");
const client_1 = require("../../client");
class NodeWebSocketClient extends client_1.WebSocketClient {
    // @ts-ignore: Ignore type conflict with parent class.
    connect(address, meta, protocols, options) {
        const ws = new ws_1.WebSocket(address, protocols, options);
        const socket = new websocket_1.WebSocket(ws);
        const connection = {
            metaFunction: meta,
            // @ts-ignore: WebSocket is not fully implemented for nodejs.
            socket,
            params: {
                address,
                protocols,
                options,
            },
        };
        this.attach(connection);
        return connection;
    }
    reconnect(connection) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.close(connection);
            this.detach(connection);
            const ws = new ws_1.WebSocket(connection.params.address, connection.params.protocols, connection.params.options);
            // @ts-ignore: Ignore type conflict.
            connection.socket = new websocket_1.WebSocket(ws);
            this.attach(connection);
        });
    }
}
exports.NodeWebSocketClient = NodeWebSocketClient;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvd3MvY2xpZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLDJCQUFnRjtBQUVoRiwyQ0FBd0M7QUFDeEMseUNBQStDO0FBRy9DLE1BQWEsbUJBQW9CLFNBQVEsd0JBQWU7SUFDdEQsc0RBQXNEO0lBQy9DLE9BQU8sQ0FDWixPQUFxQixFQUNyQixJQUFtQixFQUNuQixTQUFvQixFQUNwQixPQUF5QjtRQUV6QixNQUFNLEVBQUUsR0FBRyxJQUFJLGNBQVcsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3hELE1BQU0sTUFBTSxHQUFHLElBQUkscUJBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNqQyxNQUFNLFVBQVUsR0FBZTtZQUM3QixZQUFZLEVBQUUsSUFBSTtZQUNsQiw2REFBNkQ7WUFDN0QsTUFBTTtZQUNOLE1BQU0sRUFBRTtnQkFDTixPQUFPO2dCQUNQLFNBQVM7Z0JBQ1QsT0FBTzthQUNSO1NBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDeEIsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQztJQUVZLFNBQVMsQ0FBQyxVQUFzQjs7WUFDM0MsTUFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDeEIsTUFBTSxFQUFFLEdBQUcsSUFBSSxjQUFXLENBQ3hCLFVBQVUsQ0FBQyxNQUFPLENBQUMsT0FBdUIsRUFDMUMsVUFBVSxDQUFDLE1BQU8sQ0FBQyxTQUFxQixFQUN4QyxVQUFVLENBQUMsTUFBTyxDQUFDLE9BQWtCLENBQ3RDLENBQUM7WUFDRixvQ0FBb0M7WUFDcEMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLHFCQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMxQixDQUFDO0tBQUE7Q0FDRjtBQXBDRCxrREFvQ0MifQ==