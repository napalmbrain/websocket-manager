"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.uuidv4 = exports.limitedAssign = exports.random = exports.keepalive = exports.sleep = exports.isFunction = exports.isBrowser = exports.isDeno = void 0;
function isDeno() {
    if (window && "Deno" in window) {
        return true;
    }
    return false;
}
exports.isDeno = isDeno;
/**
 * Determines if the environment is the browser.
 * @returns true if the environment is the browser, else false.
 */
function isBrowser() {
    if (window && !isDeno()) {
        return true;
    }
    return false;
}
exports.isBrowser = isBrowser;
function isFunction(object) {
    if (typeof object === "function") {
        return true;
    }
    return false;
}
exports.isFunction = isFunction;
function sleep(time, options = {}) {
    return new Promise((resolve, reject) => {
        var _a;
        let timeout;
        const onAbort = () => {
            var _a;
            (_a = options.signal) === null || _a === void 0 ? void 0 : _a.removeEventListener("abort", onAbort);
            // @ts-ignore: Clear timeout on type `unknown` for compatibility with Deno, Bun and Node.
            clearTimeout(timeout);
            reject();
        };
        (_a = options.signal) === null || _a === void 0 ? void 0 : _a.addEventListener("abort", onAbort);
        timeout = setTimeout(() => {
            var _a;
            (_a = options.signal) === null || _a === void 0 ? void 0 : _a.removeEventListener("abort", onAbort);
            resolve();
        }, time);
    });
}
exports.sleep = sleep;
function keepalive(keepalive) {
    return (keepalive - keepalive / 2 + Math.floor((keepalive / 2) * Math.random()));
}
exports.keepalive = keepalive;
function random(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}
exports.random = random;
function limitedAssign(a, b, limit = 4096) {
    const alpha = JSON.stringify(a || {});
    const beta = JSON.stringify(b || {});
    const length = alpha.length + beta.length;
    if (length > limit) {
        throw new Error("Object size limit.");
    }
    Object.assign(a, b);
    return a;
}
exports.limitedAssign = limitedAssign;
function uuidv4() {
    return "10000000-1000-4000-8000-100000000000".replace(/[018]/g, (c) => (c ^
        (crypto.getRandomValues(new Uint8Array(1))[0] &
            (15 >> (c / 4)))).toString(16));
}
exports.uuidv4 = uuidv4;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsU0FBZ0IsTUFBTTtJQUNwQixJQUFJLE1BQU0sSUFBSSxNQUFNLElBQUksTUFBTSxFQUFFO1FBQzlCLE9BQU8sSUFBSSxDQUFDO0tBQ2I7SUFDRCxPQUFPLEtBQUssQ0FBQztBQUNmLENBQUM7QUFMRCx3QkFLQztBQUVEOzs7R0FHRztBQUNILFNBQWdCLFNBQVM7SUFDdkIsSUFBSSxNQUFNLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRTtRQUN2QixPQUFPLElBQUksQ0FBQztLQUNiO0lBQ0QsT0FBTyxLQUFLLENBQUM7QUFDZixDQUFDO0FBTEQsOEJBS0M7QUFFRCxTQUFnQixVQUFVLENBQUMsTUFBYztJQUN2QyxJQUFJLE9BQU8sTUFBTSxLQUFLLFVBQVUsRUFBRTtRQUNoQyxPQUFPLElBQUksQ0FBQztLQUNiO0lBQ0QsT0FBTyxLQUFLLENBQUM7QUFDZixDQUFDO0FBTEQsZ0NBS0M7QUFFRCxTQUFnQixLQUFLLENBQUMsSUFBWSxFQUFFLFVBQW9DLEVBQUU7SUFDeEUsT0FBTyxJQUFJLE9BQU8sQ0FBTyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTs7UUFDM0MsSUFBSSxPQUFnQixDQUFDO1FBQ3JCLE1BQU0sT0FBTyxHQUFHLEdBQUcsRUFBRTs7WUFDbkIsTUFBQSxPQUFPLENBQUMsTUFBTSwwQ0FBRSxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDdEQseUZBQXlGO1lBQ3pGLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN0QixNQUFNLEVBQUUsQ0FBQztRQUNYLENBQUMsQ0FBQztRQUNGLE1BQUEsT0FBTyxDQUFDLE1BQU0sMENBQUUsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ25ELE9BQU8sR0FBRyxVQUFVLENBQUMsR0FBRyxFQUFFOztZQUN4QixNQUFBLE9BQU8sQ0FBQyxNQUFNLDBDQUFFLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUN0RCxPQUFPLEVBQUUsQ0FBQztRQUNaLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNYLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQztBQWZELHNCQWVDO0FBRUQsU0FBZ0IsU0FBUyxDQUFDLFNBQWlCO0lBQ3pDLE9BQU8sQ0FDTCxTQUFTLEdBQUcsU0FBUyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUN4RSxDQUFDO0FBQ0osQ0FBQztBQUpELDhCQUlDO0FBRUQsU0FBZ0IsTUFBTSxDQUFDLEdBQVcsRUFBRSxHQUFXO0lBQzdDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7QUFDdkQsQ0FBQztBQUZELHdCQUVDO0FBRUQsU0FBZ0IsYUFBYSxDQUFDLENBQVMsRUFBRSxDQUFTLEVBQUUsS0FBSyxHQUFHLElBQUk7SUFDOUQsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7SUFDdEMsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7SUFDckMsTUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQzFDLElBQUksTUFBTSxHQUFHLEtBQUssRUFBRTtRQUNsQixNQUFNLElBQUksS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7S0FDdkM7SUFDRCxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNwQixPQUFPLENBQUMsQ0FBQztBQUNYLENBQUM7QUFURCxzQ0FTQztBQUVELFNBQWdCLE1BQU07SUFDcEIsT0FBTyxzQ0FBc0MsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FDcEUsQ0FDRyxDQUF1QjtRQUN4QixDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0MsQ0FBQyxFQUFFLElBQUksQ0FBRSxDQUF1QixHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FDMUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQ2YsQ0FBQztBQUNKLENBQUM7QUFSRCx3QkFRQyJ9