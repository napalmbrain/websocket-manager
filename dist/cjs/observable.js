"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Observable = void 0;
/**
 * Observable class.
 */
class Observable {
    constructor(observers = []) {
        this.observers = observers;
    }
    /**
     * Call the next function on all observers, if it exists.
     * @param data Data to pass to observers.
     */
    next(data) {
        var _a;
        for (const observer of this.observers) {
            (_a = observer.next) === null || _a === void 0 ? void 0 : _a.call(observer, data);
        }
    }
    /**
     * Call the error function on all observers, if it exists.
     * @param data Data to pass to observers.
     */
    error(data) {
        var _a;
        for (const observer of this.observers) {
            (_a = observer.error) === null || _a === void 0 ? void 0 : _a.call(observer, data);
        }
    }
    /**
     * Call the complete function on all observers, if it exists.
     */
    complete() {
        var _a;
        for (const observer of this.observers) {
            (_a = observer.complete) === null || _a === void 0 ? void 0 : _a.call(observer);
        }
        this.observers = [];
    }
    /**
     * Push an observer to the array of observers.
     * @param observer The observer to add.
     */
    push(observer) {
        this.observers.push(observer);
    }
    /**
     * Unshift an observer to the array of observers.
     * @param observer The observer to add.
     */
    unshift(observer) {
        this.observers.unshift(observer);
    }
    /**
     * Remove an observer from the array of observers.
     * @param observer The observer to remove.
     */
    unobserve(observer) {
        this.observers = this.observers.filter((o) => observer !== o);
    }
}
exports.Observable = Observable;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JzZXJ2YWJsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9vYnNlcnZhYmxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQU1BOztHQUVHO0FBQ0gsTUFBYSxVQUFVO0lBTXJCLFlBQVksWUFBMkIsRUFBRTtRQUN2QyxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztJQUM3QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksSUFBSSxDQUFDLElBQU87O1FBQ2pCLEtBQUssTUFBTSxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNyQyxNQUFBLFFBQVEsQ0FBQyxJQUFJLHlEQUFHLElBQUksQ0FBQyxDQUFDO1NBQ3ZCO0lBQ0gsQ0FBQztJQUVEOzs7T0FHRztJQUNJLEtBQUssQ0FBQyxJQUFPOztRQUNsQixLQUFLLE1BQU0sUUFBUSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDckMsTUFBQSxRQUFRLENBQUMsS0FBSyx5REFBRyxJQUFJLENBQUMsQ0FBQztTQUN4QjtJQUNILENBQUM7SUFFRDs7T0FFRztJQUNJLFFBQVE7O1FBQ2IsS0FBSyxNQUFNLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3JDLE1BQUEsUUFBUSxDQUFDLFFBQVEsd0RBQUksQ0FBQztTQUN2QjtRQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRDs7O09BR0c7SUFDSSxJQUFJLENBQUMsUUFBcUI7UUFDL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVEOzs7T0FHRztJQUNJLE9BQU8sQ0FBQyxRQUFxQjtRQUNsQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksU0FBUyxDQUFDLFFBQXFCO1FBQ3BDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLFFBQVEsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNoRSxDQUFDO0NBQ0Y7QUEvREQsZ0NBK0RDIn0=