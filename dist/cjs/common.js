"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.STATE = exports.PROTOCOL = exports.KEEPALIVE = exports.TIMEOUT = void 0;
exports.TIMEOUT = 10000;
exports.KEEPALIVE = exports.TIMEOUT / 2;
var PROTOCOL;
(function (PROTOCOL) {
    PROTOCOL["init"] = "init";
    PROTOCOL["init_ack"] = "init_ack";
    PROTOCOL["ping"] = "ping";
    PROTOCOL["pong"] = "pong";
    PROTOCOL["meta"] = "meta";
    PROTOCOL["meta_ack"] = "meta_ack";
})(PROTOCOL || (exports.PROTOCOL = PROTOCOL = {}));
var STATE;
(function (STATE) {
    STATE[STATE["connected"] = 0] = "connected";
    STATE[STATE["disconnected"] = 1] = "disconnected";
})(STATE || (exports.STATE = STATE = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL2NvbW1vbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFJYSxRQUFBLE9BQU8sR0FBRyxLQUFNLENBQUM7QUFDakIsUUFBQSxTQUFTLEdBQUcsZUFBTyxHQUFHLENBQUMsQ0FBQztBQWFyQyxJQUFZLFFBT1g7QUFQRCxXQUFZLFFBQVE7SUFDbEIseUJBQWEsQ0FBQTtJQUNiLGlDQUFxQixDQUFBO0lBQ3JCLHlCQUFhLENBQUE7SUFDYix5QkFBYSxDQUFBO0lBQ2IseUJBQWEsQ0FBQTtJQUNiLGlDQUFxQixDQUFBO0FBQ3ZCLENBQUMsRUFQVyxRQUFRLHdCQUFSLFFBQVEsUUFPbkI7QUFnQ0QsSUFBWSxLQUdYO0FBSEQsV0FBWSxLQUFLO0lBQ2YsMkNBQVMsQ0FBQTtJQUNULGlEQUFZLENBQUE7QUFDZCxDQUFDLEVBSFcsS0FBSyxxQkFBTCxLQUFLLFFBR2hCIn0=