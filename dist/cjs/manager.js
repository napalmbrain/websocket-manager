"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebSocketManager = void 0;
const common_1 = require("./common");
const signaler_1 = require("./signaler");
const timeout_1 = require("./timeout");
const observable_1 = require("./observable");
const utils_1 = require("./utils");
class WebSocketManager {
    constructor(options) {
        this.options = Object.assign({
            keepalive: common_1.KEEPALIVE,
            timeout: common_1.TIMEOUT,
            callbacks: {},
        }, options);
        this.id = (0, utils_1.uuidv4)();
        this.connections = new Map();
        this.handlers = new Map();
        this.handle(common_1.PROTOCOL.init, (connection, message) => {
            var _a, _b;
            try {
                connection.meta = (0, utils_1.limitedAssign)(connection.meta, message.meta);
            }
            catch (error) {
                console.log("[WebSocketManager handle(PROTOCOL.init)]: ", error);
                delete connection.meta;
                delete message.meta;
                this.close(connection);
                return;
            }
            this.send(connection, {
                id: message.id,
                type: common_1.PROTOCOL.init_ack,
                remote: {
                    manager: this.id,
                    connection: connection.id,
                },
                meta: connection.meta,
            });
            (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onInit) === null || _b === void 0 ? void 0 : _b.call(_a, connection, message);
        });
        this.handle(common_1.PROTOCOL.init_ack, (connection, message) => {
            var _a, _b, _c, _d, _e, _f;
            try {
                connection.meta = (0, utils_1.limitedAssign)(connection.meta, message.meta);
            }
            catch (error) {
                console.log("[WebSocketManager handle(PROTOCOL.init_ack)]: ", error);
                delete connection.meta;
                delete message.meta;
                this.close(connection);
                return;
            }
            (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onInitAck) === null || _b === void 0 ? void 0 : _b.call(_a, connection, message);
            const remote = message.remote;
            connection.remote = Object.assign({}, remote);
            (_d = (_c = connection.actions) === null || _c === void 0 ? void 0 : _c.get(message.id)) === null || _d === void 0 ? void 0 : _d.observable.next(message);
            (_f = (_e = connection.actions) === null || _e === void 0 ? void 0 : _e.get(message.id)) === null || _f === void 0 ? void 0 : _f.observable.complete();
        });
        this.handle(common_1.PROTOCOL.ping, (connection, message) => {
            var _a, _b, _c;
            (_a = connection.timeout) === null || _a === void 0 ? void 0 : _a.restart();
            this.send(connection, {
                id: message.id,
                type: common_1.PROTOCOL.pong,
                data: message.data,
            });
            (_c = (_b = this.options.callbacks) === null || _b === void 0 ? void 0 : _b.onPing) === null || _c === void 0 ? void 0 : _c.call(_b, connection, message);
        });
        this.handle(common_1.PROTOCOL.pong, (connection, message) => {
            var _a, _b, _c, _d, _e, _f, _g;
            (_a = connection.timeout) === null || _a === void 0 ? void 0 : _a.restart();
            (_c = (_b = connection.actions) === null || _b === void 0 ? void 0 : _b.get(message.id)) === null || _c === void 0 ? void 0 : _c.observable.next(message);
            (_e = (_d = connection.actions) === null || _d === void 0 ? void 0 : _d.get(message.id)) === null || _e === void 0 ? void 0 : _e.observable.complete();
            (_g = (_f = this.options.callbacks) === null || _f === void 0 ? void 0 : _f.onPong) === null || _g === void 0 ? void 0 : _g.call(_f, connection, message);
        });
        this.handle(common_1.PROTOCOL.meta, (connection, message) => {
            var _a, _b;
            try {
                connection.meta = (0, utils_1.limitedAssign)(connection.meta, message.meta);
            }
            catch (error) {
                console.log("[WebSocketManager handle(PROTOCOL.meta)]: ", error);
                delete connection.meta;
                delete message.meta;
                this.close(connection);
                return;
            }
            this.send(connection, {
                id: message.id,
                type: common_1.PROTOCOL.meta_ack,
                meta: connection.meta,
            });
            (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onMeta) === null || _b === void 0 ? void 0 : _b.call(_a, connection, message);
        });
        this.handle(common_1.PROTOCOL.meta_ack, (connection, message) => {
            var _a, _b, _c, _d, _e, _f;
            try {
                connection.meta = (0, utils_1.limitedAssign)(connection.meta, message.meta);
            }
            catch (error) {
                console.log("[WebSocketManager handle(PROTOCOL.meta_ack)]: ", error);
                delete connection.meta;
                delete message.meta;
                this.close(connection);
                return;
            }
            (_b = (_a = connection.actions) === null || _a === void 0 ? void 0 : _a.get(message.id)) === null || _b === void 0 ? void 0 : _b.observable.next(message);
            (_d = (_c = connection.actions) === null || _c === void 0 ? void 0 : _c.get(message.id)) === null || _d === void 0 ? void 0 : _d.observable.complete();
            (_f = (_e = this.options.callbacks) === null || _e === void 0 ? void 0 : _e.onMetaAck) === null || _f === void 0 ? void 0 : _f.call(_e, connection, message);
        });
    }
    init(connection) {
        if (!connection.actions) {
            connection.actions = new Map();
        }
        if (!connection.state) {
            connection.state = new signaler_1.Signaler();
            connection.state.on(({ status }) => {
                var _a, _b, _c, _d, _e;
                if (status === common_1.STATE.connected) {
                    for (const action of connection.actions.values()) {
                        if (![common_1.PROTOCOL.init, common_1.PROTOCOL.init_ack].includes(action.message.type)) {
                            this.dispatch(connection, action);
                        }
                    }
                    (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onConnected) === null || _b === void 0 ? void 0 : _b.call(_a, connection);
                }
                else if (status === common_1.STATE.disconnected) {
                    for (const action of connection.actions.values()) {
                        (_c = action.timeout) === null || _c === void 0 ? void 0 : _c.stop();
                        action.status = "inactive";
                    }
                    (_e = (_d = this.options.callbacks) === null || _d === void 0 ? void 0 : _d.onDisconnected) === null || _e === void 0 ? void 0 : _e.call(_d, connection);
                }
            });
        }
        else {
            connection.state.reset();
        }
        if (this.options.timeout && this.options.timeout > 0) {
            if (!connection.timeout) {
                connection.timeout = new timeout_1.Timeout(() => {
                    var _a, _b, _c, _d;
                    (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onTimeout) === null || _b === void 0 ? void 0 : _b.call(_a, connection);
                    // XXX: Simulate a close event.
                    // @ts-ignore: Do not provide an `event` parameter to `onclose`.
                    (_d = (_c = connection.socket).onclose) === null || _d === void 0 ? void 0 : _d.call(_c);
                }, this.options.timeout);
            }
            else {
                connection.timeout.restart();
            }
        }
        if (this.options.keepalive && this.options.keepalive > 0) {
            if (!connection.keepalive) {
                connection.keepalive = new timeout_1.Timeout(() => {
                    this.message(connection, { type: common_1.PROTOCOL.ping });
                    connection.keepalive.timeout = (0, utils_1.random)(0, this.options.keepalive);
                    connection.keepalive.restart();
                }, (0, utils_1.random)(0, this.options.keepalive));
            }
            else {
                connection.keepalive.restart();
            }
        }
        if (!connection.meta) {
            connection.meta = {};
        }
        if (!connection.params) {
            connection.params = {};
        }
    }
    deinit(connection) {
        var _a, _b, _c;
        (_a = connection.keepalive) === null || _a === void 0 ? void 0 : _a.stop();
        (_b = connection.timeout) === null || _b === void 0 ? void 0 : _b.stop();
        this.close(connection);
        if (((_c = connection.state.value) === null || _c === void 0 ? void 0 : _c.status) !== common_1.STATE.disconnected) {
            connection.state.signal({ status: common_1.STATE.disconnected });
        }
    }
    bind(connection) {
        connection.socket.onmessage = this.onmessage.bind(this, connection);
        connection.socket.onerror = this.onerror.bind(this, connection);
        connection.socket.onclose = this.onclose.bind(this, connection);
        connection.socket.onopen = this.onopen.bind(this, connection);
    }
    unbind(connection) {
        connection.socket.onmessage = null;
        connection.socket.onerror = null;
        connection.socket.onclose = null;
        connection.socket.onopen = null;
    }
    attach(connection) {
        if (!connection.id || this.connections.has(connection.id)) {
            let id;
            do {
                id = (0, utils_1.uuidv4)();
            } while (this.connections.has(id));
            connection.id = id;
        }
        this.init(connection);
        this.bind(connection);
        this.connections.set(connection.id, connection);
    }
    detach(connection) {
        this.unbind(connection);
        this.deinit(connection);
        this.connections.delete(connection.id);
    }
    handle(type, handler) {
        this.handlers.set(type, handler);
    }
    send(connection, message) {
        try {
            connection.socket.send(JSON.stringify(message));
        }
        catch (error) {
            console.log("[WebSocketManager send]: failed.", { error, message });
        }
    }
    sendOrThrow(connection, message) {
        connection.socket.send(JSON.stringify(message));
    }
    close(connection, code, reason) {
        return __awaiter(this, void 0, void 0, function* () {
            let promise = connection.state.promise();
            const { status } = yield promise;
            if (status === common_1.STATE.connected) {
                promise = connection.state.next();
                connection.socket.close(code, reason);
            }
            return promise;
        });
    }
    action(connection, message, observers = [], timeout = common_1.TIMEOUT) {
        var _a, _b;
        do {
            message.id = (0, utils_1.uuidv4)();
        } while ((_a = connection.actions) === null || _a === void 0 ? void 0 : _a.has(message.id));
        const action = {
            message,
            observable: new observable_1.Observable(observers),
            close: new signaler_1.Signaler(),
            status: "inactive",
        };
        if (timeout && timeout > 0) {
            action.timeout = new timeout_1.Timeout(() => {
                action.observable.error({
                    errors: ["Action timed out."],
                    action,
                });
                action.observable.complete();
            }, timeout);
        }
        (_b = action.timeout) === null || _b === void 0 ? void 0 : _b.stop();
        action.observable.push({
            error: (data) => {
                console.log("[WebSocketManager action]: error.", data);
            },
            complete: () => {
                this.remove(connection, action);
            },
        });
        this.add(connection, action);
        return action;
    }
    add(connection, action) {
        var _a;
        (_a = connection.actions) === null || _a === void 0 ? void 0 : _a.set(action.message.id, action);
    }
    remove(connection, action) {
        var _a, _b;
        (_a = action.timeout) === null || _a === void 0 ? void 0 : _a.stop();
        action.close.signal();
        (_b = connection.actions) === null || _b === void 0 ? void 0 : _b.delete(action.message.id);
    }
    dispatch(connection, action) {
        var _a, _b;
        if (action.status === "inactive") {
            action.status = "active";
            (_a = action.timeout) === null || _a === void 0 ? void 0 : _a.restart();
            try {
                this.sendOrThrow(connection, action.message);
            }
            catch (error) {
                action.status = "inactive";
                (_b = action.timeout) === null || _b === void 0 ? void 0 : _b.stop();
                console.log("[WebSocketManager dispatch]: failed.", { error, action });
            }
        }
    }
    message(connection, message, observers = [], timeout = common_1.TIMEOUT) {
        const action = this.action(connection, message, observers, timeout);
        connection.state.promise().then(({ status }) => {
            if (status === common_1.STATE.connected) {
                this.dispatch(connection, action);
            }
        });
        return action;
    }
    ping(connection, payload, observers = []) {
        return new Promise((resolve) => {
            let result;
            observers.push({
                next: (message) => {
                    result = message.data;
                },
                complete: () => {
                    resolve(result);
                },
            });
            this.message(connection, { type: common_1.PROTOCOL.ping, data: payload }, observers);
        });
    }
    meta(connection, payload = {}, observers = []) {
        return new Promise((resolve) => {
            let result;
            observers.push({
                next: (message) => {
                    result = message.meta;
                },
                complete: () => {
                    resolve(result);
                },
            });
            this.message(connection, { type: common_1.PROTOCOL.meta, meta: payload }, observers);
        });
    }
    stop() {
        return __awaiter(this, void 0, void 0, function* () {
            const promises = [];
            for (const connection of this.connections.values()) {
                const promise = this.close(connection).then((state) => {
                    this.detach(connection);
                    return state;
                });
                promises.push(promise);
            }
            yield Promise.all(promises);
        });
    }
    onopen(connection, _event) {
        var _a, _b, _c;
        return __awaiter(this, void 0, void 0, function* () {
            let error = false;
            const action = this.action(connection, {
                type: common_1.PROTOCOL.init,
                meta: yield ((_a = connection.metaFunction) === null || _a === void 0 ? void 0 : _a.call(connection, connection.meta)),
            }, [
                {
                    error: () => {
                        error = true;
                    },
                    complete: () => {
                        var _a, _b;
                        if (error) {
                            // Simulate `onclose`.
                            // @ts-ignore: don't pass a CloseEvent to `onclose`.
                            (_b = (_a = connection.socket).onclose) === null || _b === void 0 ? void 0 : _b.call(_a);
                        }
                        else {
                            connection.state.signal({ status: common_1.STATE.connected });
                        }
                    },
                },
            ]);
            this.dispatch(connection, action);
            (_c = (_b = this.options.callbacks) === null || _b === void 0 ? void 0 : _b.onOpen) === null || _c === void 0 ? void 0 : _c.call(_b, connection);
        });
    }
    onmessage(connection, event) {
        var _a, _b, _c;
        const message = JSON.parse(event.data);
        (_a = this.handlers.get(message.type)) === null || _a === void 0 ? void 0 : _a(connection, message);
        (_c = (_b = this.options.callbacks) === null || _b === void 0 ? void 0 : _b.onMessage) === null || _c === void 0 ? void 0 : _c.call(_b, connection, message);
    }
    onerror(connection, event) {
        var _a, _b;
        (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onError) === null || _b === void 0 ? void 0 : _b.call(_a, connection, event);
    }
    onclose(connection, _event) {
        var _a, _b;
        (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onClose) === null || _b === void 0 ? void 0 : _b.call(_a, connection);
        this.detach(connection);
    }
}
exports.WebSocketManager = WebSocketManager;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9tYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLHFDQWFrQjtBQUNsQix5Q0FBc0M7QUFDdEMsdUNBQW9DO0FBQ3BDLDZDQUFvRDtBQUNwRCxtQ0FBd0Q7QUErQnhELE1BQWEsZ0JBQWdCO0lBUTNCLFlBQVksT0FBaUM7UUFDM0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUMxQjtZQUNFLFNBQVMsRUFBRSxrQkFBUztZQUNwQixPQUFPLEVBQUUsZ0JBQU87WUFDaEIsU0FBUyxFQUFFLEVBQUU7U0FDZCxFQUNELE9BQU8sQ0FDUixDQUFDO1FBQ0YsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFBLGNBQU0sR0FBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxHQUFHLEVBQXNCLENBQUM7UUFDakQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLEdBQUcsRUFBbUIsQ0FBQztRQUMzQyxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFRLENBQUMsSUFBSSxFQUFFLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxFQUFFOztZQUNqRCxJQUFJO2dCQUNGLFVBQVUsQ0FBQyxJQUFJLEdBQUcsSUFBQSxxQkFBYSxFQUM3QixVQUFVLENBQUMsSUFBSyxFQUNoQixPQUFPLENBQUMsSUFBYyxDQUNaLENBQUM7YUFDZDtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsNENBQTRDLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ2pFLE9BQU8sVUFBVSxDQUFDLElBQUksQ0FBQztnQkFDdkIsT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDO2dCQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN2QixPQUFPO2FBQ1I7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO2dCQUNkLElBQUksRUFBRSxpQkFBUSxDQUFDLFFBQVE7Z0JBQ3ZCLE1BQU0sRUFBRTtvQkFDTixPQUFPLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0JBQ2hCLFVBQVUsRUFBRSxVQUFVLENBQUMsRUFBRTtpQkFDMUI7Z0JBQ0QsSUFBSSxFQUFFLFVBQVUsQ0FBQyxJQUFLO2FBQ3ZCLENBQUMsQ0FBQztZQUNILE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsTUFBTSxtREFBRyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxFQUFFOztZQUNyRCxJQUFJO2dCQUNGLFVBQVUsQ0FBQyxJQUFJLEdBQUcsSUFBQSxxQkFBYSxFQUM3QixVQUFVLENBQUMsSUFBSyxFQUNoQixPQUFPLENBQUMsSUFBYyxDQUNaLENBQUM7YUFDZDtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0RBQWdELEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ3JFLE9BQU8sVUFBVSxDQUFDLElBQUksQ0FBQztnQkFDdkIsT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDO2dCQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN2QixPQUFPO2FBQ1I7WUFDRCxNQUFBLE1BQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLDBDQUFFLFNBQVMsbURBQUcsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3pELE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFnQixDQUFDO1lBQ3hDLFVBQVUsQ0FBQyxNQUFNLHFCQUFRLE1BQU0sQ0FBRSxDQUFDO1lBQ2xDLE1BQUEsTUFBQSxVQUFVLENBQUMsT0FBTywwQ0FBRSxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FBRSxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3hFLE1BQUEsTUFBQSxVQUFVLENBQUMsT0FBTywwQ0FBRSxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FBRSxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDdkUsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFRLENBQUMsSUFBSSxFQUFFLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxFQUFFOztZQUNqRCxNQUFBLFVBQVUsQ0FBQyxPQUFPLDBDQUFFLE9BQU8sRUFBRSxDQUFDO1lBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNwQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7Z0JBQ2QsSUFBSSxFQUFFLGlCQUFRLENBQUMsSUFBSTtnQkFDbkIsSUFBSSxFQUFFLE9BQU8sQ0FBQyxJQUFJO2FBQ25CLENBQUMsQ0FBQztZQUNILE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsTUFBTSxtREFBRyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFRLENBQUMsSUFBSSxFQUFFLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxFQUFFOztZQUNqRCxNQUFBLFVBQVUsQ0FBQyxPQUFPLDBDQUFFLE9BQU8sRUFBRSxDQUFDO1lBQzlCLE1BQUEsTUFBQSxVQUFVLENBQUMsT0FBTywwQ0FBRSxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FBRSxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3hFLE1BQUEsTUFBQSxVQUFVLENBQUMsT0FBTywwQ0FBRSxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FBRSxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDckUsTUFBQSxNQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUywwQ0FBRSxNQUFNLG1EQUFHLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN4RCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLEVBQUU7O1lBQ2pELElBQUk7Z0JBQ0YsVUFBVSxDQUFDLElBQUksR0FBRyxJQUFBLHFCQUFhLEVBQzdCLFVBQVUsQ0FBQyxJQUFLLEVBQ2hCLE9BQU8sQ0FBQyxJQUFjLENBQ1osQ0FBQzthQUNkO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyw0Q0FBNEMsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDakUsT0FBTyxVQUFVLENBQUMsSUFBSSxDQUFDO2dCQUN2QixPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3ZCLE9BQU87YUFDUjtZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNwQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7Z0JBQ2QsSUFBSSxFQUFFLGlCQUFRLENBQUMsUUFBUTtnQkFDdkIsSUFBSSxFQUFFLFVBQVUsQ0FBQyxJQUFLO2FBQ3ZCLENBQUMsQ0FBQztZQUNILE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsTUFBTSxtREFBRyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxFQUFFOztZQUNyRCxJQUFJO2dCQUNGLFVBQVUsQ0FBQyxJQUFJLEdBQUcsSUFBQSxxQkFBYSxFQUM3QixVQUFVLENBQUMsSUFBSyxFQUNoQixPQUFPLENBQUMsSUFBYyxDQUNaLENBQUM7YUFDZDtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0RBQWdELEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ3JFLE9BQU8sVUFBVSxDQUFDLElBQUksQ0FBQztnQkFDdkIsT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDO2dCQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN2QixPQUFPO2FBQ1I7WUFDRCxNQUFBLE1BQUEsVUFBVSxDQUFDLE9BQU8sMENBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxFQUFZLENBQUMsMENBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN4RSxNQUFBLE1BQUEsVUFBVSxDQUFDLE9BQU8sMENBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxFQUFZLENBQUMsMENBQUUsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3JFLE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsU0FBUyxtREFBRyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDM0QsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sSUFBSSxDQUFDLFVBQXNCO1FBQ2hDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFO1lBQ3ZCLFVBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxHQUFHLEVBQTJCLENBQUM7U0FDekQ7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRTtZQUNyQixVQUFVLENBQUMsS0FBSyxHQUFHLElBQUksbUJBQVEsRUFBUyxDQUFDO1lBQ3pDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFOztnQkFDakMsSUFBSSxNQUFNLEtBQUssY0FBSyxDQUFDLFNBQVMsRUFBRTtvQkFDOUIsS0FBSyxNQUFNLE1BQU0sSUFBSSxVQUFVLENBQUMsT0FBUSxDQUFDLE1BQU0sRUFBRSxFQUFFO3dCQUNqRCxJQUNFLENBQUMsQ0FBQyxpQkFBUSxDQUFDLElBQUksRUFBRSxpQkFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FDMUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFnQixDQUNoQyxFQUNEOzRCQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO3lCQUNuQztxQkFDRjtvQkFDRCxNQUFBLE1BQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLDBDQUFFLFdBQVcsbURBQUcsVUFBVSxDQUFDLENBQUM7aUJBQ25EO3FCQUFNLElBQUksTUFBTSxLQUFLLGNBQUssQ0FBQyxZQUFZLEVBQUU7b0JBQ3hDLEtBQUssTUFBTSxNQUFNLElBQUksVUFBVSxDQUFDLE9BQVEsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDakQsTUFBQSxNQUFNLENBQUMsT0FBTywwQ0FBRSxJQUFJLEVBQUUsQ0FBQzt3QkFDdkIsTUFBTSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7cUJBQzVCO29CQUNELE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsY0FBYyxtREFBRyxVQUFVLENBQUMsQ0FBQztpQkFDdEQ7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQzFCO1FBQ0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxDQUFDLEVBQUU7WUFDcEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3ZCLFVBQVUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxpQkFBTyxDQUFDLEdBQUcsRUFBRTs7b0JBQ3BDLE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsU0FBUyxtREFBRyxVQUFVLENBQUMsQ0FBQztvQkFDaEQsK0JBQStCO29CQUMvQixnRUFBZ0U7b0JBQ2hFLE1BQUEsTUFBQSxVQUFVLENBQUMsTUFBTSxFQUFDLE9BQU8sa0RBQUksQ0FBQztnQkFDaEMsQ0FBQyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBUSxDQUFDLENBQUM7YUFDM0I7aUJBQU07Z0JBQ0wsVUFBVSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUM5QjtTQUNGO1FBQ0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxDQUFDLEVBQUU7WUFDeEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUU7Z0JBQ3pCLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxpQkFBTyxDQUFDLEdBQUcsRUFBRTtvQkFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxJQUFJLEVBQUUsaUJBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO29CQUNsRCxVQUFVLENBQUMsU0FBVSxDQUFDLE9BQU8sR0FBRyxJQUFBLGNBQU0sRUFBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFVLENBQUMsQ0FBQztvQkFDbkUsVUFBVSxDQUFDLFNBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDbEMsQ0FBQyxFQUFFLElBQUEsY0FBTSxFQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVUsQ0FBQyxDQUFDLENBQUM7YUFDeEM7aUJBQU07Z0JBQ0wsVUFBVSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUNoQztTQUNGO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUU7WUFDcEIsVUFBVSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7U0FDdEI7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRTtZQUN0QixVQUFVLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztTQUN4QjtJQUNILENBQUM7SUFFTSxNQUFNLENBQUMsVUFBc0I7O1FBQ2xDLE1BQUEsVUFBVSxDQUFDLFNBQVMsMENBQUUsSUFBSSxFQUFFLENBQUM7UUFDN0IsTUFBQSxVQUFVLENBQUMsT0FBTywwQ0FBRSxJQUFJLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQSxNQUFBLFVBQVUsQ0FBQyxLQUFNLENBQUMsS0FBSywwQ0FBRSxNQUFNLE1BQUssY0FBSyxDQUFDLFlBQVksRUFBRTtZQUMxRCxVQUFVLENBQUMsS0FBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE1BQU0sRUFBRSxjQUFLLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztTQUMxRDtJQUNILENBQUM7SUFFTSxJQUFJLENBQUMsVUFBc0I7UUFDaEMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ3BFLFVBQVUsQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQztRQUNoRSxVQUFVLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDaEUsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFTSxNQUFNLENBQUMsVUFBc0I7UUFDbEMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ25DLFVBQVUsQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNqQyxVQUFVLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDakMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ2xDLENBQUM7SUFFTSxNQUFNLENBQUMsVUFBc0I7UUFDbEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxFQUFFO1lBQ3pELElBQUksRUFBVSxDQUFDO1lBQ2YsR0FBRztnQkFDRCxFQUFFLEdBQUcsSUFBQSxjQUFNLEdBQUUsQ0FBQzthQUNmLFFBQVEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDbkMsVUFBVSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUM7U0FDcEI7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRU0sTUFBTSxDQUFDLFVBQXNCO1FBQ2xDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDeEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsRUFBRyxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVNLE1BQU0sQ0FBQyxJQUFZLEVBQUUsT0FBZ0I7UUFDMUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFTSxJQUFJLENBQUMsVUFBc0IsRUFBRSxPQUFnQjtRQUNsRCxJQUFJO1lBQ0YsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1NBQ2pEO1FBQUMsT0FBTyxLQUFLLEVBQUU7WUFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxFQUFFLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7U0FDckU7SUFDSCxDQUFDO0lBRU0sV0FBVyxDQUFDLFVBQXNCLEVBQUUsT0FBZ0I7UUFDekQsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFWSxLQUFLLENBQ2hCLFVBQXNCLEVBQ3RCLElBQWEsRUFDYixNQUFlOztZQUVmLElBQUksT0FBTyxHQUFHLFVBQVUsQ0FBQyxLQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDMUMsTUFBTSxFQUFFLE1BQU0sRUFBRSxHQUFHLE1BQU0sT0FBTyxDQUFDO1lBQ2pDLElBQUksTUFBTSxLQUFLLGNBQUssQ0FBQyxTQUFTLEVBQUU7Z0JBQzlCLE9BQU8sR0FBRyxVQUFVLENBQUMsS0FBTSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNuQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7YUFDdkM7WUFDRCxPQUFPLE9BQU8sQ0FBQztRQUNqQixDQUFDO0tBQUE7SUFFTSxNQUFNLENBQ1gsVUFBc0IsRUFDdEIsT0FBZ0IsRUFDaEIsWUFBaUMsRUFBRSxFQUNuQyxPQUFPLEdBQUcsZ0JBQU87O1FBRWpCLEdBQUc7WUFDRCxPQUFPLENBQUMsRUFBRSxHQUFHLElBQUEsY0FBTSxHQUFFLENBQUM7U0FDdkIsUUFBUSxNQUFBLFVBQVUsQ0FBQyxPQUFPLDBDQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBWSxDQUFDLEVBQUU7UUFDeEQsTUFBTSxNQUFNLEdBQW9CO1lBQzlCLE9BQU87WUFDUCxVQUFVLEVBQUUsSUFBSSx1QkFBVSxDQUFVLFNBQVMsQ0FBQztZQUM5QyxLQUFLLEVBQUUsSUFBSSxtQkFBUSxFQUFRO1lBQzNCLE1BQU0sRUFBRSxVQUFVO1NBQ25CLENBQUM7UUFDRixJQUFJLE9BQU8sSUFBSSxPQUFPLEdBQUcsQ0FBQyxFQUFFO1lBQzFCLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxpQkFBTyxDQUFDLEdBQUcsRUFBRTtnQkFDaEMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUM7b0JBQ3RCLE1BQU0sRUFBRSxDQUFDLG1CQUFtQixDQUFDO29CQUM3QixNQUFNO2lCQUNQLENBQUMsQ0FBQztnQkFDSCxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQy9CLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztTQUNiO1FBQ0QsTUFBQSxNQUFNLENBQUMsT0FBTywwQ0FBRSxJQUFJLEVBQUUsQ0FBQztRQUN2QixNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztZQUNyQixLQUFLLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRTtnQkFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLG1DQUFtQyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3pELENBQUM7WUFDRCxRQUFRLEVBQUUsR0FBRyxFQUFFO2dCQUNiLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ2xDLENBQUM7U0FDRixDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM3QixPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBRU0sR0FBRyxDQUFDLFVBQXNCLEVBQUUsTUFBdUI7O1FBQ3hELE1BQUEsVUFBVSxDQUFDLE9BQU8sMENBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBWSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFTSxNQUFNLENBQUMsVUFBc0IsRUFBRSxNQUF1Qjs7UUFDM0QsTUFBQSxNQUFNLENBQUMsT0FBTywwQ0FBRSxJQUFJLEVBQUUsQ0FBQztRQUN2QixNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3RCLE1BQUEsVUFBVSxDQUFDLE9BQU8sMENBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBWSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVNLFFBQVEsQ0FBQyxVQUFzQixFQUFFLE1BQXVCOztRQUM3RCxJQUFJLE1BQU0sQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO1lBQ2hDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDO1lBQ3pCLE1BQUEsTUFBTSxDQUFDLE9BQU8sMENBQUUsT0FBTyxFQUFFLENBQUM7WUFDMUIsSUFBSTtnQkFDRixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDOUM7WUFBQyxPQUFPLEtBQUssRUFBRTtnQkFDZCxNQUFNLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztnQkFDM0IsTUFBQSxNQUFNLENBQUMsT0FBTywwQ0FBRSxJQUFJLEVBQUUsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQ0FBc0MsRUFBRSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO2FBQ3hFO1NBQ0Y7SUFDSCxDQUFDO0lBRU0sT0FBTyxDQUNaLFVBQXNCLEVBQ3RCLE9BQWdCLEVBQ2hCLFlBQWlDLEVBQUUsRUFDbkMsT0FBTyxHQUFHLGdCQUFPO1FBRWpCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDcEUsVUFBVSxDQUFDLEtBQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUU7WUFDOUMsSUFBSSxNQUFNLEtBQUssY0FBSyxDQUFDLFNBQVMsRUFBRTtnQkFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7YUFDbkM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxJQUFJLENBQ1QsVUFBc0IsRUFDdEIsT0FBZ0IsRUFDaEIsWUFBaUMsRUFBRTtRQUVuQyxPQUFPLElBQUksT0FBTyxDQUFVLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDdEMsSUFBSSxNQUFlLENBQUM7WUFDcEIsU0FBUyxDQUFDLElBQUksQ0FBQztnQkFDYixJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRTtvQkFDaEIsTUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFlLENBQUM7Z0JBQ25DLENBQUM7Z0JBQ0QsUUFBUSxFQUFFLEdBQUcsRUFBRTtvQkFDYixPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ2xCLENBQUM7YUFDRixDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsT0FBTyxDQUNWLFVBQVUsRUFDVixFQUFFLElBQUksRUFBRSxpQkFBUSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQ3RDLFNBQVMsQ0FDVixDQUFDO1FBQ0osQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sSUFBSSxDQUNULFVBQXNCLEVBQ3RCLFVBQW1CLEVBQUUsRUFDckIsWUFBaUMsRUFBRTtRQUVuQyxPQUFPLElBQUksT0FBTyxDQUFVLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDdEMsSUFBSSxNQUFlLENBQUM7WUFDcEIsU0FBUyxDQUFDLElBQUksQ0FBQztnQkFDYixJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRTtvQkFDaEIsTUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFlLENBQUM7Z0JBQ25DLENBQUM7Z0JBQ0QsUUFBUSxFQUFFLEdBQUcsRUFBRTtvQkFDYixPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ2xCLENBQUM7YUFDRixDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsT0FBTyxDQUNWLFVBQVUsRUFDVixFQUFFLElBQUksRUFBRSxpQkFBUSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQ3RDLFNBQVMsQ0FDVixDQUFDO1FBQ0osQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRVksSUFBSTs7WUFDZixNQUFNLFFBQVEsR0FBcUIsRUFBRSxDQUFDO1lBQ3RDLEtBQUssTUFBTSxVQUFVLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsRUFBRTtnQkFDbEQsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDcEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDeEIsT0FBTyxLQUFLLENBQUM7Z0JBQ2YsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUN4QjtZQUNELE1BQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM5QixDQUFDO0tBQUE7SUFFZSxNQUFNLENBQUMsVUFBc0IsRUFBRSxNQUFhOzs7WUFDMUQsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ2xCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQ3hCLFVBQVUsRUFDVjtnQkFDRSxJQUFJLEVBQUUsaUJBQVEsQ0FBQyxJQUFJO2dCQUNuQixJQUFJLEVBQUUsTUFBTSxDQUFBLE1BQUEsVUFBVSxDQUFDLFlBQVksMkRBQUcsVUFBVSxDQUFDLElBQUssQ0FBQyxDQUFBO2FBQ3hELEVBQ0Q7Z0JBQ0U7b0JBQ0UsS0FBSyxFQUFFLEdBQUcsRUFBRTt3QkFDVixLQUFLLEdBQUcsSUFBSSxDQUFDO29CQUNmLENBQUM7b0JBQ0QsUUFBUSxFQUFFLEdBQUcsRUFBRTs7d0JBQ2IsSUFBSSxLQUFLLEVBQUU7NEJBQ1Qsc0JBQXNCOzRCQUN0QixvREFBb0Q7NEJBQ3BELE1BQUEsTUFBQSxVQUFVLENBQUMsTUFBTSxFQUFDLE9BQU8sa0RBQUksQ0FBQzt5QkFDL0I7NkJBQU07NEJBQ0wsVUFBVSxDQUFDLEtBQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxNQUFNLEVBQUUsY0FBSyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7eUJBQ3ZEO29CQUNILENBQUM7aUJBQ0Y7YUFDRixDQUNGLENBQUM7WUFDRixJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUNsQyxNQUFBLE1BQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLDBDQUFFLE1BQU0sbURBQUcsVUFBVSxDQUFDLENBQUM7O0tBQzlDO0lBRVMsU0FBUyxDQUFDLFVBQXNCLEVBQUUsS0FBbUI7O1FBQzdELE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZDLE1BQUEsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQywwQ0FBRyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDdkQsTUFBQSxNQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUywwQ0FBRSxTQUFTLG1EQUFHLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRVMsT0FBTyxDQUFDLFVBQXNCLEVBQUUsS0FBeUI7O1FBQ2pFLE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsT0FBTyxtREFBRyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVTLE9BQU8sQ0FBQyxVQUFzQixFQUFFLE1BQWtCOztRQUMxRCxNQUFBLE1BQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLDBDQUFFLE9BQU8sbURBQUcsVUFBVSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMxQixDQUFDO0NBQ0Y7QUExYUQsNENBMGFDIn0=