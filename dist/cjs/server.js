"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebSocketServer = void 0;
const common_1 = require("./common");
const manager_1 = require("./manager");
class WebSocketServer extends manager_1.WebSocketManager {
    constructor(options = {}) {
        super(Object.assign({ keepalive: common_1.KEEPALIVE }, options));
    }
    deinit(connection) {
        var _a, _b;
        if (((_b = (_a = connection.state) === null || _a === void 0 ? void 0 : _a.value) === null || _b === void 0 ? void 0 : _b.status) === common_1.STATE.connected) {
            for (const action of connection.actions.values()) {
                action.observable.complete();
            }
            this.close(connection);
            super.deinit(connection);
        }
    }
    stop() {
        const _super = Object.create(null, {
            stop: { get: () => super.stop }
        });
        return __awaiter(this, void 0, void 0, function* () {
            yield _super.stop.call(this);
            for (const connection of this.connections.values()) {
                for (const action of connection.actions.values()) {
                    this.remove(connection, action);
                }
            }
        });
    }
}
exports.WebSocketServer = WebSocketServer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL3NlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxxQ0FBd0Q7QUFDeEQsdUNBQXNFO0FBSXRFLE1BQWEsZUFBZ0IsU0FBUSwwQkFBZ0I7SUFHbkQsWUFBWSxVQUFrQyxFQUFFO1FBQzlDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsU0FBUyxFQUFFLGtCQUFTLEVBQUUsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFTSxNQUFNLENBQUMsVUFBc0I7O1FBQ2xDLElBQUksQ0FBQSxNQUFBLE1BQUEsVUFBVSxDQUFDLEtBQUssMENBQUUsS0FBSywwQ0FBRSxNQUFNLE1BQUssY0FBSyxDQUFDLFNBQVMsRUFBRTtZQUN2RCxLQUFLLE1BQU0sTUFBTSxJQUFJLFVBQVUsQ0FBQyxPQUFRLENBQUMsTUFBTSxFQUFFLEVBQUU7Z0JBQ2pELE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDOUI7WUFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3ZCLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDMUI7SUFDSCxDQUFDO0lBRVksSUFBSTs7Ozs7WUFDZixNQUFNLE9BQU0sSUFBSSxXQUFFLENBQUM7WUFDbkIsS0FBSyxNQUFNLFVBQVUsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxFQUFFO2dCQUNsRCxLQUFLLE1BQU0sTUFBTSxJQUFJLFVBQVUsQ0FBQyxPQUFRLENBQUMsTUFBTSxFQUFFLEVBQUU7b0JBQ2pELElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2lCQUNqQzthQUNGO1FBQ0gsQ0FBQztLQUFBO0NBQ0Y7QUF6QkQsMENBeUJDIn0=