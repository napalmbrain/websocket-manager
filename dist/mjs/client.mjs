var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { TIMEOUT } from "./common.mjs";
import { WebSocketManager } from "./manager.mjs";
import { random, sleep } from "./utils.mjs";
export class WebSocketClient extends WebSocketManager {
    constructor(options = {}) {
        super(Object.assign({
            keepalive: -1,
            timeout: TIMEOUT,
            reconnect: true,
            callbacks: {},
        }, options));
    }
    connect(url, meta) {
        const socket = new WebSocket(url);
        const connection = {
            socket,
            metaFunction: meta,
            params: {
                url,
            },
        };
        this.attach(connection);
        return connection;
    }
    reconnect(connection) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.close(connection);
            this.detach(connection);
            connection.socket = new WebSocket(connection.params.url);
            this.attach(connection);
        });
    }
    onclose(connection, event) {
        const _super = Object.create(null, {
            onclose: { get: () => super.onclose }
        });
        return __awaiter(this, void 0, void 0, function* () {
            _super.onclose.call(this, connection, event);
            if (this.options.reconnect) {
                yield sleep(random(0, TIMEOUT));
                this.reconnect(connection);
            }
        });
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL2NsaWVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQSxPQUFPLEVBQXVDLE9BQU8sRUFBRSxNQUFNLFVBQVUsQ0FBQztBQUN4RSxPQUFPLEVBQUUsZ0JBQWdCLEVBQTJCLE1BQU0sV0FBVyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBTXhDLE1BQU0sT0FBTyxlQUFnQixTQUFRLGdCQUFnQjtJQUduRCxZQUFZLFVBQWtDLEVBQUU7UUFDOUMsS0FBSyxDQUNILE1BQU0sQ0FBQyxNQUFNLENBQ1g7WUFDRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO1lBQ2IsT0FBTyxFQUFFLE9BQU87WUFDaEIsU0FBUyxFQUFFLElBQUk7WUFDZixTQUFTLEVBQUUsRUFBRTtTQUNkLEVBQ0QsT0FBTyxDQUNSLENBQ0YsQ0FBQztJQUNKLENBQUM7SUFFTSxPQUFPLENBQUMsR0FBaUIsRUFBRSxJQUFtQjtRQUNuRCxNQUFNLE1BQU0sR0FBRyxJQUFJLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNsQyxNQUFNLFVBQVUsR0FBZTtZQUM3QixNQUFNO1lBQ04sWUFBWSxFQUFFLElBQUk7WUFDbEIsTUFBTSxFQUFFO2dCQUNOLEdBQUc7YUFDSjtTQUNGLENBQUM7UUFDRixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3hCLE9BQU8sVUFBVSxDQUFDO0lBQ3BCLENBQUM7SUFFWSxTQUFTLENBQUMsVUFBc0I7O1lBQzNDLE1BQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hCLFVBQVUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU8sQ0FBQyxHQUFtQixDQUFDLENBQUM7WUFDMUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMxQixDQUFDO0tBQUE7SUFFZSxPQUFPLENBQUMsVUFBc0IsRUFBRSxLQUFpQjs7Ozs7WUFDL0QsT0FBTSxPQUFPLFlBQUMsVUFBVSxFQUFFLEtBQUssRUFBRTtZQUNqQyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFO2dCQUMxQixNQUFNLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDNUI7UUFDSCxDQUFDO0tBQUE7Q0FDRiJ9