var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
// @ts-ignore: bun import will fail in node.
import { serve } from "bun";
import { WebSocketServer } from "../server.mjs";
import { WebSocket } from "./websocket.mjs";
export class BunWebSocketServer extends WebSocketServer {
    listen(options = { port: 8000 }) {
        this.server = serve(Object.assign(Object.assign({}, options), { fetch: (request, server) => {
                const connection = {};
                server.upgrade(request, {
                    data: { connection },
                });
            }, websocket: {
                open: (ws) => {
                    var _a, _b;
                    ws.data.connection.socket = new WebSocket(ws);
                    this.attach(ws.data.connection);
                    const event = new Event("open");
                    (_b = (_a = ws.data.connection.socket).onopen) === null || _b === void 0 ? void 0 : _b.call(_a, event);
                },
                message: (ws, message) => {
                    var _a, _b;
                    const event = new MessageEvent("message", { data: message });
                    (_b = (_a = ws.data.connection.socket).onmessage) === null || _b === void 0 ? void 0 : _b.call(_a, event);
                },
                error: (ws, error) => {
                    var _a, _b;
                    const event = new ErrorEvent("error", { message: error.message });
                    (_b = (_a = ws.data.connection.socket).onerror) === null || _b === void 0 ? void 0 : _b.call(_a, event);
                },
                close: (ws) => {
                    var _a, _b;
                    const event = new CloseEvent("close");
                    (_b = (_a = ws.data.connection.socket).onclose) === null || _b === void 0 ? void 0 : _b.call(_a, event);
                },
            } }));
    }
    stop() {
        const _super = Object.create(null, {
            stop: { get: () => super.stop }
        });
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            yield _super.stop.call(this);
            (_a = this.server) === null || _a === void 0 ? void 0 : _a.stop();
        });
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2J1bi9zZXJ2ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsNENBQTRDO0FBQzVDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxLQUFLLENBQUM7QUFDNUIsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUU1QyxPQUFPLEVBQUUsU0FBUyxFQUFtQixNQUFNLGFBQWEsQ0FBQztBQThCekQsTUFBTSxPQUFPLGtCQUFtQixTQUFRLGVBQWU7SUFHOUMsTUFBTSxDQUNYLFVBQWdELEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRTtRQUU5RCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssaUNBQ2QsT0FBTyxLQUNWLEtBQUssRUFBRSxDQUFDLE9BQWdCLEVBQUUsTUFBZ0IsRUFBRSxFQUFFO2dCQUM1QyxNQUFNLFVBQVUsR0FBRyxFQUFnQixDQUFDO2dCQUNwQyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRTtvQkFDdEIsSUFBSSxFQUFFLEVBQUUsVUFBVSxFQUFFO2lCQUNyQixDQUFDLENBQUM7WUFDTCxDQUFDLEVBQ0QsU0FBUyxFQUFFO2dCQUNULElBQUksRUFBRSxDQUFDLEVBQW1CLEVBQUUsRUFBRTs7b0JBQzVCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDOUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUNoQyxNQUFNLEtBQUssR0FBRyxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDaEMsTUFBQSxNQUFBLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBQyxNQUFNLG1EQUFHLEtBQUssQ0FBQyxDQUFDO2dCQUM1QyxDQUFDO2dCQUNELE9BQU8sRUFBRSxDQUFDLEVBQW1CLEVBQUUsT0FBZSxFQUFFLEVBQUU7O29CQUNoRCxNQUFNLEtBQUssR0FBRyxJQUFJLFlBQVksQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztvQkFDN0QsTUFBQSxNQUFBLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBQyxTQUFTLG1EQUFHLEtBQUssQ0FBQyxDQUFDO2dCQUMvQyxDQUFDO2dCQUNELEtBQUssRUFBRSxDQUFDLEVBQW1CLEVBQUUsS0FBWSxFQUFFLEVBQUU7O29CQUMzQyxNQUFNLEtBQUssR0FBRyxJQUFJLFVBQVUsQ0FBQyxPQUFPLEVBQUUsRUFBRSxPQUFPLEVBQUUsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7b0JBQ2xFLE1BQUEsTUFBQSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUMsT0FBTyxtREFBRyxLQUFLLENBQUMsQ0FBQztnQkFDN0MsQ0FBQztnQkFDRCxLQUFLLEVBQUUsQ0FBQyxFQUFtQixFQUFFLEVBQUU7O29CQUM3QixNQUFNLEtBQUssR0FBRyxJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDdEMsTUFBQSxNQUFBLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBQyxPQUFPLG1EQUFHLEtBQUssQ0FBQyxDQUFDO2dCQUM3QyxDQUFDO2FBQ0YsSUFDRCxDQUFDO0lBQ0wsQ0FBQztJQUVZLElBQUk7Ozs7OztZQUNmLE1BQU0sT0FBTSxJQUFJLFdBQUUsQ0FBQztZQUNuQixNQUFBLElBQUksQ0FBQyxNQUFNLDBDQUFFLElBQUksRUFBRSxDQUFDOztLQUNyQjtDQUNGIn0=