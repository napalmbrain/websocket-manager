var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { KEEPALIVE, PROTOCOL, STATE, TIMEOUT, } from "./common.mjs";
import { Signaler } from "./signaler.mjs";
import { Timeout } from "./timeout.mjs";
import { Observable } from "./observable.mjs";
import { random, limitedAssign, uuidv4 } from "./utils.mjs";
export class WebSocketManager {
    constructor(options) {
        this.options = Object.assign({
            keepalive: KEEPALIVE,
            timeout: TIMEOUT,
            callbacks: {},
        }, options);
        this.id = uuidv4();
        this.connections = new Map();
        this.handlers = new Map();
        this.handle(PROTOCOL.init, (connection, message) => {
            var _a, _b;
            try {
                connection.meta = limitedAssign(connection.meta, message.meta);
            }
            catch (error) {
                console.log("[WebSocketManager handle(PROTOCOL.init)]: ", error);
                delete connection.meta;
                delete message.meta;
                this.close(connection);
                return;
            }
            this.send(connection, {
                id: message.id,
                type: PROTOCOL.init_ack,
                remote: {
                    manager: this.id,
                    connection: connection.id,
                },
                meta: connection.meta,
            });
            (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onInit) === null || _b === void 0 ? void 0 : _b.call(_a, connection, message);
        });
        this.handle(PROTOCOL.init_ack, (connection, message) => {
            var _a, _b, _c, _d, _e, _f;
            try {
                connection.meta = limitedAssign(connection.meta, message.meta);
            }
            catch (error) {
                console.log("[WebSocketManager handle(PROTOCOL.init_ack)]: ", error);
                delete connection.meta;
                delete message.meta;
                this.close(connection);
                return;
            }
            (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onInitAck) === null || _b === void 0 ? void 0 : _b.call(_a, connection, message);
            const remote = message.remote;
            connection.remote = Object.assign({}, remote);
            (_d = (_c = connection.actions) === null || _c === void 0 ? void 0 : _c.get(message.id)) === null || _d === void 0 ? void 0 : _d.observable.next(message);
            (_f = (_e = connection.actions) === null || _e === void 0 ? void 0 : _e.get(message.id)) === null || _f === void 0 ? void 0 : _f.observable.complete();
        });
        this.handle(PROTOCOL.ping, (connection, message) => {
            var _a, _b, _c;
            (_a = connection.timeout) === null || _a === void 0 ? void 0 : _a.restart();
            this.send(connection, {
                id: message.id,
                type: PROTOCOL.pong,
                data: message.data,
            });
            (_c = (_b = this.options.callbacks) === null || _b === void 0 ? void 0 : _b.onPing) === null || _c === void 0 ? void 0 : _c.call(_b, connection, message);
        });
        this.handle(PROTOCOL.pong, (connection, message) => {
            var _a, _b, _c, _d, _e, _f, _g;
            (_a = connection.timeout) === null || _a === void 0 ? void 0 : _a.restart();
            (_c = (_b = connection.actions) === null || _b === void 0 ? void 0 : _b.get(message.id)) === null || _c === void 0 ? void 0 : _c.observable.next(message);
            (_e = (_d = connection.actions) === null || _d === void 0 ? void 0 : _d.get(message.id)) === null || _e === void 0 ? void 0 : _e.observable.complete();
            (_g = (_f = this.options.callbacks) === null || _f === void 0 ? void 0 : _f.onPong) === null || _g === void 0 ? void 0 : _g.call(_f, connection, message);
        });
        this.handle(PROTOCOL.meta, (connection, message) => {
            var _a, _b;
            try {
                connection.meta = limitedAssign(connection.meta, message.meta);
            }
            catch (error) {
                console.log("[WebSocketManager handle(PROTOCOL.meta)]: ", error);
                delete connection.meta;
                delete message.meta;
                this.close(connection);
                return;
            }
            this.send(connection, {
                id: message.id,
                type: PROTOCOL.meta_ack,
                meta: connection.meta,
            });
            (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onMeta) === null || _b === void 0 ? void 0 : _b.call(_a, connection, message);
        });
        this.handle(PROTOCOL.meta_ack, (connection, message) => {
            var _a, _b, _c, _d, _e, _f;
            try {
                connection.meta = limitedAssign(connection.meta, message.meta);
            }
            catch (error) {
                console.log("[WebSocketManager handle(PROTOCOL.meta_ack)]: ", error);
                delete connection.meta;
                delete message.meta;
                this.close(connection);
                return;
            }
            (_b = (_a = connection.actions) === null || _a === void 0 ? void 0 : _a.get(message.id)) === null || _b === void 0 ? void 0 : _b.observable.next(message);
            (_d = (_c = connection.actions) === null || _c === void 0 ? void 0 : _c.get(message.id)) === null || _d === void 0 ? void 0 : _d.observable.complete();
            (_f = (_e = this.options.callbacks) === null || _e === void 0 ? void 0 : _e.onMetaAck) === null || _f === void 0 ? void 0 : _f.call(_e, connection, message);
        });
    }
    init(connection) {
        if (!connection.actions) {
            connection.actions = new Map();
        }
        if (!connection.state) {
            connection.state = new Signaler();
            connection.state.on(({ status }) => {
                var _a, _b, _c, _d, _e;
                if (status === STATE.connected) {
                    for (const action of connection.actions.values()) {
                        if (![PROTOCOL.init, PROTOCOL.init_ack].includes(action.message.type)) {
                            this.dispatch(connection, action);
                        }
                    }
                    (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onConnected) === null || _b === void 0 ? void 0 : _b.call(_a, connection);
                }
                else if (status === STATE.disconnected) {
                    for (const action of connection.actions.values()) {
                        (_c = action.timeout) === null || _c === void 0 ? void 0 : _c.stop();
                        action.status = "inactive";
                    }
                    (_e = (_d = this.options.callbacks) === null || _d === void 0 ? void 0 : _d.onDisconnected) === null || _e === void 0 ? void 0 : _e.call(_d, connection);
                }
            });
        }
        else {
            connection.state.reset();
        }
        if (this.options.timeout && this.options.timeout > 0) {
            if (!connection.timeout) {
                connection.timeout = new Timeout(() => {
                    var _a, _b, _c, _d;
                    (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onTimeout) === null || _b === void 0 ? void 0 : _b.call(_a, connection);
                    // XXX: Simulate a close event.
                    // @ts-ignore: Do not provide an `event` parameter to `onclose`.
                    (_d = (_c = connection.socket).onclose) === null || _d === void 0 ? void 0 : _d.call(_c);
                }, this.options.timeout);
            }
            else {
                connection.timeout.restart();
            }
        }
        if (this.options.keepalive && this.options.keepalive > 0) {
            if (!connection.keepalive) {
                connection.keepalive = new Timeout(() => {
                    this.message(connection, { type: PROTOCOL.ping });
                    connection.keepalive.timeout = random(0, this.options.keepalive);
                    connection.keepalive.restart();
                }, random(0, this.options.keepalive));
            }
            else {
                connection.keepalive.restart();
            }
        }
        if (!connection.meta) {
            connection.meta = {};
        }
        if (!connection.params) {
            connection.params = {};
        }
    }
    deinit(connection) {
        var _a, _b, _c;
        (_a = connection.keepalive) === null || _a === void 0 ? void 0 : _a.stop();
        (_b = connection.timeout) === null || _b === void 0 ? void 0 : _b.stop();
        this.close(connection);
        if (((_c = connection.state.value) === null || _c === void 0 ? void 0 : _c.status) !== STATE.disconnected) {
            connection.state.signal({ status: STATE.disconnected });
        }
    }
    bind(connection) {
        connection.socket.onmessage = this.onmessage.bind(this, connection);
        connection.socket.onerror = this.onerror.bind(this, connection);
        connection.socket.onclose = this.onclose.bind(this, connection);
        connection.socket.onopen = this.onopen.bind(this, connection);
    }
    unbind(connection) {
        connection.socket.onmessage = null;
        connection.socket.onerror = null;
        connection.socket.onclose = null;
        connection.socket.onopen = null;
    }
    attach(connection) {
        if (!connection.id || this.connections.has(connection.id)) {
            let id;
            do {
                id = uuidv4();
            } while (this.connections.has(id));
            connection.id = id;
        }
        this.init(connection);
        this.bind(connection);
        this.connections.set(connection.id, connection);
    }
    detach(connection) {
        this.unbind(connection);
        this.deinit(connection);
        this.connections.delete(connection.id);
    }
    handle(type, handler) {
        this.handlers.set(type, handler);
    }
    send(connection, message) {
        try {
            connection.socket.send(JSON.stringify(message));
        }
        catch (error) {
            console.log("[WebSocketManager send]: failed.", { error, message });
        }
    }
    sendOrThrow(connection, message) {
        connection.socket.send(JSON.stringify(message));
    }
    close(connection, code, reason) {
        return __awaiter(this, void 0, void 0, function* () {
            let promise = connection.state.promise();
            const { status } = yield promise;
            if (status === STATE.connected) {
                promise = connection.state.next();
                connection.socket.close(code, reason);
            }
            return promise;
        });
    }
    action(connection, message, observers = [], timeout = TIMEOUT) {
        var _a, _b;
        do {
            message.id = uuidv4();
        } while ((_a = connection.actions) === null || _a === void 0 ? void 0 : _a.has(message.id));
        const action = {
            message,
            observable: new Observable(observers),
            close: new Signaler(),
            status: "inactive",
        };
        if (timeout && timeout > 0) {
            action.timeout = new Timeout(() => {
                action.observable.error({
                    errors: ["Action timed out."],
                    action,
                });
                action.observable.complete();
            }, timeout);
        }
        (_b = action.timeout) === null || _b === void 0 ? void 0 : _b.stop();
        action.observable.push({
            error: (data) => {
                console.log("[WebSocketManager action]: error.", data);
            },
            complete: () => {
                this.remove(connection, action);
            },
        });
        this.add(connection, action);
        return action;
    }
    add(connection, action) {
        var _a;
        (_a = connection.actions) === null || _a === void 0 ? void 0 : _a.set(action.message.id, action);
    }
    remove(connection, action) {
        var _a, _b;
        (_a = action.timeout) === null || _a === void 0 ? void 0 : _a.stop();
        action.close.signal();
        (_b = connection.actions) === null || _b === void 0 ? void 0 : _b.delete(action.message.id);
    }
    dispatch(connection, action) {
        var _a, _b;
        if (action.status === "inactive") {
            action.status = "active";
            (_a = action.timeout) === null || _a === void 0 ? void 0 : _a.restart();
            try {
                this.sendOrThrow(connection, action.message);
            }
            catch (error) {
                action.status = "inactive";
                (_b = action.timeout) === null || _b === void 0 ? void 0 : _b.stop();
                console.log("[WebSocketManager dispatch]: failed.", { error, action });
            }
        }
    }
    message(connection, message, observers = [], timeout = TIMEOUT) {
        const action = this.action(connection, message, observers, timeout);
        connection.state.promise().then(({ status }) => {
            if (status === STATE.connected) {
                this.dispatch(connection, action);
            }
        });
        return action;
    }
    ping(connection, payload, observers = []) {
        return new Promise((resolve) => {
            let result;
            observers.push({
                next: (message) => {
                    result = message.data;
                },
                complete: () => {
                    resolve(result);
                },
            });
            this.message(connection, { type: PROTOCOL.ping, data: payload }, observers);
        });
    }
    meta(connection, payload = {}, observers = []) {
        return new Promise((resolve) => {
            let result;
            observers.push({
                next: (message) => {
                    result = message.meta;
                },
                complete: () => {
                    resolve(result);
                },
            });
            this.message(connection, { type: PROTOCOL.meta, meta: payload }, observers);
        });
    }
    stop() {
        return __awaiter(this, void 0, void 0, function* () {
            const promises = [];
            for (const connection of this.connections.values()) {
                const promise = this.close(connection).then((state) => {
                    this.detach(connection);
                    return state;
                });
                promises.push(promise);
            }
            yield Promise.all(promises);
        });
    }
    onopen(connection, _event) {
        var _a, _b, _c;
        return __awaiter(this, void 0, void 0, function* () {
            let error = false;
            const action = this.action(connection, {
                type: PROTOCOL.init,
                meta: yield ((_a = connection.metaFunction) === null || _a === void 0 ? void 0 : _a.call(connection, connection.meta)),
            }, [
                {
                    error: () => {
                        error = true;
                    },
                    complete: () => {
                        var _a, _b;
                        if (error) {
                            // Simulate `onclose`.
                            // @ts-ignore: don't pass a CloseEvent to `onclose`.
                            (_b = (_a = connection.socket).onclose) === null || _b === void 0 ? void 0 : _b.call(_a);
                        }
                        else {
                            connection.state.signal({ status: STATE.connected });
                        }
                    },
                },
            ]);
            this.dispatch(connection, action);
            (_c = (_b = this.options.callbacks) === null || _b === void 0 ? void 0 : _b.onOpen) === null || _c === void 0 ? void 0 : _c.call(_b, connection);
        });
    }
    onmessage(connection, event) {
        var _a, _b, _c;
        const message = JSON.parse(event.data);
        (_a = this.handlers.get(message.type)) === null || _a === void 0 ? void 0 : _a(connection, message);
        (_c = (_b = this.options.callbacks) === null || _b === void 0 ? void 0 : _b.onMessage) === null || _c === void 0 ? void 0 : _c.call(_b, connection, message);
    }
    onerror(connection, event) {
        var _a, _b;
        (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onError) === null || _b === void 0 ? void 0 : _b.call(_a, connection, event);
    }
    onclose(connection, _event) {
        var _a, _b;
        (_b = (_a = this.options.callbacks) === null || _a === void 0 ? void 0 : _a.onClose) === null || _b === void 0 ? void 0 : _b.call(_a, connection);
        this.detach(connection);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9tYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLE9BQU8sRUFNTCxTQUFTLEVBRVQsUUFBUSxFQUVSLEtBQUssRUFFTCxPQUFPLEdBQ1IsTUFBTSxVQUFVLENBQUM7QUFDbEIsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUN0QyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sV0FBVyxDQUFDO0FBQ3BDLE9BQU8sRUFBRSxVQUFVLEVBQVksTUFBTSxjQUFjLENBQUM7QUFDcEQsT0FBTyxFQUFFLE1BQU0sRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBK0J4RCxNQUFNLE9BQU8sZ0JBQWdCO0lBUTNCLFlBQVksT0FBaUM7UUFDM0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUMxQjtZQUNFLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLFNBQVMsRUFBRSxFQUFFO1NBQ2QsRUFDRCxPQUFPLENBQ1IsQ0FBQztRQUNGLElBQUksQ0FBQyxFQUFFLEdBQUcsTUFBTSxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLEdBQUcsRUFBc0IsQ0FBQztRQUNqRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksR0FBRyxFQUFtQixDQUFDO1FBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDLFVBQVUsRUFBRSxPQUFPLEVBQUUsRUFBRTs7WUFDakQsSUFBSTtnQkFDRixVQUFVLENBQUMsSUFBSSxHQUFHLGFBQWEsQ0FDN0IsVUFBVSxDQUFDLElBQUssRUFDaEIsT0FBTyxDQUFDLElBQWMsQ0FDWixDQUFDO2FBQ2Q7WUFBQyxPQUFPLEtBQUssRUFBRTtnQkFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLDRDQUE0QyxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUNqRSxPQUFPLFVBQVUsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZCLE9BQU8sT0FBTyxDQUFDLElBQUksQ0FBQztnQkFDcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDdkIsT0FBTzthQUNSO1lBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDZCxJQUFJLEVBQUUsUUFBUSxDQUFDLFFBQVE7Z0JBQ3ZCLE1BQU0sRUFBRTtvQkFDTixPQUFPLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0JBQ2hCLFVBQVUsRUFBRSxVQUFVLENBQUMsRUFBRTtpQkFDMUI7Z0JBQ0QsSUFBSSxFQUFFLFVBQVUsQ0FBQyxJQUFLO2FBQ3ZCLENBQUMsQ0FBQztZQUNILE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsTUFBTSxtREFBRyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLEVBQUU7O1lBQ3JELElBQUk7Z0JBQ0YsVUFBVSxDQUFDLElBQUksR0FBRyxhQUFhLENBQzdCLFVBQVUsQ0FBQyxJQUFLLEVBQ2hCLE9BQU8sQ0FBQyxJQUFjLENBQ1osQ0FBQzthQUNkO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnREFBZ0QsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDckUsT0FBTyxVQUFVLENBQUMsSUFBSSxDQUFDO2dCQUN2QixPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3ZCLE9BQU87YUFDUjtZQUNELE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsU0FBUyxtREFBRyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDekQsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQWdCLENBQUM7WUFDeEMsVUFBVSxDQUFDLE1BQU0scUJBQVEsTUFBTSxDQUFFLENBQUM7WUFDbEMsTUFBQSxNQUFBLFVBQVUsQ0FBQyxPQUFPLDBDQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBWSxDQUFDLDBDQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDeEUsTUFBQSxNQUFBLFVBQVUsQ0FBQyxPQUFPLDBDQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBWSxDQUFDLDBDQUFFLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUN2RSxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDLFVBQVUsRUFBRSxPQUFPLEVBQUUsRUFBRTs7WUFDakQsTUFBQSxVQUFVLENBQUMsT0FBTywwQ0FBRSxPQUFPLEVBQUUsQ0FBQztZQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO2dCQUNkLElBQUksRUFBRSxRQUFRLENBQUMsSUFBSTtnQkFDbkIsSUFBSSxFQUFFLE9BQU8sQ0FBQyxJQUFJO2FBQ25CLENBQUMsQ0FBQztZQUNILE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsTUFBTSxtREFBRyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLEVBQUU7O1lBQ2pELE1BQUEsVUFBVSxDQUFDLE9BQU8sMENBQUUsT0FBTyxFQUFFLENBQUM7WUFDOUIsTUFBQSxNQUFBLFVBQVUsQ0FBQyxPQUFPLDBDQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBWSxDQUFDLDBDQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDeEUsTUFBQSxNQUFBLFVBQVUsQ0FBQyxPQUFPLDBDQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBWSxDQUFDLDBDQUFFLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNyRSxNQUFBLE1BQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLDBDQUFFLE1BQU0sbURBQUcsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3hELENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxFQUFFOztZQUNqRCxJQUFJO2dCQUNGLFVBQVUsQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUM3QixVQUFVLENBQUMsSUFBSyxFQUNoQixPQUFPLENBQUMsSUFBYyxDQUNaLENBQUM7YUFDZDtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsNENBQTRDLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ2pFLE9BQU8sVUFBVSxDQUFDLElBQUksQ0FBQztnQkFDdkIsT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDO2dCQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN2QixPQUFPO2FBQ1I7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO2dCQUNkLElBQUksRUFBRSxRQUFRLENBQUMsUUFBUTtnQkFDdkIsSUFBSSxFQUFFLFVBQVUsQ0FBQyxJQUFLO2FBQ3ZCLENBQUMsQ0FBQztZQUNILE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsTUFBTSxtREFBRyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLEVBQUU7O1lBQ3JELElBQUk7Z0JBQ0YsVUFBVSxDQUFDLElBQUksR0FBRyxhQUFhLENBQzdCLFVBQVUsQ0FBQyxJQUFLLEVBQ2hCLE9BQU8sQ0FBQyxJQUFjLENBQ1osQ0FBQzthQUNkO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnREFBZ0QsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDckUsT0FBTyxVQUFVLENBQUMsSUFBSSxDQUFDO2dCQUN2QixPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3ZCLE9BQU87YUFDUjtZQUNELE1BQUEsTUFBQSxVQUFVLENBQUMsT0FBTywwQ0FBRSxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FBRSxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3hFLE1BQUEsTUFBQSxVQUFVLENBQUMsT0FBTywwQ0FBRSxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FBRSxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDckUsTUFBQSxNQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUywwQ0FBRSxTQUFTLG1EQUFHLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUMzRCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxJQUFJLENBQUMsVUFBc0I7UUFDaEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUU7WUFDdkIsVUFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLEdBQUcsRUFBMkIsQ0FBQztTQUN6RDtRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFO1lBQ3JCLFVBQVUsQ0FBQyxLQUFLLEdBQUcsSUFBSSxRQUFRLEVBQVMsQ0FBQztZQUN6QyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRTs7Z0JBQ2pDLElBQUksTUFBTSxLQUFLLEtBQUssQ0FBQyxTQUFTLEVBQUU7b0JBQzlCLEtBQUssTUFBTSxNQUFNLElBQUksVUFBVSxDQUFDLE9BQVEsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDakQsSUFDRSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUMxQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQWdCLENBQ2hDLEVBQ0Q7NEJBQ0EsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7eUJBQ25DO3FCQUNGO29CQUNELE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsV0FBVyxtREFBRyxVQUFVLENBQUMsQ0FBQztpQkFDbkQ7cUJBQU0sSUFBSSxNQUFNLEtBQUssS0FBSyxDQUFDLFlBQVksRUFBRTtvQkFDeEMsS0FBSyxNQUFNLE1BQU0sSUFBSSxVQUFVLENBQUMsT0FBUSxDQUFDLE1BQU0sRUFBRSxFQUFFO3dCQUNqRCxNQUFBLE1BQU0sQ0FBQyxPQUFPLDBDQUFFLElBQUksRUFBRSxDQUFDO3dCQUN2QixNQUFNLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztxQkFDNUI7b0JBQ0QsTUFBQSxNQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUywwQ0FBRSxjQUFjLG1EQUFHLFVBQVUsQ0FBQyxDQUFDO2lCQUN0RDtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDMUI7UUFDRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxHQUFHLENBQUMsRUFBRTtZQUNwRCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRTtnQkFDdkIsVUFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLE9BQU8sQ0FBQyxHQUFHLEVBQUU7O29CQUNwQyxNQUFBLE1BQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLDBDQUFFLFNBQVMsbURBQUcsVUFBVSxDQUFDLENBQUM7b0JBQ2hELCtCQUErQjtvQkFDL0IsZ0VBQWdFO29CQUNoRSxNQUFBLE1BQUEsVUFBVSxDQUFDLE1BQU0sRUFBQyxPQUFPLGtEQUFJLENBQUM7Z0JBQ2hDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQVEsQ0FBQyxDQUFDO2FBQzNCO2lCQUFNO2dCQUNMLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDOUI7U0FDRjtRQUNELElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsQ0FBQyxFQUFFO1lBQ3hELElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFO2dCQUN6QixVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksT0FBTyxDQUFDLEdBQUcsRUFBRTtvQkFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7b0JBQ2xELFVBQVUsQ0FBQyxTQUFVLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFVLENBQUMsQ0FBQztvQkFDbkUsVUFBVSxDQUFDLFNBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDbEMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFVLENBQUMsQ0FBQyxDQUFDO2FBQ3hDO2lCQUFNO2dCQUNMLFVBQVUsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDaEM7U0FDRjtRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFO1lBQ3BCLFVBQVUsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUU7WUFDdEIsVUFBVSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7U0FDeEI7SUFDSCxDQUFDO0lBRU0sTUFBTSxDQUFDLFVBQXNCOztRQUNsQyxNQUFBLFVBQVUsQ0FBQyxTQUFTLDBDQUFFLElBQUksRUFBRSxDQUFDO1FBQzdCLE1BQUEsVUFBVSxDQUFDLE9BQU8sMENBQUUsSUFBSSxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUEsTUFBQSxVQUFVLENBQUMsS0FBTSxDQUFDLEtBQUssMENBQUUsTUFBTSxNQUFLLEtBQUssQ0FBQyxZQUFZLEVBQUU7WUFDMUQsVUFBVSxDQUFDLEtBQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7U0FDMUQ7SUFDSCxDQUFDO0lBRU0sSUFBSSxDQUFDLFVBQXNCO1FBQ2hDLFVBQVUsQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQztRQUNwRSxVQUFVLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDaEUsVUFBVSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ2hFLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBRU0sTUFBTSxDQUFDLFVBQXNCO1FBQ2xDLFVBQVUsQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUNuQyxVQUFVLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDakMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUNsQyxDQUFDO0lBRU0sTUFBTSxDQUFDLFVBQXNCO1FBQ2xDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUN6RCxJQUFJLEVBQVUsQ0FBQztZQUNmLEdBQUc7Z0JBQ0QsRUFBRSxHQUFHLE1BQU0sRUFBRSxDQUFDO2FBQ2YsUUFBUSxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUNuQyxVQUFVLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztTQUNwQjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFTSxNQUFNLENBQUMsVUFBc0I7UUFDbEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxFQUFHLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRU0sTUFBTSxDQUFDLElBQVksRUFBRSxPQUFnQjtRQUMxQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVNLElBQUksQ0FBQyxVQUFzQixFQUFFLE9BQWdCO1FBQ2xELElBQUk7WUFDRixVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7U0FDakQ7UUFBQyxPQUFPLEtBQUssRUFBRTtZQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0NBQWtDLEVBQUUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztTQUNyRTtJQUNILENBQUM7SUFFTSxXQUFXLENBQUMsVUFBc0IsRUFBRSxPQUFnQjtRQUN6RCxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVZLEtBQUssQ0FDaEIsVUFBc0IsRUFDdEIsSUFBYSxFQUNiLE1BQWU7O1lBRWYsSUFBSSxPQUFPLEdBQUcsVUFBVSxDQUFDLEtBQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUMxQyxNQUFNLEVBQUUsTUFBTSxFQUFFLEdBQUcsTUFBTSxPQUFPLENBQUM7WUFDakMsSUFBSSxNQUFNLEtBQUssS0FBSyxDQUFDLFNBQVMsRUFBRTtnQkFDOUIsT0FBTyxHQUFHLFVBQVUsQ0FBQyxLQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ25DLFVBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQzthQUN2QztZQUNELE9BQU8sT0FBTyxDQUFDO1FBQ2pCLENBQUM7S0FBQTtJQUVNLE1BQU0sQ0FDWCxVQUFzQixFQUN0QixPQUFnQixFQUNoQixZQUFpQyxFQUFFLEVBQ25DLE9BQU8sR0FBRyxPQUFPOztRQUVqQixHQUFHO1lBQ0QsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLEVBQUUsQ0FBQztTQUN2QixRQUFRLE1BQUEsVUFBVSxDQUFDLE9BQU8sMENBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxFQUFZLENBQUMsRUFBRTtRQUN4RCxNQUFNLE1BQU0sR0FBb0I7WUFDOUIsT0FBTztZQUNQLFVBQVUsRUFBRSxJQUFJLFVBQVUsQ0FBVSxTQUFTLENBQUM7WUFDOUMsS0FBSyxFQUFFLElBQUksUUFBUSxFQUFRO1lBQzNCLE1BQU0sRUFBRSxVQUFVO1NBQ25CLENBQUM7UUFDRixJQUFJLE9BQU8sSUFBSSxPQUFPLEdBQUcsQ0FBQyxFQUFFO1lBQzFCLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQUMsR0FBRyxFQUFFO2dCQUNoQyxNQUFNLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQztvQkFDdEIsTUFBTSxFQUFFLENBQUMsbUJBQW1CLENBQUM7b0JBQzdCLE1BQU07aUJBQ1AsQ0FBQyxDQUFDO2dCQUNILE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDL0IsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1NBQ2I7UUFDRCxNQUFBLE1BQU0sQ0FBQyxPQUFPLDBDQUFFLElBQUksRUFBRSxDQUFDO1FBQ3ZCLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ3JCLEtBQUssRUFBRSxDQUFDLElBQUksRUFBRSxFQUFFO2dCQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUNBQW1DLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDekQsQ0FBQztZQUNELFFBQVEsRUFBRSxHQUFHLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDbEMsQ0FBQztTQUNGLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzdCLE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxHQUFHLENBQUMsVUFBc0IsRUFBRSxNQUF1Qjs7UUFDeEQsTUFBQSxVQUFVLENBQUMsT0FBTywwQ0FBRSxHQUFHLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFZLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVNLE1BQU0sQ0FBQyxVQUFzQixFQUFFLE1BQXVCOztRQUMzRCxNQUFBLE1BQU0sQ0FBQyxPQUFPLDBDQUFFLElBQUksRUFBRSxDQUFDO1FBQ3ZCLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDdEIsTUFBQSxVQUFVLENBQUMsT0FBTywwQ0FBRSxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFZLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRU0sUUFBUSxDQUFDLFVBQXNCLEVBQUUsTUFBdUI7O1FBQzdELElBQUksTUFBTSxDQUFDLE1BQU0sS0FBSyxVQUFVLEVBQUU7WUFDaEMsTUFBTSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUM7WUFDekIsTUFBQSxNQUFNLENBQUMsT0FBTywwQ0FBRSxPQUFPLEVBQUUsQ0FBQztZQUMxQixJQUFJO2dCQUNGLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUM5QztZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLE1BQU0sQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDO2dCQUMzQixNQUFBLE1BQU0sQ0FBQyxPQUFPLDBDQUFFLElBQUksRUFBRSxDQUFDO2dCQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLHNDQUFzQyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7YUFDeEU7U0FDRjtJQUNILENBQUM7SUFFTSxPQUFPLENBQ1osVUFBc0IsRUFDdEIsT0FBZ0IsRUFDaEIsWUFBaUMsRUFBRSxFQUNuQyxPQUFPLEdBQUcsT0FBTztRQUVqQixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3BFLFVBQVUsQ0FBQyxLQUFNLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFO1lBQzlDLElBQUksTUFBTSxLQUFLLEtBQUssQ0FBQyxTQUFTLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ25DO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBRU0sSUFBSSxDQUNULFVBQXNCLEVBQ3RCLE9BQWdCLEVBQ2hCLFlBQWlDLEVBQUU7UUFFbkMsT0FBTyxJQUFJLE9BQU8sQ0FBVSxDQUFDLE9BQU8sRUFBRSxFQUFFO1lBQ3RDLElBQUksTUFBZSxDQUFDO1lBQ3BCLFNBQVMsQ0FBQyxJQUFJLENBQUM7Z0JBQ2IsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEVBQUU7b0JBQ2hCLE1BQU0sR0FBRyxPQUFPLENBQUMsSUFBZSxDQUFDO2dCQUNuQyxDQUFDO2dCQUNELFFBQVEsRUFBRSxHQUFHLEVBQUU7b0JBQ2IsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNsQixDQUFDO2FBQ0YsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FDVixVQUFVLEVBQ1YsRUFBRSxJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQ3RDLFNBQVMsQ0FDVixDQUFDO1FBQ0osQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sSUFBSSxDQUNULFVBQXNCLEVBQ3RCLFVBQW1CLEVBQUUsRUFDckIsWUFBaUMsRUFBRTtRQUVuQyxPQUFPLElBQUksT0FBTyxDQUFVLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDdEMsSUFBSSxNQUFlLENBQUM7WUFDcEIsU0FBUyxDQUFDLElBQUksQ0FBQztnQkFDYixJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRTtvQkFDaEIsTUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFlLENBQUM7Z0JBQ25DLENBQUM7Z0JBQ0QsUUFBUSxFQUFFLEdBQUcsRUFBRTtvQkFDYixPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ2xCLENBQUM7YUFDRixDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsT0FBTyxDQUNWLFVBQVUsRUFDVixFQUFFLElBQUksRUFBRSxRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsRUFDdEMsU0FBUyxDQUNWLENBQUM7UUFDSixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFWSxJQUFJOztZQUNmLE1BQU0sUUFBUSxHQUFxQixFQUFFLENBQUM7WUFDdEMsS0FBSyxNQUFNLFVBQVUsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxFQUFFO2dCQUNsRCxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO29CQUNwRCxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUN4QixPQUFPLEtBQUssQ0FBQztnQkFDZixDQUFDLENBQUMsQ0FBQztnQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ3hCO1lBQ0QsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzlCLENBQUM7S0FBQTtJQUVlLE1BQU0sQ0FBQyxVQUFzQixFQUFFLE1BQWE7OztZQUMxRCxJQUFJLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDbEIsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FDeEIsVUFBVSxFQUNWO2dCQUNFLElBQUksRUFBRSxRQUFRLENBQUMsSUFBSTtnQkFDbkIsSUFBSSxFQUFFLE1BQU0sQ0FBQSxNQUFBLFVBQVUsQ0FBQyxZQUFZLDJEQUFHLFVBQVUsQ0FBQyxJQUFLLENBQUMsQ0FBQTthQUN4RCxFQUNEO2dCQUNFO29CQUNFLEtBQUssRUFBRSxHQUFHLEVBQUU7d0JBQ1YsS0FBSyxHQUFHLElBQUksQ0FBQztvQkFDZixDQUFDO29CQUNELFFBQVEsRUFBRSxHQUFHLEVBQUU7O3dCQUNiLElBQUksS0FBSyxFQUFFOzRCQUNULHNCQUFzQjs0QkFDdEIsb0RBQW9EOzRCQUNwRCxNQUFBLE1BQUEsVUFBVSxDQUFDLE1BQU0sRUFBQyxPQUFPLGtEQUFJLENBQUM7eUJBQy9COzZCQUFNOzRCQUNMLFVBQVUsQ0FBQyxLQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO3lCQUN2RDtvQkFDSCxDQUFDO2lCQUNGO2FBQ0YsQ0FDRixDQUFDO1lBQ0YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDbEMsTUFBQSxNQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUywwQ0FBRSxNQUFNLG1EQUFHLFVBQVUsQ0FBQyxDQUFDOztLQUM5QztJQUVTLFNBQVMsQ0FBQyxVQUFzQixFQUFFLEtBQW1COztRQUM3RCxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QyxNQUFBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsMENBQUcsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3ZELE1BQUEsTUFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsMENBQUUsU0FBUyxtREFBRyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVTLE9BQU8sQ0FBQyxVQUFzQixFQUFFLEtBQXlCOztRQUNqRSxNQUFBLE1BQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLDBDQUFFLE9BQU8sbURBQUcsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFUyxPQUFPLENBQUMsVUFBc0IsRUFBRSxNQUFrQjs7UUFDMUQsTUFBQSxNQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUywwQ0FBRSxPQUFPLG1EQUFHLFVBQVUsQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDMUIsQ0FBQztDQUNGIn0=