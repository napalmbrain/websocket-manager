/**
 * Observable class.
 */
export class Observable {
    constructor(observers = []) {
        this.observers = observers;
    }
    /**
     * Call the next function on all observers, if it exists.
     * @param data Data to pass to observers.
     */
    next(data) {
        var _a;
        for (const observer of this.observers) {
            (_a = observer.next) === null || _a === void 0 ? void 0 : _a.call(observer, data);
        }
    }
    /**
     * Call the error function on all observers, if it exists.
     * @param data Data to pass to observers.
     */
    error(data) {
        var _a;
        for (const observer of this.observers) {
            (_a = observer.error) === null || _a === void 0 ? void 0 : _a.call(observer, data);
        }
    }
    /**
     * Call the complete function on all observers, if it exists.
     */
    complete() {
        var _a;
        for (const observer of this.observers) {
            (_a = observer.complete) === null || _a === void 0 ? void 0 : _a.call(observer);
        }
        this.observers = [];
    }
    /**
     * Push an observer to the array of observers.
     * @param observer The observer to add.
     */
    push(observer) {
        this.observers.push(observer);
    }
    /**
     * Unshift an observer to the array of observers.
     * @param observer The observer to add.
     */
    unshift(observer) {
        this.observers.unshift(observer);
    }
    /**
     * Remove an observer from the array of observers.
     * @param observer The observer to remove.
     */
    unobserve(observer) {
        this.observers = this.observers.filter((o) => observer !== o);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JzZXJ2YWJsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9vYnNlcnZhYmxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQU1BOztHQUVHO0FBQ0gsTUFBTSxPQUFPLFVBQVU7SUFNckIsWUFBWSxZQUEyQixFQUFFO1FBQ3ZDLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO0lBQzdCLENBQUM7SUFFRDs7O09BR0c7SUFDSSxJQUFJLENBQUMsSUFBTzs7UUFDakIsS0FBSyxNQUFNLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3JDLE1BQUEsUUFBUSxDQUFDLElBQUkseURBQUcsSUFBSSxDQUFDLENBQUM7U0FDdkI7SUFDSCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksS0FBSyxDQUFDLElBQU87O1FBQ2xCLEtBQUssTUFBTSxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNyQyxNQUFBLFFBQVEsQ0FBQyxLQUFLLHlEQUFHLElBQUksQ0FBQyxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQztJQUVEOztPQUVHO0lBQ0ksUUFBUTs7UUFDYixLQUFLLE1BQU0sUUFBUSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDckMsTUFBQSxRQUFRLENBQUMsUUFBUSx3REFBSSxDQUFDO1NBQ3ZCO1FBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVEOzs7T0FHRztJQUNJLElBQUksQ0FBQyxRQUFxQjtRQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksT0FBTyxDQUFDLFFBQXFCO1FBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRDs7O09BR0c7SUFDSSxTQUFTLENBQUMsUUFBcUI7UUFDcEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsUUFBUSxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Q0FDRiJ9