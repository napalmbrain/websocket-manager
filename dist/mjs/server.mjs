var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { KEEPALIVE, STATE } from "./common.mjs";
import { WebSocketManager } from "./manager.mjs";
export class WebSocketServer extends WebSocketManager {
    constructor(options = {}) {
        super(Object.assign({ keepalive: KEEPALIVE }, options));
    }
    deinit(connection) {
        var _a, _b;
        if (((_b = (_a = connection.state) === null || _a === void 0 ? void 0 : _a.value) === null || _b === void 0 ? void 0 : _b.status) === STATE.connected) {
            for (const action of connection.actions.values()) {
                action.observable.complete();
            }
            this.close(connection);
            super.deinit(connection);
        }
    }
    stop() {
        const _super = Object.create(null, {
            stop: { get: () => super.stop }
        });
        return __awaiter(this, void 0, void 0, function* () {
            yield _super.stop.call(this);
            for (const connection of this.connections.values()) {
                for (const action of connection.actions.values()) {
                    this.remove(connection, action);
                }
            }
        });
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL3NlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQSxPQUFPLEVBQWMsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLFVBQVUsQ0FBQztBQUN4RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQTJCLE1BQU0sV0FBVyxDQUFDO0FBSXRFLE1BQU0sT0FBTyxlQUFnQixTQUFRLGdCQUFnQjtJQUduRCxZQUFZLFVBQWtDLEVBQUU7UUFDOUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRU0sTUFBTSxDQUFDLFVBQXNCOztRQUNsQyxJQUFJLENBQUEsTUFBQSxNQUFBLFVBQVUsQ0FBQyxLQUFLLDBDQUFFLEtBQUssMENBQUUsTUFBTSxNQUFLLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDdkQsS0FBSyxNQUFNLE1BQU0sSUFBSSxVQUFVLENBQUMsT0FBUSxDQUFDLE1BQU0sRUFBRSxFQUFFO2dCQUNqRCxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQzlCO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN2QixLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQzFCO0lBQ0gsQ0FBQztJQUVZLElBQUk7Ozs7O1lBQ2YsTUFBTSxPQUFNLElBQUksV0FBRSxDQUFDO1lBQ25CLEtBQUssTUFBTSxVQUFVLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsRUFBRTtnQkFDbEQsS0FBSyxNQUFNLE1BQU0sSUFBSSxVQUFVLENBQUMsT0FBUSxDQUFDLE1BQU0sRUFBRSxFQUFFO29CQUNqRCxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztpQkFDakM7YUFDRjtRQUNILENBQUM7S0FBQTtDQUNGIn0=