export function isDeno() {
    if (window && "Deno" in window) {
        return true;
    }
    return false;
}
/**
 * Determines if the environment is the browser.
 * @returns true if the environment is the browser, else false.
 */
export function isBrowser() {
    if (window && !isDeno()) {
        return true;
    }
    return false;
}
export function isFunction(object) {
    if (typeof object === "function") {
        return true;
    }
    return false;
}
export function sleep(time, options = {}) {
    return new Promise((resolve, reject) => {
        var _a;
        let timeout;
        const onAbort = () => {
            var _a;
            (_a = options.signal) === null || _a === void 0 ? void 0 : _a.removeEventListener("abort", onAbort);
            // @ts-ignore: Clear timeout on type `unknown` for compatibility with Deno, Bun and Node.
            clearTimeout(timeout);
            reject();
        };
        (_a = options.signal) === null || _a === void 0 ? void 0 : _a.addEventListener("abort", onAbort);
        timeout = setTimeout(() => {
            var _a;
            (_a = options.signal) === null || _a === void 0 ? void 0 : _a.removeEventListener("abort", onAbort);
            resolve();
        }, time);
    });
}
export function keepalive(keepalive) {
    return (keepalive - keepalive / 2 + Math.floor((keepalive / 2) * Math.random()));
}
export function random(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}
export function limitedAssign(a, b, limit = 4096) {
    const alpha = JSON.stringify(a || {});
    const beta = JSON.stringify(b || {});
    const length = alpha.length + beta.length;
    if (length > limit) {
        throw new Error("Object size limit.");
    }
    Object.assign(a, b);
    return a;
}
export function uuidv4() {
    return "10000000-1000-4000-8000-100000000000".replace(/[018]/g, (c) => (c ^
        (crypto.getRandomValues(new Uint8Array(1))[0] &
            (15 >> (c / 4)))).toString(16));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxVQUFVLE1BQU07SUFDcEIsSUFBSSxNQUFNLElBQUksTUFBTSxJQUFJLE1BQU0sRUFBRTtRQUM5QixPQUFPLElBQUksQ0FBQztLQUNiO0lBQ0QsT0FBTyxLQUFLLENBQUM7QUFDZixDQUFDO0FBRUQ7OztHQUdHO0FBQ0gsTUFBTSxVQUFVLFNBQVM7SUFDdkIsSUFBSSxNQUFNLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRTtRQUN2QixPQUFPLElBQUksQ0FBQztLQUNiO0lBQ0QsT0FBTyxLQUFLLENBQUM7QUFDZixDQUFDO0FBRUQsTUFBTSxVQUFVLFVBQVUsQ0FBQyxNQUFjO0lBQ3ZDLElBQUksT0FBTyxNQUFNLEtBQUssVUFBVSxFQUFFO1FBQ2hDLE9BQU8sSUFBSSxDQUFDO0tBQ2I7SUFDRCxPQUFPLEtBQUssQ0FBQztBQUNmLENBQUM7QUFFRCxNQUFNLFVBQVUsS0FBSyxDQUFDLElBQVksRUFBRSxVQUFvQyxFQUFFO0lBQ3hFLE9BQU8sSUFBSSxPQUFPLENBQU8sQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7O1FBQzNDLElBQUksT0FBZ0IsQ0FBQztRQUNyQixNQUFNLE9BQU8sR0FBRyxHQUFHLEVBQUU7O1lBQ25CLE1BQUEsT0FBTyxDQUFDLE1BQU0sMENBQUUsbUJBQW1CLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3RELHlGQUF5RjtZQUN6RixZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdEIsTUFBTSxFQUFFLENBQUM7UUFDWCxDQUFDLENBQUM7UUFDRixNQUFBLE9BQU8sQ0FBQyxNQUFNLDBDQUFFLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNuRCxPQUFPLEdBQUcsVUFBVSxDQUFDLEdBQUcsRUFBRTs7WUFDeEIsTUFBQSxPQUFPLENBQUMsTUFBTSwwQ0FBRSxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDdEQsT0FBTyxFQUFFLENBQUM7UUFDWixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDWCxDQUFDLENBQUMsQ0FBQztBQUNMLENBQUM7QUFFRCxNQUFNLFVBQVUsU0FBUyxDQUFDLFNBQWlCO0lBQ3pDLE9BQU8sQ0FDTCxTQUFTLEdBQUcsU0FBUyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUN4RSxDQUFDO0FBQ0osQ0FBQztBQUVELE1BQU0sVUFBVSxNQUFNLENBQUMsR0FBVyxFQUFFLEdBQVc7SUFDN0MsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQztBQUN2RCxDQUFDO0FBRUQsTUFBTSxVQUFVLGFBQWEsQ0FBQyxDQUFTLEVBQUUsQ0FBUyxFQUFFLEtBQUssR0FBRyxJQUFJO0lBQzlELE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ3RDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ3JDLE1BQU0sTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUMxQyxJQUFJLE1BQU0sR0FBRyxLQUFLLEVBQUU7UUFDbEIsTUFBTSxJQUFJLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO0tBQ3ZDO0lBQ0QsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDcEIsT0FBTyxDQUFDLENBQUM7QUFDWCxDQUFDO0FBRUQsTUFBTSxVQUFVLE1BQU07SUFDcEIsT0FBTyxzQ0FBc0MsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FDcEUsQ0FDRyxDQUF1QjtRQUN4QixDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0MsQ0FBQyxFQUFFLElBQUksQ0FBRSxDQUF1QixHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FDMUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQ2YsQ0FBQztBQUNKLENBQUMifQ==