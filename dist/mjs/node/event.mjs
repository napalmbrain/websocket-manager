export class MessageEvent extends Event {
    constructor(type, eventInitDict) {
        super(type, eventInitDict);
        this.data = eventInitDict === null || eventInitDict === void 0 ? void 0 : eventInitDict.data;
    }
}
export class ErrorEvent extends Event {
    constructor(type, eventInitDict) {
        super(type, eventInitDict);
        this.message = eventInitDict === null || eventInitDict === void 0 ? void 0 : eventInitDict.message;
    }
}
export class CloseEvent extends Event {
    constructor(type, eventInitDict) {
        super(type, eventInitDict);
        this.code = eventInitDict === null || eventInitDict === void 0 ? void 0 : eventInitDict.code;
        this.reason = eventInitDict === null || eventInitDict === void 0 ? void 0 : eventInitDict.reason;
        this.wasClean = eventInitDict === null || eventInitDict === void 0 ? void 0 : eventInitDict.wasClean;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvbm9kZS9ldmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLE9BQU8sWUFBYSxTQUFRLEtBQUs7SUFHckMsWUFBWSxJQUFZLEVBQUUsYUFBdUM7UUFDL0QsS0FBSyxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxJQUFjLENBQUM7SUFDNUMsQ0FBQztDQUNGO0FBRUQsTUFBTSxPQUFPLFVBQVcsU0FBUSxLQUFLO0lBR25DLFlBQVksSUFBWSxFQUFFLGFBQXVDO1FBQy9ELEtBQUssQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsT0FBaUIsQ0FBQztJQUNsRCxDQUFDO0NBQ0Y7QUFFRCxNQUFNLE9BQU8sVUFBVyxTQUFRLEtBQUs7SUFLbkMsWUFBWSxJQUFZLEVBQUUsYUFBdUM7UUFDL0QsS0FBSyxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxJQUFjLENBQUM7UUFDMUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsTUFBZ0IsQ0FBQztRQUM5QyxJQUFJLENBQUMsUUFBUSxHQUFHLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxRQUFtQixDQUFDO0lBQ3JELENBQUM7Q0FDRiJ9