var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { WebSocketServer as WSWebSocketServer, } from "ws";
import { WebSocket } from "./websocket.mjs";
import { WebSocketServer } from "../../server.mjs";
export class NodeWebSocketServer extends WebSocketServer {
    constructor(options = {}) {
        super(options);
        this.onconnection = this.connection.bind(this);
    }
    connection(ws) {
        var _a, _b;
        const socket = new WebSocket(ws);
        const connection = {
            // @ts-ignore: WebSocket is not fully implemented for nodejs.
            socket,
        };
        this.attach(connection);
        // XXX: Simulate `onopen` for server websockets.
        const event = new Event("open");
        (_b = (_a = connection.socket).onopen) === null || _b === void 0 ? void 0 : _b.call(_a, event);
    }
    listen(options = { port: 8000 }) {
        this.server = new WSWebSocketServer(Object.assign({}, options));
        this.server.on("connection", this.onconnection);
    }
    stop() {
        const _super = Object.create(null, {
            stop: { get: () => super.stop }
        });
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            (_a = this.server) === null || _a === void 0 ? void 0 : _a.off("connection", this.onconnection);
            yield _super.stop.call(this);
            yield new Promise((resolve, reject) => {
                var _a;
                (_a = this.server) === null || _a === void 0 ? void 0 : _a.close((error) => {
                    if (error) {
                        console.log("[NodeWebSocketServer stop]: server stop error.", error);
                        reject(error);
                    }
                    else {
                        resolve();
                    }
                });
            });
        });
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvd3Mvc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLE9BQU8sRUFDTCxlQUFlLElBQUksaUJBQWlCLEdBR3JDLE1BQU0sSUFBSSxDQUFDO0FBRVosT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUN4QyxPQUFPLEVBQUUsZUFBZSxFQUEwQixNQUFNLGNBQWMsQ0FBQztBQUd2RSxNQUFNLE9BQU8sbUJBQW9CLFNBQVEsZUFBZTtJQUl0RCxZQUFZLFVBQWtDLEVBQUU7UUFDOUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2YsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRU8sVUFBVSxDQUFDLEVBQWU7O1FBQ2hDLE1BQU0sTUFBTSxHQUFHLElBQUksU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2pDLE1BQU0sVUFBVSxHQUFlO1lBQzdCLDZEQUE2RDtZQUM3RCxNQUFNO1NBQ1AsQ0FBQztRQUNGLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDeEIsZ0RBQWdEO1FBQ2hELE1BQU0sS0FBSyxHQUFHLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2hDLE1BQUEsTUFBQSxVQUFVLENBQUMsTUFBTSxFQUFDLE1BQU0sbURBQUcsS0FBSyxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVNLE1BQU0sQ0FBQyxVQUEyQixFQUFFLElBQUksRUFBRSxJQUFJLEVBQUU7UUFDckQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLGlCQUFpQixtQkFBTSxPQUFPLEVBQUcsQ0FBQztRQUNwRCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFWSxJQUFJOzs7Ozs7WUFDZixNQUFBLElBQUksQ0FBQyxNQUFNLDBDQUFFLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xELE1BQU0sT0FBTSxJQUFJLFdBQUUsQ0FBQztZQUNuQixNQUFNLElBQUksT0FBTyxDQUFPLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFOztnQkFDMUMsTUFBQSxJQUFJLENBQUMsTUFBTSwwQ0FBRSxLQUFLLENBQUMsQ0FBQyxLQUFhLEVBQUUsRUFBRTtvQkFDbkMsSUFBSSxLQUFLLEVBQUU7d0JBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnREFBZ0QsRUFBRSxLQUFLLENBQUMsQ0FBQzt3QkFDckUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUNmO3lCQUFNO3dCQUNMLE9BQU8sRUFBRSxDQUFDO3FCQUNYO2dCQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7O0tBQ0o7Q0FDRiJ9