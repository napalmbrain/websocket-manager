var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { WebSocket as WSWebSocket } from "ws";
import { WebSocket } from "./websocket.mjs";
import { WebSocketClient } from "../../client.mjs";
export class NodeWebSocketClient extends WebSocketClient {
    // @ts-ignore: Ignore type conflict with parent class.
    connect(address, meta, protocols, options) {
        const ws = new WSWebSocket(address, protocols, options);
        const socket = new WebSocket(ws);
        const connection = {
            metaFunction: meta,
            // @ts-ignore: WebSocket is not fully implemented for nodejs.
            socket,
            params: {
                address,
                protocols,
                options,
            },
        };
        this.attach(connection);
        return connection;
    }
    reconnect(connection) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.close(connection);
            this.detach(connection);
            const ws = new WSWebSocket(connection.params.address, connection.params.protocols, connection.params.options);
            // @ts-ignore: Ignore type conflict.
            connection.socket = new WebSocket(ws);
            this.attach(connection);
        });
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvd3MvY2xpZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLElBQUksV0FBVyxFQUFvQyxNQUFNLElBQUksQ0FBQztBQUVoRixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQ3hDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFHL0MsTUFBTSxPQUFPLG1CQUFvQixTQUFRLGVBQWU7SUFDdEQsc0RBQXNEO0lBQy9DLE9BQU8sQ0FDWixPQUFxQixFQUNyQixJQUFtQixFQUNuQixTQUFvQixFQUNwQixPQUF5QjtRQUV6QixNQUFNLEVBQUUsR0FBRyxJQUFJLFdBQVcsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3hELE1BQU0sTUFBTSxHQUFHLElBQUksU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2pDLE1BQU0sVUFBVSxHQUFlO1lBQzdCLFlBQVksRUFBRSxJQUFJO1lBQ2xCLDZEQUE2RDtZQUM3RCxNQUFNO1lBQ04sTUFBTSxFQUFFO2dCQUNOLE9BQU87Z0JBQ1AsU0FBUztnQkFDVCxPQUFPO2FBQ1I7U0FDRixDQUFDO1FBQ0YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN4QixPQUFPLFVBQVUsQ0FBQztJQUNwQixDQUFDO0lBRVksU0FBUyxDQUFDLFVBQXNCOztZQUMzQyxNQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN4QixNQUFNLEVBQUUsR0FBRyxJQUFJLFdBQVcsQ0FDeEIsVUFBVSxDQUFDLE1BQU8sQ0FBQyxPQUF1QixFQUMxQyxVQUFVLENBQUMsTUFBTyxDQUFDLFNBQXFCLEVBQ3hDLFVBQVUsQ0FBQyxNQUFPLENBQUMsT0FBa0IsQ0FDdEMsQ0FBQztZQUNGLG9DQUFvQztZQUNwQyxVQUFVLENBQUMsTUFBTSxHQUFHLElBQUksU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDMUIsQ0FBQztLQUFBO0NBQ0YifQ==