import { WebSocketServer } from "../../server.mjs";
import { MessageEvent, CloseEvent } from "../event.mjs";
import { WebSocket } from "./websocket.mjs";
export class NodeMicroWebSocketServer extends WebSocketServer {
    constructor(options = {}) {
        super(options);
        this.decoder = new TextDecoder();
    }
    listen(options) {
        options = Object.assign({ pattern: "/*", behavior: {} }, options);
        options.app.ws(options.pattern, Object.assign(Object.assign({}, options.behavior), { upgrade: (response, request, context) => {
                const connection = {};
                response.upgrade({ connection }, request.getHeader("sec-websocket-key"), request.getHeader("sec-websocket-protocol"), request.getHeader("sec-websocket-extensions"), context);
            }, open: (ws) => {
                var _a, _b;
                const data = ws.getUserData();
                // @ts-ignore: Ignore missing attributes.
                data.connection.socket = new WebSocket(ws);
                this.attach(data.connection);
                const event = new Event("open");
                (_b = (_a = data.connection.socket).onopen) === null || _b === void 0 ? void 0 : _b.call(_a, event);
            }, message: (ws, message) => {
                var _a, _b;
                const data = ws.getUserData();
                const event = new MessageEvent("message", {
                    data: this.decoder.decode(message),
                });
                // @ts-ignore: Ignore missing attributes.
                (_b = (_a = data.connection.socket).onmessage) === null || _b === void 0 ? void 0 : _b.call(_a, event);
            }, close: (ws) => {
                var _a, _b;
                const data = ws.getUserData();
                const event = new CloseEvent("close");
                (_b = (_a = data.connection.socket).onclose) === null || _b === void 0 ? void 0 : _b.call(_a, event);
            } }));
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvdXdlYnNvY2tldHMvc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE9BQU8sRUFBRSxlQUFlLEVBQTBCLE1BQU0sY0FBYyxDQUFDO0FBRXZFLE9BQU8sRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLE1BQU0sVUFBVSxDQUFDO0FBRXBELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFNeEMsTUFBTSxPQUFPLHdCQUF5QixTQUFRLGVBQWU7SUFHM0QsWUFBWSxVQUFrQyxFQUFFO1FBQzlDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNmLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztJQUNuQyxDQUFDO0lBRU0sTUFBTSxDQUFDLE9BSWI7UUFDQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2xFLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFXLE9BQU8sQ0FBQyxPQUFRLGtDQUNwQyxPQUFPLENBQUMsUUFBUSxLQUNuQixPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxFQUFFO2dCQUN0QyxNQUFNLFVBQVUsR0FBRyxFQUFnQixDQUFDO2dCQUNwQyxRQUFRLENBQUMsT0FBTyxDQUNkLEVBQUUsVUFBVSxFQUFFLEVBQ2QsT0FBTyxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUN0QyxPQUFPLENBQUMsU0FBUyxDQUFDLHdCQUF3QixDQUFDLEVBQzNDLE9BQU8sQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsRUFDN0MsT0FBTyxDQUNSLENBQUM7WUFDSixDQUFDLEVBQ0QsSUFBSSxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUU7O2dCQUNYLE1BQU0sSUFBSSxHQUFHLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDOUIseUNBQXlDO2dCQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDM0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzdCLE1BQU0sS0FBSyxHQUFHLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNoQyxNQUFBLE1BQUEsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUMsTUFBTSxtREFBRyxLQUFLLENBQUMsQ0FBQztZQUN6QyxDQUFDLEVBQ0QsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFLE9BQU8sRUFBRSxFQUFFOztnQkFDdkIsTUFBTSxJQUFJLEdBQUcsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUM5QixNQUFNLEtBQUssR0FBRyxJQUFJLFlBQVksQ0FBQyxTQUFTLEVBQUU7b0JBQ3hDLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7aUJBQ25DLENBQUMsQ0FBQztnQkFDSCx5Q0FBeUM7Z0JBQ3pDLE1BQUEsTUFBQSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBQyxTQUFTLG1EQUFHLEtBQUssQ0FBQyxDQUFDO1lBQzVDLENBQUMsRUFDRCxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRTs7Z0JBQ1osTUFBTSxJQUFJLEdBQUcsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUM5QixNQUFNLEtBQUssR0FBRyxJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdEMsTUFBQSxNQUFBLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFDLE9BQU8sbURBQUcsS0FBSyxDQUFDLENBQUM7WUFDMUMsQ0FBQyxJQUNELENBQUM7SUFDTCxDQUFDO0NBQ0YifQ==