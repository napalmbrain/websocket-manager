import { Observable } from "./observable";
import { Signaler } from "./signaler";
import { Timeout } from "./timeout";
export declare const TIMEOUT = 10000;
export declare const KEEPALIVE: number;
export type Generic = Record<string | number, unknown>;
export type Message = Generic;
export type Handler = (connection: Connection, message: Message) => Promise<void> | void;
export type HandlerCallback = Handler;
export declare enum PROTOCOL {
    init = "init",
    init_ack = "init_ack",
    ping = "ping",
    pong = "pong",
    meta = "meta",
    meta_ack = "meta_ack"
}
export interface Action<T> {
    message: T;
    observable: Observable<T>;
    timeout?: Timeout;
    close: Signaler<void>;
    status: "active" | "inactive";
}
export type MetaFunction = ((meta: Generic) => Generic) | ((meta: Generic) => Promise<Generic>);
export interface Remote {
    manager?: string;
    connection?: string;
}
export interface Connection {
    socket: WebSocket;
    id?: string;
    remote?: Remote;
    actions?: Map<string, Action<Message>>;
    state?: Signaler<State>;
    keepalive?: Timeout;
    timeout?: Timeout;
    meta?: Generic;
    metaFunction?: MetaFunction;
    params?: Generic;
}
export declare enum STATE {
    connected = 0,
    disconnected = 1
}
export interface State {
    status: STATE;
}
export interface Deferred<T> {
    promise: Promise<T>;
    resolve: (value: T | PromiseLike<T>) => void;
}
//# sourceMappingURL=common.d.ts.map