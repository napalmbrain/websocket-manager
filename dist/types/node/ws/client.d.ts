import { ClientOptions as WSClientOptions } from "ws";
import { WebSocketClient } from "../../client";
import { Connection, MetaFunction } from "../../common";
export declare class NodeWebSocketClient extends WebSocketClient {
    connect(address: string | URL, meta?: MetaFunction, protocols?: string[], options?: WSClientOptions): Connection;
    reconnect(connection: Connection): Promise<void>;
}
//# sourceMappingURL=client.d.ts.map