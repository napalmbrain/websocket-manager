import { WebSocket as WSWebSocket, RawData as WSRawData } from "ws";
import { MessageEvent, ErrorEvent, CloseEvent } from "./../event";
type Buffer = any;
type Empty = null | undefined;
type OnOpen = (() => void) | Empty;
type OnMessage = ((data: WSRawData) => void) | Empty;
type OnError = ((error: Error) => void) | Empty;
type OnClose = ((code?: number, reason?: Buffer) => void) | Empty;
export declare class WebSocket {
    private ws;
    private _onopen;
    private _onmessage;
    private _onerror;
    private _onclose;
    constructor(ws: WSWebSocket);
    static get CONNECTING(): 0;
    static get OPEN(): 1;
    static get CLOSING(): 2;
    static get CLOSED(): 3;
    get readyState(): 0 | 2 | 1 | 3;
    get bufferedAmount(): number;
    get onopen(): OnOpen;
    set onopen(handler: ((event: Event) => void) | Empty);
    get onmessage(): OnMessage;
    set onmessage(handler: ((event: MessageEvent) => void) | Empty);
    get onerror(): OnError;
    set onerror(handler: ((event: Event | ErrorEvent) => void) | Empty);
    get onclose(): OnClose;
    set onclose(handler: ((event: CloseEvent) => void) | Empty);
    send(data: string): void;
    close(code?: number, reason?: string): void;
}
export {};
//# sourceMappingURL=websocket.d.ts.map