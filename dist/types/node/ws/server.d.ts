import { WebSocketServer as WSWebSocketServer, WebSocket as WSWebSocket, ServerOptions as WSServerOptions } from "ws";
import { WebSocketServer, WebSocketServerOptions } from "../../server";
export declare class NodeWebSocketServer extends WebSocketServer {
    protected server?: WSWebSocketServer;
    protected onconnection: (ws: WSWebSocket) => void;
    constructor(options?: WebSocketServerOptions);
    private connection;
    listen(options?: WSServerOptions): void;
    stop(): Promise<void>;
}
//# sourceMappingURL=server.d.ts.map