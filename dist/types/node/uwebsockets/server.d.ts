import { TemplatedApp, WebSocketBehavior } from "uWebSockets.js";
import { WebSocketServer, WebSocketServerOptions } from "../../server";
export declare class NodeMicroWebSocketServer extends WebSocketServer {
    protected decoder: TextDecoder;
    constructor(options?: WebSocketServerOptions);
    listen(options: {
        app: TemplatedApp;
        pattern?: string;
        behavior?: WebSocketBehavior<unknown>;
    }): void;
}
//# sourceMappingURL=server.d.ts.map