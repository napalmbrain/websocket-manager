import { WebSocket as MicroWebSocket } from "uWebSockets.js";
import { MessageEvent, ErrorEvent, CloseEvent } from "../event";
export declare class WebSocket {
    private ws;
    onopen?: (event: Event) => void;
    onmessage?: (event: MessageEvent) => void;
    onerror?: (event: Event | ErrorEvent) => void;
    onclose?: (event: CloseEvent) => void;
    constructor(ws: MicroWebSocket<unknown>);
    get bufferedAmount(): number;
    send(data: string): void;
    close(code?: number, reason?: string): void;
}
//# sourceMappingURL=websocket.d.ts.map