export declare class MessageEvent extends Event {
    readonly data?: string;
    constructor(type: string, eventInitDict?: Record<string, unknown>);
}
export declare class ErrorEvent extends Event {
    readonly message?: string;
    constructor(type: string, eventInitDict?: Record<string, unknown>);
}
export declare class CloseEvent extends Event {
    readonly code: number;
    readonly reason: string;
    readonly wasClean: boolean;
    constructor(type: string, eventInitDict?: Record<string, unknown>);
}
//# sourceMappingURL=event.d.ts.map