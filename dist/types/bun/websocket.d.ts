export interface ServerWebSocket {
    readonly data: any;
    readonly readyState: number;
    readonly remoteAddress: string;
    getBufferedAmount(): number;
    send(message: string | ArrayBuffer | Uint8Array, compress?: boolean): number;
    close(code?: number, reason?: string): void;
    subscribe(topic: string): void;
    unsubscribe(topic: string): void;
    publish(topic: string, message: string | ArrayBuffer | Uint8Array): void;
    isSubscribed(topic: string): boolean;
    cork(cb: (ws: ServerWebSocket) => void): void;
}
export declare class WebSocket {
    private ws;
    onopen?: (event: Event) => void;
    onmessage?: (event: MessageEvent) => void;
    onerror?: (event: Event | ErrorEvent) => void;
    onclose?: (event: CloseEvent) => void;
    constructor(ws: ServerWebSocket);
    get bufferedAmount(): number;
    send(data: string): void;
    close(code?: number, reason?: string): void;
}
//# sourceMappingURL=websocket.d.ts.map