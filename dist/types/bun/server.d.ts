import { WebSocketServer } from "../server";
interface HTTPServer {
    development: boolean;
    hostname: string;
    port: number;
    pendingRequests: number;
    stop(): void;
}
interface WSServer extends HTTPServer {
    pendingWebsockets: number;
    publish(topic: string, data: string | ArrayBufferView | ArrayBuffer, compress?: boolean): number;
    upgrade(req: Request, options?: {
        headers?: HeadersInit;
        data?: any;
    }): boolean;
}
export declare class BunWebSocketServer extends WebSocketServer {
    protected server?: WSServer;
    listen(options?: {
        hostname?: string;
        port?: number;
    }): void;
    stop(): Promise<void>;
}
export {};
//# sourceMappingURL=server.d.ts.map