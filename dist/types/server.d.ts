import { Connection } from "./common";
import { WebSocketManager, WebSocketManagerOptions } from "./manager";
export type WebSocketServerOptions = WebSocketManagerOptions;
export declare class WebSocketServer extends WebSocketManager {
    options: WebSocketServerOptions;
    constructor(options?: WebSocketServerOptions);
    deinit(connection: Connection): void;
    stop(): Promise<void>;
}
//# sourceMappingURL=server.d.ts.map