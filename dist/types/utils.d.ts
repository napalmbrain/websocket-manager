export declare function isDeno(): boolean;
/**
 * Determines if the environment is the browser.
 * @returns true if the environment is the browser, else false.
 */
export declare function isBrowser(): boolean;
export declare function isFunction(object: object): boolean;
export declare function sleep(time: number, options?: {
    signal?: AbortSignal;
}): Promise<void>;
export declare function keepalive(keepalive: number): number;
export declare function random(min: number, max: number): number;
export declare function limitedAssign(a: object, b: object, limit?: number): object;
export declare function uuidv4(): string;
//# sourceMappingURL=utils.d.ts.map