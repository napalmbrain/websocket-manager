import { Action, Connection, Generic, Handler, HandlerCallback, Message, State } from "./common";
import { Observer } from "./observable";
export interface Callbacks {
    onOpen?: (connection: Connection) => Promise<void> | void;
    onConnected?: (connection: Connection) => Promise<void> | void;
    onDisconnected?: (connection: Connection) => Promise<void> | void;
    onMessage?: (connection: Connection, message: Message) => Promise<void> | void;
    onError?: (connection: Connection, event: ErrorEvent | Event) => Promise<void> | void;
    onTimeout?: (connection: Connection) => Promise<void> | void;
    onClose?: (connection: Connection) => Promise<void> | void;
    onInit?: HandlerCallback;
    onInitAck?: HandlerCallback;
    onPing?: HandlerCallback;
    onPong?: HandlerCallback;
    onMeta?: HandlerCallback;
    onMetaAck?: HandlerCallback;
}
export interface WebSocketManagerOptions {
    callbacks?: Callbacks;
    enableKeepalive?: boolean;
    keepalive?: number;
    timeout?: number;
}
export declare class WebSocketManager {
    options: WebSocketManagerOptions;
    connections: Map<string, Connection>;
    protected handlers: Map<string, Handler>;
    id: string;
    constructor(options?: WebSocketManagerOptions);
    init(connection: Connection): void;
    deinit(connection: Connection): void;
    bind(connection: Connection): void;
    unbind(connection: Connection): void;
    attach(connection: Connection): void;
    detach(connection: Connection): void;
    handle(type: string, handler: Handler): void;
    send(connection: Connection, message: Message): void;
    sendOrThrow(connection: Connection, message: Message): void;
    close(connection: Connection, code?: number, reason?: string): Promise<State>;
    action(connection: Connection, message: Message, observers?: Observer<Message>[], timeout?: number): Action<Message>;
    add(connection: Connection, action: Action<Message>): void;
    remove(connection: Connection, action: Action<Message>): void;
    dispatch(connection: Connection, action: Action<Message>): void;
    message(connection: Connection, message: Message, observers?: Observer<Message>[], timeout?: number): Action<Message>;
    ping(connection: Connection, payload: Generic, observers?: Observer<Generic>[]): Promise<Generic>;
    meta(connection: Connection, payload?: Generic, observers?: Observer<Generic>[]): Promise<Generic>;
    stop(): Promise<void>;
    protected onopen(connection: Connection, _event: Event): Promise<void>;
    protected onmessage(connection: Connection, event: MessageEvent): void;
    protected onerror(connection: Connection, event: Event | ErrorEvent): void;
    protected onclose(connection: Connection, _event: CloseEvent): void;
}
//# sourceMappingURL=manager.d.ts.map