export interface Observer<T> {
    next?: (data: T) => Promise<void> | void;
    error?: (data: T) => Promise<void> | void;
    complete?: () => Promise<void> | void;
}
/**
 * Observable class.
 */
export declare class Observable<T> {
    /**
     * The array of observers.
     */
    observers: Observer<T>[];
    constructor(observers?: Observer<T>[]);
    /**
     * Call the next function on all observers, if it exists.
     * @param data Data to pass to observers.
     */
    next(data: T): void;
    /**
     * Call the error function on all observers, if it exists.
     * @param data Data to pass to observers.
     */
    error(data: T): void;
    /**
     * Call the complete function on all observers, if it exists.
     */
    complete(): void;
    /**
     * Push an observer to the array of observers.
     * @param observer The observer to add.
     */
    push(observer: Observer<T>): void;
    /**
     * Unshift an observer to the array of observers.
     * @param observer The observer to add.
     */
    unshift(observer: Observer<T>): void;
    /**
     * Remove an observer from the array of observers.
     * @param observer The observer to remove.
     */
    unobserve(observer: Observer<T>): void;
}
//# sourceMappingURL=observable.d.ts.map