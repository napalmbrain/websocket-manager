import { WebSocketManager } from "./manager";
import { Connection } from "./common";
export declare class ConnectionAsyncIterator implements AsyncIterableIterator<Connection> {
    private manager;
    private pullQueue;
    private pushQueue;
    private onConnected?;
    constructor(manager: WebSocketManager);
    [Symbol.asyncIterator](): this;
    next(): Promise<IteratorResult<Connection, unknown>>;
    return(): Promise<IteratorResult<Connection, unknown>>;
    private pullValue;
    private pushValue;
    private emptyQueue;
}
//# sourceMappingURL=iterator.d.ts.map