import { Connection, MetaFunction } from "./common";
import { WebSocketManager, WebSocketManagerOptions } from "./manager";
export interface WebSocketClientOptions extends WebSocketManagerOptions {
    reconnect?: boolean;
}
export declare class WebSocketClient extends WebSocketManager {
    options: WebSocketClientOptions;
    constructor(options?: WebSocketClientOptions);
    connect(url: string | URL, meta?: MetaFunction): Connection;
    reconnect(connection: Connection): Promise<void>;
    protected onclose(connection: Connection, event: CloseEvent): Promise<void>;
}
//# sourceMappingURL=client.d.ts.map