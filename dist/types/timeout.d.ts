export declare class Timeout {
    private callback;
    private handle;
    private callbacks;
    timeout: number;
    state: "started" | "stopped";
    constructor(callback: () => void, timeout: number);
    start(): void;
    stop(): void;
    restart(): void;
    on(callback: () => void): void;
    off(callback: () => void): void;
}
//# sourceMappingURL=timeout.d.ts.map