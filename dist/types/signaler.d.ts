export declare class Signaler<T> {
    value?: T;
    private deferreds;
    private callbacks;
    constructor();
    get signaled(): boolean;
    reset(): void;
    promise(): Promise<T>;
    unpromise(promise: Promise<T>): void;
    next(): Promise<T>;
    signal(value: T): void;
    on(callback: (value: T) => void): void;
    off(callback: (value: T) => void): void;
}
//# sourceMappingURL=signaler.d.ts.map